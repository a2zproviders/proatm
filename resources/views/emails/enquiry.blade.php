<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $subject }}</title>

    <style>
        body {
            font-family: Arial;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            border-radius: 5px;
            border-style: hidden;
            overflow: hidden;
            box-shadow: 0 0 0 1px #ccc;
        }
        table th, table td {
            border: 1px solid #ccc;
            text-align: justify;
            padding: 15px;
            background-color: #f9f9f9;
        }
        table th {
            white-space: nowrap;
            background-color: #ebebeb;
        }
    </style>
</head>
<body style="margin: 0;">
    <section
        style="max-width: 800px; margin: auto;"
    >
        <header
            style="background-color: #182C61; color: #fff; padding: 20px 0; text-align: center; font-size: 36px; font-weight: bold; text-transform: uppercase;"
        >
            {{ $store->store_name }}
        </header>
        <div
            style="padding: 30px 15px; border: 1px solid #ccc; border-top: 0;"
        >
            <div>Dear Admin,</div>
            <h3>{{ $subject }}.</h3>
            <p>Details are sent by custom is shown as below:</p>
            <div>
                <table
                    style="width: 100%;"
                >
                    <tr>
                        <th>Name</th>
                        <td>{{ $inquiry->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $inquiry->email }}</td>
                    </tr>
                    <tr>
                        <th>Contact No.</th>
                        <td>{{ $inquiry->mobile ?: 'n/a' }}</td>
                    </tr>
                    <tr>
                        <th>Message</th>
                        <td>{{ $inquiry->message }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </section>
</body>
</html>
