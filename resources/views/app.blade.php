<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $meta['title'] }}</title>

  <meta name="description" content="{{ @$meta['description'] }}">
  <meta name="keywords" content="{{ @$meta['keywords'] }}">
  <meta name="theme-color" content="{{ $store->primary_color }}" />

  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @if(!empty($store->favicon))
  <link rel="icon" href="{{ url('img/stores/'.$store->favicon) }}">
  @endif
</head>

<body>
  <div id="app"></div>
</body>
<script src="{{ asset('js/app.js') }}" id="mainJs" defer></script>

</html>