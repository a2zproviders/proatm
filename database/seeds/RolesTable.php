<?php

use Illuminate\Database\Seeder;

class RolesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\Role::create([
            'name' => 'Admin'
        ]);

        App\Model\Role::create([
            'name' => 'Store'
        ]);
        App\Model\Role::create([
            'name' => 'Customer'
        ]);
        App\Model\Role::create([
            'name' => 'Staff'
        ]);
        App\Model\Role::create([
            'name' => 'Supplier'
        ]);
    }
}
