<?php

use Illuminate\Database\Seeder;

class MenuLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\MenuLocation::create(
            [
      'name' => 'header',
      'location'=>'Header'
  ]
        );
        App\Model\MenuLocation::create(
            [
            'name' => 'footer1',
            'location'=>'Footer1'
  ]
        );
        App\Model\MenuLocation::create(
            [
            'name' => 'footer2',
            'location'=>'Footer2'
  ]
        );
    }
}
