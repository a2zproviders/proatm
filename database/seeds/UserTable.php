<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\User::create([
        'title' => 'Mr.',
        'first_name' => 'admin',
        'last_name' => 'Staff',
        'name' => 'Staff',
        'mobile' => '7972303958',
        'role' => 1,
        'password'=>Hash::make('12345678'),
        'login' => 'admin'
      ]);
    }
}
