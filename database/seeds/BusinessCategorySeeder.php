<?php

use Illuminate\Database\Seeder;

class BusinessCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\BusinessCategory::create(
            [
          'name' => 'Information',
          'slug'=> 'information'
      ]
        );
        App\Model\BusinessCategory::create(
            [
        'name' => 'E-Commerce',
        'slug'=> 'ecommerce'
      ]
        );
    }
}
