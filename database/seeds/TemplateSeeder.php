<?php

use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\Template::create(
            [
        'name' => 'About Us'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Testimonial'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Faq'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Home'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Gallery'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Service'
    ]
        );
        App\Model\Template::create(
            [
      'name' => 'Contact Us'
    ]
        );
    }
}
