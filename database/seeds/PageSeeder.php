<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Model\Pages::create([
          'title' => 'About Us',
          'slug' => 'about-us'
      ]);
        App\Model\Pages::create([
          'title' => 'Terms and Condition',
          'slug' => 'terms-and-condition'
      ]);
        App\Model\Pages::create([
          'title' => 'Privacy Policy',
          'slug' => 'privacy-policy'
      ]);
    }
}
