<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalientFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salient_features', function (Blueprint $table) {
            $table->id();
            $table->string('title', 191);
            $table->text('descrition')->nullable();
            $table->string('image', 191)->nullable();
            $table->string('icon', 191)->nullable();
            $table->unsignedBigInteger('store_id')->nullable();
            $table->foreign('store_id')->references('id')->on('users');
            $table->enum('is_visible', [1,0])->default(1);
            $table->unique(['title', 'store_id']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salient_features');
    }
}
