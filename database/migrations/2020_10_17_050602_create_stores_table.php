<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('uid');
            $table->foreign('uid')->references('id')->on('users')->onDelete('cascade');
            $table->string('store_name', 191);
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('business_categories')->onDelete('cascade');
            $table->unsignedBigInteger('logo')->nullable();
            $table->foreign('logo')->references('id')->on('media');
            $table->unsignedBigInteger('favicon')->nullable();
            $table->foreign('favicon')->references('id')->on('media');
            $table->unsignedBigInteger('aadhar_file')->nullable();
            $table->foreign('aadhar_file')->references('id')->on('media');
            $table->unsignedBigInteger('pancard_file')->nullable();
            $table->foreign('pancard_file')->references('id')->on('media');
            $table->string('aadhar_number', 40)->nullable();
            $table->string('pan_number', 40)->nullable();
            $table->string('gstin', 40)->nullable();
            $table->string('site_tagline', 191)->nullable();
            $table->decimal('phone', 20, 0)->nullable();
            $table->text('footer_script')->nullable();
            $table->text('google_map')->nullable();
            $table->text('social_links')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
