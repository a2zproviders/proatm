<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('title', 191);
            $table->text('short_description')->nullable();
            $table->string('slug', 191);
            $table->text('descrition')->nullable();
            $table->string('image', 191)->nullable();
            // $table->foreign('image')->references('id')->on('media');
            $table->string('seo_title', 191)->nullable();
            $table->string('seo_keywords', 191)->nullable();
            $table->text('seo_description')->nullable();
            $table->unsignedBigInteger('store_id')->nullable();
            $table->foreign('store_id')->references('id')->on('users');
            $table->unique(['slug', 'store_id']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
