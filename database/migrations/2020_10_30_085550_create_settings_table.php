<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('site_title', 50);
            $table->string('site_tagline', 191)->nullable();
            $table->string('email', 191)->nullable();
            $table->decimal('phone', 20, 0)->nullable();
            $table->decimal('mobile', 10, 0)->nullable();
            $table->string('address', 191)->nullable();
            $table->text('footer_script')->nullable();
            $table->text('google_map')->nullable();
            $table->text('social_links')->nullable();
            $table->string('logo', 191)->nullable();
            $table->string('favicon', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
