<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicePageDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_page_data', function (Blueprint $table) {
            $table->id();
            $table->string('title', 191);
            $table->text('descrition')->nullable();
            $table->string('image', 191)->nullable();
            $table->string('icon', 191)->nullable();
            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('services')->onDelete('cascade');
            $table->enum('is_visible', [1,0])->default(1);
            $table->unique(['title', 's_id']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_page_data');
    }
}
