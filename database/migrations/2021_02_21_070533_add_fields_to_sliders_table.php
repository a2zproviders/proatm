<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->enum('type', ['service', 'gallery', 'page', 'custom'])->default('custom');
            $table->unsignedBigInteger('page_id')->nullable();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('set null');
            // $table->unsignedBigInteger('gallery_id')->nullable();
            // $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('set null');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sliders', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropForeign(['page_id']);
            $table->dropColumn('page_id');
            $table->dropForeign(['service_id']);
            $table->dropColumn('service_id');
            // $table->dropForeign(['gallery_id']);
            // $table->dropColumn('gallery_id');
        });
    }
}
