<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('title', ['Mr.', 'Ms.', 'Mrs.'])->nullable();
            $table->string('first_name', 191);
            $table->string('last_name', 191)->nullable();
            $table->string('name', 191);
            $table->decimal('mobile', 10, 0);
            $table->string('email', 191)->nullable();
            $table->enum('gender', ['Male', 'Female', 'Other'])->default('Male');
            $table->date('dob')->nullable();
            $table->string('password', 191)->nullable();
            $table->string('login', 191)->unique();
            $table->string('device_token', 191)->nullable();
            $table->unsignedBigInteger('store_id')->nullable();
            $table->foreign('store_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('role');
            $table->foreign('role')->references('id')->on('roles')->onDelete('cascade');
            $table->unique(['mobile', 'store_id']);
            $table->unsignedBigInteger('image')->nullable();
            $table->foreign('image')->references('id')->on('media');
            $table->softDeletes('deleted_at', 0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
