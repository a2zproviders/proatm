<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            // $table->text('address')->nullable();
            // $table->string('logo', 191)->change();
            // $table->string('favicon', 191)->change();
            // $table->boolean('show_title')->default(1);

            $table->string('primary_color', 10)->default('#182C61');
            $table->string('secondary_color', 10)->default('#007bff');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            // $table->dropColumn('address');
        });
    }
}
