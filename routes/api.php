<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!

|
*/

Route::group(
    ['prefix' => '{store:store_url}', 'namespace' => 'Api'],
    function () {
        Route::post('store/sign-up', 'UserController@register')->name('register');
        Route::post('store/login', 'UserController@login')->name('login');
        Route::get('web/service/{service:slug}', 'ServiceController@showService');
        Route::post('web/service-list', 'ServiceController@serviceList');
        Route::post('web/random-post-home', 'PostController@randomPostHome');
        Route::apiResource('store/web/slider', 'SliderController', ['only' => ['index']]);
        Route::apiResource('web/service', 'ServiceController', ['only' => ['index']]);
        Route::apiResource('web/service-point', 'ServicePageDataController', ['only' => ['show']]);
        Route::get('web/faq', 'FaqController@faqList');
        Route::get('web/testimonial', 'TestimonialController@TestimonialList');
        Route::get('web/portfolio', 'PortfolioController@portfolioList');
        Route::get('web/menu', 'MenuLocationController@index');
        Route::get('web/page/{page:slug}', 'PageController@show');
        Route::get('web/web-page/{id}', 'PageController@WebShow');
        Route::get('web/randomPost', 'PostController@randomPost');
        Route::get('web/single-post/{post:slug}', 'PostController@singleShow');
        Route::get('web/single-tag/{tag:slug}', 'TagController@singleShow');
        //for category
        Route::get('web/single-category/{category:slug}', 'CategoryController@singleShow');
        Route::get('web/category-list', 'CategoryController@blogCategory');
        Route::get('web/category/service/{categoryType:name?}', 'CategoryController@allCategory');

        Route::get('web/single-portfolio/{portfolio:slug}', 'PortfolioController@singleShow');
        Route::get('store/web/menu-location', 'MenuLocationController@index');
        Route::get('web/home-template', 'HomeSettingController@homeTemplate');
        Route::get('store/web/template-info', 'HomeSettingController@templateInfo');
        //home page details
        Route::get('web/home-basic', 'UserController@userDetails');
        //contact inquiry
        Route::post('store/web/send-inquiry', 'InquiryController@store');

        Route::get('store/web/post-sidebar', 'PostController@sidebar');
        Route::apiResource('store/web/post', 'PostController', ['only' => ['index', 'show']]);
    }
);

Route::group(['prefix' => 'admin', 'namespace' => 'Api', 'as' => 'admin.'], function () {
    Route::post('login', 'UserController@admin_login')->name('login');

    Route::apiResource('business-category', 'BusinessCategoryController', ['only' => ['index']]);
    Route::apiResource('country', 'CountryController', ['only' => ['index']]);
    Route::apiResource('state', 'StateController', ['only' => ['index']]);
    Route::apiResource('city', 'CityController', ['only' => ['index']]);

    Route::apiResources([
        'store' => 'StoreController'
    ]);
    // Route::group(['middleware' => 'auth:api'], function () {
    // });
});

Route::group(['prefix' => '{store:store_url}/store', 'middleware' => 'auth:api', 'namespace' => 'Api'], function () {
    Route::get('logout', 'UserController@logout');
    Route::get('user', 'UserController@userDetails');
    Route::post('edit-profile', 'UserController@editProfile');
    Route::post('edit-store-profile', 'StoreController@editProfile');
    // Route::apiResource('page', 'PageController');
    // Route::apiResource('testimonial', 'TestimonialController');
    // Route::apiResource('faqs', 'FaqController');
    Route::post('delete-page', 'PageController@multipalDelete');
    Route::post('delete-service', 'ServiceController@multipalDelete');
    Route::post('update-page-status/{id}', 'PageController@updateStatus');
    Route::post('update-post-status/{id}', 'PostController@updateStatus');
    Route::post('delete-post', 'PostController@multipalDelete');

    Route::post('update-inquery-status/{id}', 'InquiryController@updateStatus');
    Route::post('delete-inquery', 'InquiryController@multipalDelete');
    // portfolio routes
    Route::post('update-portfolio-status/{id}', 'PortfolioController@updateStatus');
    Route::post('delete-portfolio', 'PortfolioController@multipalDelete');
    Route::post('update-service-status/{id}', 'ServiceController@updateStatus');
    Route::post('delete-testimonial', 'TestimonialController@multipalDelete');
    Route::post('delete-faq', 'FaqController@multipalDelete');
    Route::post('change-password', 'SettingController@update');
    Route::get('services/all', 'ServiceController@all');
    Route::get('template/all', 'TemplateController@all');
    //category routes
    Route::get('category/all/{categoryType:name?}', 'CategoryController@all');
    Route::post('category/add-category', 'CategoryController@addCategory');
    Route::post('delete-category', 'CategoryController@multipalDelete');

    Route::get('tag/all', 'TagController@all');
    Route::post('tag/add-tag', 'TagController@addTag');
    Route::get('menu-location/all', 'MenuLocationController@all');
    //for menu
    Route::get('menu-location/all', 'MenuLocationController@all');
    Route::get('menu-location', 'MenuLocationController@index');
    Route::post('menu-location-order', 'MenuItemController@update');
    Route::post('menu-parent', 'MenuItemController@parentupdate');
    //for homepage settings

    Route::post('slider/bulk-action', 'SliderController@bulk_action');

    Route::apiResources([
        'page'  => 'PageController',
        'testimonial'  => 'TestimonialController',
        'faqs'  => 'FaqController',
        'services' => 'ServiceController',
        'service-point' => 'ServicePageDataController',
        'page-point' => 'PageDataController',
        'category' => 'CategoryController',
        'tag' => 'TagController',
        'post' => 'PostController',
        'inquery' => 'InquiryController',
        'portfolio' => 'PortfolioController',
        'menu-item'   => 'MenuItemController',
        'home-appearance' => 'HomeSettingController',
        'media' => 'MediaController',
        'slider'    => 'SliderController'
    ]);
});
