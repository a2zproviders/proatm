(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_picture_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-picture-input */ "./node_modules/vue-picture-input/PictureInput.vue");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PagesComponent",
  components: {
    PictureInput: vue_picture_input__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      formdata: {
        title: "",
        slug: "",
        short_description: "",
        description: "",
        menu_location: [],
        template: 1,
        seo_title: "",
        seo_keywords: "",
        seo_description: ""
      },
      image: "",
      image_url: "",
      templates: {},
      menu_locations: {},
      token: localStorage.getItem("token"),
      tinyOptions: {
        height: 300,
        branding: false
      }
    };
  },
  validations: {
    formdata: {
      title: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      short_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(300)
      },
      slug: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      seo_title: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(75)
      },
      seo_keywords: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(150)
      },
      seo_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(250)
      }
    }
  },
  mounted: function mounted() {
    this.applymounted();
  },
  methods: {
    applymounted: function applymounted() {
      // this.getInfo()
      this.getTemplates();
      this.getMenuLocation();
    },
    getTemplates: function getTemplates() {
      var _this = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TEMPLATE_ALL"], header).then(function (res) {
        _this.templates = res.data;
      });
    },
    getMenuLocation: function getMenuLocation() {
      var _this2 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_MENULOCATION_ALL"], header).then(function (res) {
        _this2.menu_locations = res.data;
      });
    },
    getInfo: function getInfo() {
      var _this3 = this;

      var id = this.$route.params.Pid; // console.log(id)

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_PAGES"] + "/" + id, header).then(function (res) {
        _this3.formdata = res.data; // console.log(res.data)
      });
    },
    addPage: function addPage() {
      var _this4 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append("record[title]", this.formdata.title);
        formData.append("record[slug]", this.formdata.slug);
        formData.append("record[short_description]", this.formdata.short_description);
        formData.append("record[description]", this.formdata.description);
        formData.append("record[menu_location]", this.formdata.menu_location);
        formData.append("record[template]", this.formdata.template);
        formData.append("record[seo_title]", this.formdata.seo_title);
        formData.append("record[seo_keywords]", this.formdata.seo_keywords);
        formData.append("record[seo_description]", this.formdata.seo_description);
        formData.append("image", this.image);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_PAGES"], formData, header).then(function (res) {
          _this4.$swal("Sucecess", "Record inserted successfully", "Ok").then(function (result) {
            if (result.value) {
              _this4.$router.push({
                name: "ViewPage"
              });
            }
          });
        });
      }
    },
    onChanged: function onChanged() {
      console.log("new picture loaded", this.$refs.pictureInput.file);

      if (this.$refs.pictureInput.file) {
        this.image = this.$refs.pictureInput.file;
      } else {
        console.log("old browser. No support for FilereaderApi");
      }

      console.log(this.image);
    },
    onRemoved: function onRemoved() {
      this.formdata.image = "";
      console.log(this.formdata.image);
    }
  },
  computed: {
    slug: function slug() {
      var slug = this.formdata.title.toString().trim().toLowerCase() // LowerCase
      .replace(/\s+/g, "-") // space to -
      .replace(/&/g, "-and-") // & to and
      .replace(/\?/g, "") // & to and
      .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
      return slug;
    }
  },
  watch: {
    slug: function slug() {
      this.formdata.slug = this.slug;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.form-check-input[type=\"checkbox\"] + label,\r\nlabel.btn input[type=\"checkbox\"] + label {\r\n  position: relative;\r\n  display: inline-block;\r\n  height: 1.5625rem;\r\n  padding-left: 35px;\r\n  line-height: 1.5625rem;\r\n  cursor: pointer;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\n}\n.custom-control {\r\n  position: relative;\r\n  z-index: 1;\r\n  display: inline;\r\n  min-height: 1.5rem;\r\n  padding-left: 1.5rem;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("Pages")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "ViewPage" } } }, [
                _vm._v("Pages")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("Add page")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.addPage($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-8" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "page_title" } }, [
                        _vm._v("Page Title")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.title.$model,
                            expression: "$v.formdata.title.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.title.$error },
                        attrs: {
                          type: "text",
                          id: "page_title",
                          placeholder: "Page title",
                          autocomplete: "off"
                        },
                        domProps: { value: _vm.$v.formdata.title.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.title,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.title.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "short_description" } }, [
                        _vm._v("Short Description(300)")
                      ]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.short_description.$model,
                            expression: "$v.formdata.short_description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.short_description.$error
                        },
                        attrs: { id: "short_description", rows: "4" },
                        domProps: {
                          value: _vm.$v.formdata.short_description.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.short_description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.short_description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Short description must not have more than\n                    " +
                                _vm._s(
                                  _vm.$v.formdata.short_description.$params
                                    .maxLength.max
                                ) +
                                "\n                    letters.\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "page_slug" } }, [
                        _vm._v("Slug")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.slug.$model,
                            expression: "$v.formdata.slug.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.slug.$error },
                        attrs: {
                          type: "text",
                          id: "page_slug",
                          placeholder: "Page slug"
                        },
                        domProps: { value: _vm.$v.formdata.slug.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.slug,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-4" },
                    [
                      _c("picture-input", {
                        ref: "pictureInput",
                        attrs: {
                          zIndex: -1,
                          crop: false,
                          removable: false,
                          width: 364,
                          height: 280,
                          removeButtonClass: "btn btn-danger",
                          accept: "image/*",
                          prefill: _vm.image_url,
                          buttonClass: "btn btn-primary",
                          customStrings: {
                            upload: "<h1>Upload it!</h1>",
                            drag: "Drag and drop your image here"
                          }
                        },
                        on: { change: _vm.onChanged, remove: _vm.onRemoved }
                      })
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", { attrs: { for: "description" } }, [
                      _vm._v("Description")
                    ]),
                    _vm._v(" "),
                    _c("tinymce", {
                      attrs: { id: "d1", other_options: _vm.tinyOptions },
                      model: {
                        value: _vm.formdata.description,
                        callback: function($$v) {
                          _vm.$set(_vm.formdata, "description", $$v)
                        },
                        expression: "formdata.description"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "section",
                      { staticClass: "border p-3 mb-3" },
                      _vm._l(_vm.menu_locations, function(name, id) {
                        return _c(
                          "div",
                          {
                            key: id,
                            staticClass: "custom-control custom-checkbox"
                          },
                          [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.formdata.menu_location,
                                  expression: "formdata.menu_location"
                                }
                              ],
                              staticClass: "custom-control-input",
                              attrs: {
                                type: "checkbox",
                                id: "headermenu" + id,
                                name: "headermenu"
                              },
                              domProps: {
                                value: id,
                                checked: Array.isArray(
                                  _vm.formdata.menu_location
                                )
                                  ? _vm._i(_vm.formdata.menu_location, id) > -1
                                  : _vm.formdata.menu_location
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.formdata.menu_location,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? true : false
                                  if (Array.isArray($$a)) {
                                    var $$v = id,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.formdata,
                                          "menu_location",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.formdata,
                                          "menu_location",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(_vm.formdata, "menu_location", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c(
                              "label",
                              {
                                staticClass: "custom-control-label",
                                attrs: { for: "headermenu" + id }
                              },
                              [_vm._v(_vm._s(name))]
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _vm._m(1),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group p-3" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.formdata.template,
                              expression: "formdata.template"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.formdata,
                                "template",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.templates, function(name, id) {
                          return _c(
                            "option",
                            { key: id, domProps: { value: id } },
                            [
                              _vm._v(
                                "\n                      " +
                                  _vm._s(name) +
                                  "\n                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "py-3 pl-2 mb-3 font-weight-bold",
                    staticStyle: {
                      background: "linear-gradient(to right, #da8cff, #9a55ff)"
                    }
                  },
                  [_vm._v("\n              Meta Tag\n            ")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_title" } }, [
                    _vm._v("Meta title(75)")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_title.$model,
                        expression: "$v.formdata.seo_title.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.$v.formdata.seo_title.$error },
                    attrs: {
                      type: "text",
                      id: "meta_title",
                      placeholder: "SEO title"
                    },
                    domProps: { value: _vm.$v.formdata.seo_title.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_title,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_title.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Seo title must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_title.$params.maxLength.max
                            ) +
                            " letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_keywords" } }, [
                    _vm._v("Meta keywords(150)")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_keywords.$model,
                        expression: "$v.formdata.seo_keywords.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: {
                      "is-invalid": _vm.$v.formdata.seo_keywords.$error
                    },
                    attrs: {
                      id: "meta_keywords",
                      rows: "4",
                      placeholder: "seo keywords"
                    },
                    domProps: { value: _vm.$v.formdata.seo_keywords.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_keywords,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_keywords.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Seo keywords must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_keywords.$params.maxLength.max
                            ) +
                            " letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_description" } }, [
                    _vm._v("Meta description(250)")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_description.$model,
                        expression: "$v.formdata.seo_description.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: {
                      "is-invalid": _vm.$v.formdata.seo_description.$error
                    },
                    attrs: {
                      id: "meta_description",
                      rows: "4",
                      placeholder: "seo description"
                    },
                    domProps: { value: _vm.$v.formdata.seo_description.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_description,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_description.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Short description must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_description.$params.maxLength
                                .max
                            ) +
                            "\n                letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-gradient-primary mr-2",
                    attrs: { type: "submit" }
                  },
                  [_vm._v("\n              Submit\n            ")]
                ),
                _vm._v(" "),
                _c("button", { staticClass: "btn btn-light" }, [
                  _vm._v("Cancel")
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "h4 dark-grey-text mt-3 mb-4" }, [
      _vm._v("\n                  Menu Location\n                  "),
      _c("i", {
        staticClass: "mdi mdi-alert-circle-outline mr-2 text-success",
        staticStyle: { cursor: "pointer" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", { staticClass: "h4 dark-grey-text mt-3" }, [
      _vm._v("\n                  Select template\n                  "),
      _c("i", {
        staticClass: "mdi mdi-alert-circle-outline mr-2 text-success",
        staticStyle: { cursor: "pointer" }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/pages/Add.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/backend/pages/Add.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Add.vue?vue&type=template&id=68f875c0& */ "./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0&");
/* harmony import */ var _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Add.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Add.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/pages/Add.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=template&id=68f875c0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/Add.vue?vue&type=template&id=68f875c0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68f875c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);