(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[15],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_picture_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-picture-input */ "./node_modules/vue-picture-input/PictureInput.vue");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'AddPageData',
  components: {
    PictureInput: vue_picture_input__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      id: null,
      formdata: {
        description: '',
        title: '',
        short_description: '',
        icon: ''
      },
      image: {},
      image_url: '',
      token: localStorage.getItem('token'),
      page_id: '',
      pages: [{
        id: 13,
        title: 'About us'
      }],
      tinyOptions: {
        'height': 300,
        'branding': false
      }
    };
  },
  validations: {
    formdata: {
      title: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      short_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(500)
      }
    }
  },
  mounted: function mounted() {
    var id = this.$route.params.Pid;
    var p_id = this.$route.query.pid;

    if (p_id) {
      this.page_id = p_id;
    }

    if (id) {
      this.id = id;
      this.getInfo();
    }
  },
  methods: {
    getInfo: function getInfo() {
      var _this = this;

      var id = this.$route.params.Pid;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_PAGE_POINT"] + '/' + id, header).then(function (res) {
        _this.formdata = res.data;
        _this.page_id = res.data.page_id;
        _this.image_url = res.data.image_url;
      });
    },
    storePoint: function storePoint() {
      var _this2 = this;

      // let id = this.$route.params.Pid
      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token,
          'Content-Type': 'multipart/form-data'
        };
        var formData = new FormData();
        formData.append('record[description]', this.formdata.description);
        formData.append('record[title]', this.formdata.title);
        formData.append('record[short_description]', this.formdata.short_description);
        formData.append('record[page_id]', this.page_id);
        formData.append('record[icon]', this.formdata.icon);
        formData.append('image', this.image);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_PAGE_POINT"], formData, header).then(function (res) {
          _this2.$swal('Sucecess', 'Record insert successfully', 'Ok').then(function (result) {
            if (result.value) {
              _this2.$router.push({
                name: 'ViewPageData',
                query: {
                  pid: _this2.page_id
                }
              });
            }
          });
        });
      }
    },
    updateData: function updateData() {
      var _this3 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      var formData = new FormData();
      formData.append("_method", "put");
      formData.append('record[description]', this.formdata.description);
      formData.append('record[title]', this.formdata.title);
      formData.append('record[short_description]', this.formdata.short_description);
      formData.append('record[page_id]', this.page_id);
      formData.append('record[icon]', this.formdata.icon);
      formData.append('image', this.image);
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_PAGE_POINT"] + '/' + this.id, formData, header).then(function (res) {
        _this3.$swal('Success', 'Record updated successfully', 'Ok').then(function (result) {
          if (result.value) {
            _this3.$router.push({
              name: 'ViewPageData',
              query: {
                pid: res.data.page_id
              }
            });
          }
        });
      });
    },
    onChanged: function onChanged() {
      console.log('new picture loaded', this.$refs.pictureInput.file);

      if (this.$refs.pictureInput.file) {
        this.image = this.$refs.pictureInput.file;
      } else {
        console.log("old browser. No support for FilereaderApi");
      } // console.log(this.image);

    },
    onRemoved: function onRemoved() {
      this.formdata.image = '';
      console.log(this.formdata.image);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [
        _vm._v(_vm._s(_vm.id ? "Edit" : "Add") + " page point")
      ]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "ViewPageData" } } }, [
                _vm._v("View page point")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("Page point field")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    !_vm.id ? _vm.storePoint() : _vm.updateData()
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-8" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "title" } }, [
                        _vm._v("Title")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.title.$model,
                            expression: "$v.formdata.title.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.title.$error },
                        attrs: {
                          type: "text",
                          id: "title",
                          placeholder: "Title"
                        },
                        domProps: { value: _vm.$v.formdata.title.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.title,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.title.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v("Field is required")
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "short_description" } }, [
                        _vm._v("Short Description")
                      ]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.short_description.$model,
                            expression: "$v.formdata.short_description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.short_description.$error
                        },
                        attrs: { id: "short_description", rows: "4" },
                        domProps: {
                          value: _vm.$v.formdata.short_description.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.short_description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.short_description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Max length is " +
                                _vm._s(
                                  _vm.$v.formdata.short_description.$params
                                    .maxLength.max
                                )
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", { attrs: { for: "description" } }, [
                          _vm._v("Description")
                        ]),
                        _vm._v(" "),
                        _c("tinymce", {
                          attrs: { id: "d1", other_options: _vm.tinyOptions },
                          model: {
                            value: _vm.formdata.description,
                            callback: function($$v) {
                              _vm.$set(_vm.formdata, "description", $$v)
                            },
                            expression: "formdata.description"
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "icon" } }, [
                        _vm._v("Point Icon")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.formdata.icon,
                            expression: "formdata.icon"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "icon",
                          placeholder: "Page icon ex. fa fa-facebook"
                        },
                        domProps: { value: _vm.formdata.icon },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.formdata, "icon", $event.target.value)
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Image upload")]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "input-group col-xs-12 bg-light" },
                        [
                          _c("picture-input", {
                            ref: "pictureInput",
                            attrs: {
                              zIndex: -1,
                              crop: false,
                              removable: false,
                              width: 364,
                              height: 280,
                              removeButtonClass: "btn btn-danger",
                              accept: "image/*",
                              prefill: _vm.image_url,
                              buttonClass: "btn btn-primary",
                              customStrings: {
                                upload: "<h1>Upload it!</h1>",
                                drag: "Drag and drop your image here"
                              }
                            },
                            on: { change: _vm.onChanged, remove: _vm.onRemoved }
                          })
                        ],
                        1
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                !_vm.id
                  ? _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-2",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("Submit")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.id
                  ? _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-2",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("Update")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("button", { staticClass: "btn btn-light" }, [
                  _vm._v("Cancel")
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/pages/AddPageData.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/backend/pages/AddPageData.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddPageData.vue?vue&type=template&id=4c43f439& */ "./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439&");
/* harmony import */ var _AddPageData_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddPageData.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddPageData_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/pages/AddPageData.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddPageData_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddPageData.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/AddPageData.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddPageData_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AddPageData.vue?vue&type=template&id=4c43f439& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/pages/AddPageData.vue?vue&type=template&id=4c43f439&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddPageData_vue_vue_type_template_id_4c43f439___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);