(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[96],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8& ***!
  \********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      {
        staticClass: "suncity_services mt-4",
        staticStyle: { background: "#fff !important" }
      },
      [
        _c("div", { staticClass: "container py-5" }, [
          _c("h1", [_vm._v(_vm._s(_vm.page.title))]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("p", { domProps: { innerHTML: _vm._s(_vm.page.description) } }),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(2),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(3),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "circle_dot" }, [
      _c("span"),
      _vm._v(" "),
      _c("span"),
      _vm._v(" "),
      _c("span")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Cras justo odio\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Dapibus ac facilisis in\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Morbi leo risus\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Cras justo odio\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Dapibus ac facilisis in\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Morbi leo risus\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Cras justo odio\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Dapibus ac facilisis in\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Morbi leo risus\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/template/AboutComponent.vue":
/*!*******************************************************************!*\
  !*** ./resources/js/components/front/template/AboutComponent.vue ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AboutComponent.vue?vue&type=template&id=bcb6e2b8& */ "./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/template/AboutComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8& ***!
  \**************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AboutComponent.vue?vue&type=template&id=bcb6e2b8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/AboutComponent.vue?vue&type=template&id=bcb6e2b8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutComponent_vue_vue_type_template_id_bcb6e2b8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);