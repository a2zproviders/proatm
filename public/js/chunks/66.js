(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[66],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var _services_api_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/api.js */ "./resources/js/services/api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      formdata: {},
      deleteItems: [],
      all_select: false,
      status: [],
      action: "",
      token: localStorage.getItem("token"),
      search: "",
      trash: 0,
      drafted: false,
      counts: {}
    };
  },
  methods: {
    searchList: lodash__WEBPACK_IMPORTED_MODULE_0___default.a.debounce(function () {
      this.listPost(1);
    }, 500),
    listPost: function listPost() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["view_slider"])().then(function (res) {
        _this.formdata = res;
      });
    },
    ToggleTrash: function ToggleTrash() {
      this.trash = !this.trash;
      this.listPost();
    },
    showDrafted: function showDrafted(drafted) {
      this.trash = false;
      this.drafted = drafted;
      this.listPost();
    },
    updateStatus: function updateStatus(data) {
      var _this2 = this;

      var id = data.id;
      var status = data.is_visible ? "1" : "0";
      var params = {
        slider: {
          is_visible: status
        }
      };
      Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["update_slider"])(id, params).then(function (res) {
        _this2.$swal("Done!", res.data.message, "success");

        _this2.listPost();
      });
    },
    deleteRow: function deleteRow(id, flag) {
      var _this3 = this;

      var msg = "",
          delete_record = "";

      if (flag == "Delete") {
        msg = "You will not be able to recover this record!";
        delete_record = "Yes, delete it!";
      } else if (flag == "Trash") {
        msg = "You will move this record to trash!";
        delete_record = "Yes, trash it!";
      } else {
        msg = "You will restore this record!";
        delete_record = "Yes, restore it!";
      }

      this.$swal({
        title: "Are you sure?",
        text: msg,
        showCancelButton: true,
        confirmButtonText: delete_record,
        cancelButtonText: "No, keep it"
      }).then(function (result) {
        if (result.value) {
          Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["delete_slider"])(id, "flag=".concat(flag)).then(function (res) {
            _this3.trash = false;

            _this3.listPost(1);
          });
        }
      });
    },
    multipalDelete: function multipalDelete() {
      var _this4 = this;

      if (this.action != 0) {
        if (this.deleteItems.length == 0) {
          this.$swal("Warning", "please select an item", "Ok");
        } else {
          // let header = { Authorization: "Bearer " + this.token };
          // let formData = new FormData();
          // formData.append("ids", this.deleteItems);
          // formData.append("action", this.action);
          // POST(STORE_PORTFOLIO_DELETE, formData, header).then((res) => {
          //   if (res.data.status) {
          //     this.deleteItems = [];
          //     this.all_select = false;
          //     this.action = "";
          //     this.listPost();
          //   } else {
          //   }
          // });
          Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["bulk_action_slider"])({
            action: this.action,
            ids: this.deleteItems
          }).then(function (res) {
            _this4.deleteItems = [];
            _this4.all_select = false;
            _this4.action = "";

            _this4.$swal("Success!", res.data.message, "success");

            _this4.listPost();
          });
        }
      } else {
        this.$swal("Warning", "please select a valid action", "Ok");
      }
    },
    select_all_via_check_box: function select_all_via_check_box() {
      var _this5 = this;

      if (this.all_select == false) {
        this.all_select = true;
        this.deleteItems = [];
        this.formdata.data.forEach(function (item) {
          _this5.deleteItems.push(item.id);
        });
      } else {
        this.all_select = false;
        this.deleteItems = [];
      }
    }
  },
  mounted: function mounted() {
    this.listPost();
  },
  watch: {
    deleteItems: function deleteItems() {
      if (this.formdata.data && this.formdata.data.data.length == this.deleteItems.length) {
        this.all_select = true;
      } else {
        this.all_select = false;
      }
    },
    search: function search() {
      this.searchList();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("Slider")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "SliderMaster" } } }, [
                _vm._v("Slider Master")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("Slider")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        !_vm.formdata.data
          ? _c(
              "div",
              { staticClass: "text-center w-100" },
              [
                _c("b-spinner", {
                  staticStyle: { width: "100px", height: "100px" }
                })
              ],
              1
            )
          : _vm._e(),
        _vm._v(" "),
        _vm.formdata.data
          ? _c("div", { staticClass: "card" }, [
              _c("div", { staticClass: "card-body" }, [
                !_vm.formdata.data.length
                  ? _c("div", { staticClass: "alert alert-danger" }, [
                      _vm._v("\n            No records found.\n          ")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _vm.formdata.data.length
                  ? _c("div", [
                      _c(
                        "div",
                        { staticClass: "float-right py-2" },
                        [
                          _c(
                            "router-link",
                            {
                              staticClass: "btn btn-sm btn-dark",
                              attrs: { to: { name: "AddSlides" } }
                            },
                            [
                              _c("i", { staticClass: "mdi mdi-plus" }),
                              _vm._v(" Add new\n              ")
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "card-description form-group w-50" },
                        [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-sm-4" }, [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.action,
                                      expression: "action"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { padding: "10px" },
                                  on: {
                                    change: [
                                      function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.action = $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      },
                                      _vm.multipalDelete
                                    ]
                                  }
                                },
                                [
                                  _c("option", { attrs: { value: "" } }, [
                                    _vm._v("Bulk Action")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "Delete" } }, [
                                    _vm._v("Permanent Delete")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "Enable" } }, [
                                    _vm._v("Enable")
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "option",
                                    { attrs: { value: "Disable" } },
                                    [_vm._v("Disable")]
                                  )
                                ]
                              )
                            ])
                          ])
                        ]
                      ),
                      _vm._v(" "),
                      _vm.formdata.data.length
                        ? _c("table", { staticClass: "table table-striped" }, [
                            _c("thead", { staticClass: "bg-dark text-light" }, [
                              _c("tr", [
                                _c("th", [
                                  _c("label", [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.all_select,
                                          expression: "all_select"
                                        }
                                      ],
                                      attrs: { type: "checkbox" },
                                      domProps: {
                                        checked: Array.isArray(_vm.all_select)
                                          ? _vm._i(_vm.all_select, null) > -1
                                          : _vm.all_select
                                      },
                                      on: {
                                        click: _vm.select_all_via_check_box,
                                        change: function($event) {
                                          var $$a = _vm.all_select,
                                            $$el = $event.target,
                                            $$c = $$el.checked ? true : false
                                          if (Array.isArray($$a)) {
                                            var $$v = null,
                                              $$i = _vm._i($$a, $$v)
                                            if ($$el.checked) {
                                              $$i < 0 &&
                                                (_vm.all_select = $$a.concat([
                                                  $$v
                                                ]))
                                            } else {
                                              $$i > -1 &&
                                                (_vm.all_select = $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1)))
                                            }
                                          } else {
                                            _vm.all_select = $$c
                                          }
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("span", [
                                      _vm._v(
                                        "\n                        " +
                                          _vm._s(
                                            _vm.all_select == true
                                              ? "Uncheck All"
                                              : "Select All"
                                          ) +
                                          "\n                      "
                                      )
                                    ])
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Image")]),
                                _vm._v(" "),
                                _c("th", [_vm._v("Title")]),
                                _vm._v(" "),
                                !_vm.trash
                                  ? _c("th", [_vm._v("Published")])
                                  : _vm._e()
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(this.formdata.data, function(page, i) {
                                return _c("tr", { key: i }, [
                                  _c("td", [
                                    _c("label", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.deleteItems,
                                            expression: "deleteItems"
                                          }
                                        ],
                                        attrs: { type: "checkbox" },
                                        domProps: {
                                          value: page.id,
                                          checked: Array.isArray(
                                            _vm.deleteItems
                                          )
                                            ? _vm._i(_vm.deleteItems, page.id) >
                                              -1
                                            : _vm.deleteItems
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = _vm.deleteItems,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = page.id,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  (_vm.deleteItems = $$a.concat(
                                                    [$$v]
                                                  ))
                                              } else {
                                                $$i > -1 &&
                                                  (_vm.deleteItems = $$a
                                                    .slice(0, $$i)
                                                    .concat($$a.slice($$i + 1)))
                                              }
                                            } else {
                                              _vm.deleteItems = $$c
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(" " + _vm._s(i + 1) + ". ")
                                      ])
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "py-1" }, [
                                    _c("img", {
                                      attrs: { src: page.image, alt: "image" }
                                    })
                                  ]),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(page.title) +
                                        "\n                    "
                                    ),
                                    !_vm.trash
                                      ? _c(
                                          "div",
                                          { staticClass: "mt-2" },
                                          [
                                            _c(
                                              "button",
                                              {
                                                staticClass: "btn btn-link p-0",
                                                attrs: {
                                                  type: "button",
                                                  name: "button"
                                                },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.deleteRow(
                                                      page.id,
                                                      "Delete"
                                                    )
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "mdi mdi-delete text-danger"
                                                }),
                                                _vm._v(
                                                  " Delete\n                      "
                                                )
                                              ]
                                            ),
                                            _vm._v(
                                              "\n                      |\n                      "
                                            ),
                                            _c(
                                              "router-link",
                                              {
                                                staticClass: "btn btn-link p-0",
                                                attrs: {
                                                  to: {
                                                    name: "EditSlides",
                                                    params: { id: page.id }
                                                  }
                                                }
                                              },
                                              [
                                                _c("i", {
                                                  staticClass:
                                                    "mdi mdi-pencil text-info"
                                                }),
                                                _vm._v(
                                                  " Edit\n                      "
                                                )
                                              ]
                                            )
                                          ],
                                          1
                                        )
                                      : _vm._e()
                                  ]),
                                  _vm._v(" "),
                                  !_vm.trash
                                    ? _c("td", [
                                        _c("label", { staticClass: "switch" }, [
                                          _c("input", {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: page.is_visible,
                                                expression: "page.is_visible"
                                              }
                                            ],
                                            attrs: { type: "checkbox" },
                                            domProps: {
                                              checked: Array.isArray(
                                                page.is_visible
                                              )
                                                ? _vm._i(
                                                    page.is_visible,
                                                    null
                                                  ) > -1
                                                : page.is_visible
                                            },
                                            on: {
                                              change: [
                                                function($event) {
                                                  var $$a = page.is_visible,
                                                    $$el = $event.target,
                                                    $$c = $$el.checked
                                                      ? true
                                                      : false
                                                  if (Array.isArray($$a)) {
                                                    var $$v = null,
                                                      $$i = _vm._i($$a, $$v)
                                                    if ($$el.checked) {
                                                      $$i < 0 &&
                                                        _vm.$set(
                                                          page,
                                                          "is_visible",
                                                          $$a.concat([$$v])
                                                        )
                                                    } else {
                                                      $$i > -1 &&
                                                        _vm.$set(
                                                          page,
                                                          "is_visible",
                                                          $$a
                                                            .slice(0, $$i)
                                                            .concat(
                                                              $$a.slice($$i + 1)
                                                            )
                                                        )
                                                    }
                                                  } else {
                                                    _vm.$set(
                                                      page,
                                                      "is_visible",
                                                      $$c
                                                    )
                                                  }
                                                },
                                                function($event) {
                                                  return _vm.updateStatus(page)
                                                }
                                              ]
                                            }
                                          }),
                                          _vm._v(" "),
                                          _c("span", {
                                            staticClass: "slider round"
                                          })
                                        ])
                                      ])
                                    : _vm._e()
                                ])
                              }),
                              0
                            )
                          ])
                        : _vm._e()
                    ])
                  : _vm._e()
              ])
            ])
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/slider/View.vue":
/*!*********************************************************!*\
  !*** ./resources/js/components/backend/slider/View.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./View.vue?vue&type=template&id=8c028b86& */ "./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86&");
/* harmony import */ var _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./View.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__["render"],
  _View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/slider/View.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=template&id=8c028b86& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/View.vue?vue&type=template&id=8c028b86&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_8c028b86___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: update_meta, baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, update_menu_parent, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, view_posts, post_sidebar, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_meta", function() { return update_meta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_parent", function() { return update_menu_parent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_posts", function() { return view_posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "post_sidebar", function() { return post_sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var update_meta = function update_meta(metaJSON) {
  if (metaJSON.title) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('title').text(metaJSON.title);
  }

  if (metaJSON.keywords) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=keywords]').text(metaJSON.keywords);
  }

  if (metaJSON.description) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=description]').text(metaJSON.description);
  }
};
var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  // prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var update_menu_parent = function update_menu_parent(data) {
  var params = {
    method: 'POST',
    url: "menu-parent",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data,
    files: true
  };
  return execute(params);
};
/**
 * Blogs
 */

var view_posts = function view_posts(query) {
  var params = {
    url: "web/post/?".concat(query),
    no_auth: true
  };
  return execute(params);
};
var post_sidebar = function post_sidebar() {
  var params = {
    url: "web/post-sidebar",
    no_auth: true
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);