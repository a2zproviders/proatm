(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    page: Object,
    items: Array
  },
  computed: {
    backgroundImage: function backgroundImage() {
      return this.page.image ? this.page.image : "";
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/single.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_PostSidebar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/PostSidebar */ "./resources/js/components/front/components/PostSidebar.vue");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var _components_PageHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/PageHeader */ "./resources/js/components/front/components/PageHeader.vue");
/* harmony import */ var _services_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api */ "./resources/js/services/api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      service: {},
      points: {}
    };
  },
  components: {
    PageHeader: _components_PageHeader__WEBPACK_IMPORTED_MODULE_3__["default"],
    PostSidebar: _components_PostSidebar__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  computed: {
    page: function page() {
      var page = this.service;
      page.title = page.name;
      return page;
    }
  },
  mounted: function mounted() {
    this.getBlog();
  },
  methods: {
    searchPost: function searchPost(search) {},
    getBlog: function getBlog() {
      var _this = this;

      this.$emit("loader_token", true);
      var id = this.$route.params.slug;
      var header = {};
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_FRONT_SINGLE_POST"] + "/" + this.$route.params.slug, header).then(function (res) {
        var _res$data$seo_title;

        _this.service = res.data; // this.points = res.data.points

        Object(_services_api__WEBPACK_IMPORTED_MODULE_4__["update_meta"])({
          title: (_res$data$seo_title = res.data.seo_title) !== null && _res$data$seo_title !== void 0 ? _res$data$seo_title : res.data.name,
          keywords: res.data.keywords,
          description: res.data.description
        });

        _this.$emit("loader_token", false);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.top-header[data-v-d651fe58] {\r\n  background-color: var(--primary);\r\n  text-align: center;\r\n  color: var(--white);\r\n  text-transform: capitalize;\n}\n.top-header #overlay[data-v-d651fe58] {\r\n  padding: 40px 0;\r\n  position: relative;\r\n  background-color: rgba(0, 0, 0, 0.5);\n}\n.breadcrumb[data-v-d651fe58] {\r\n  justify-content: center;\n}\n.breadcrumb a[data-v-d651fe58] {\r\n  color: var(--white);\r\n  font-weight: bold;\n}\n.breadcrumb-item.active[data-v-d651fe58],\r\n.breadcrumb-item + .breadcrumb-item[data-v-d651fe58]::before {\r\n  color: var(--white);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#overlay {\r\n  position: relative;\r\n  display: block;\r\n  width: 100%;\r\n  height: 100%;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 2;\r\n  cursor: pointer;\n}\n#text {\r\n  position: absolute;\r\n  top: 60%;\r\n  left: 50%;\r\n  font-size: 50px;\r\n  color: white;\r\n  transform: translate(-50%, -50%);\r\n  -ms-transform: translate(-50%, -50%);\n}\n.line_height p {\r\n  line-height: 2;\n}\na.post-tag {\r\n  background: #f5f5f5;\r\n  margin-left: 5px;\r\n  border-radius: 15px;\r\n  display: inline-block;\r\n  padding: 0px 15px;\r\n  text-transform: capitalize;\r\n  text-decoration: none;\n}\na.post-tag:hover {\r\n  background-color: var(--primary);\r\n  color: var(--white);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.page.title && _vm.page.title !== "Home"
    ? _c(
        "section",
        {
          staticClass: "top-header",
          style: {
            backgroundImage: "url('" + _vm.backgroundImage + "')"
          }
        },
        [
          _c(
            "div",
            { attrs: { id: "overlay" } },
            [
              _c("h1", [_vm._v(_vm._s(_vm.page.title))]),
              _vm._v(" "),
              _c("b-breadcrumb", { attrs: { items: _vm.items } })
            ],
            1
          )
        ]
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=template&id=157a008e&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/single.vue?vue&type=template&id=157a008e& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {},
    [
      _c("page-header", {
        attrs: {
          page: _vm.page,
          items: [
            {
              text: "Home",
              href: "/"
            },
            {
              text: "Blog",
              href: "/blog"
            },
            {
              text: _vm.page.title,
              active: true
            }
          ]
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "container py-5" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-sm-9" }, [
            _c("div", { staticStyle: { position: "relative" } }, [
              _c("img", {
                staticClass: "card-img-top",
                attrs: { src: _vm.service.image, alt: _vm.service.name }
              })
            ]),
            _vm._v(" "),
            _vm.service.category && _vm.service.category.length
              ? _c("div", [
                  _c(
                    "p",
                    { staticClass: "my-2" },
                    [
                      _vm._v("\n            Category:\n            "),
                      _vm._l(_vm.service.category, function(category, i) {
                        return _c(
                          "router-link",
                          {
                            key: i,
                            attrs: {
                              to: {
                                name: "CategoryDetails",
                                params: { slug: category.slug }
                              }
                            }
                          },
                          [
                            _vm._v(
                              _vm._s(category.name) +
                                _vm._s(
                                  i !== _vm.service.category.length - 1
                                    ? ","
                                    : ""
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      })
                    ],
                    2
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.service.tag.length
              ? _c("div", [
                  _c(
                    "p",
                    { staticClass: "my-2" },
                    [
                      _c("i", { staticClass: "icon-tag" }),
                      _vm._v(" Tag:\n            "),
                      _vm._l(_vm.service.tag, function(tag, i) {
                        return _c(
                          "router-link",
                          {
                            key: i,
                            staticClass: "post-tag",
                            attrs: {
                              to: {
                                name: "TagDetails",
                                params: { slug: tag.slug }
                              }
                            }
                          },
                          [_vm._v(_vm._s(tag.name) + "\n            ")]
                        )
                      })
                    ],
                    2
                  )
                ])
              : _vm._e(),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "p-3", staticStyle: { background: "#f9f9f9" } },
              [
                !_vm.service.short_description == ""
                  ? _c("p", { staticClass: "py-3 mx-auto text-left" }, [
                      _vm._v(
                        "\n            " +
                          _vm._s(_vm.service.short_description) +
                          "\n          "
                      )
                    ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", {
                  staticClass: "line_height texteditor-text",
                  domProps: { innerHTML: _vm._s(_vm.service.description) }
                })
              ]
            ),
            _vm._v(" "),
            _vm.points.length
              ? _c("h2", { staticClass: "mt-4" }, [
                  _vm._v("Why people choose us")
                ])
              : _vm._e(),
            _vm._v(" "),
            _vm.points.length
              ? _c(
                  "div",
                  { staticClass: "row mt-4" },
                  _vm._l(_vm.points, function(p, i) {
                    return _c("div", { key: i, staticClass: "col-sm-4 mb-3" }, [
                      _c(
                        "div",
                        {
                          staticClass: "card h-100",
                          staticStyle: { border: "1px solid #ccc" }
                        },
                        [
                          _c("div", { staticClass: "card-header" }, [
                            _c("h5", [_vm._v(_vm._s(p.name))])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticStyle: { padding: "10px" } }, [
                            _vm._v(
                              "\n                " +
                                _vm._s(p.short_description) +
                                "\n              "
                            )
                          ])
                        ]
                      )
                    ])
                  }),
                  0
                )
              : _vm._e()
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "col-sm-3" },
            [
              _c("post-sidebar", {
                attrs: { search: false },
                on: { searchPost: _vm.searchPost }
              })
            ],
            1
          )
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&");
/* harmony import */ var _PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=script&lang=js& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d651fe58",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/components/PageHeader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/front/single.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/front/single.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single.vue?vue&type=template&id=157a008e& */ "./resources/js/components/front/single.vue?vue&type=template&id=157a008e&");
/* harmony import */ var _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single.vue?vue&type=script&lang=js& */ "./resources/js/components/front/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./single.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/single.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/front/single.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/single.vue?vue&type=template&id=157a008e&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/front/single.vue?vue&type=template&id=157a008e& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=template&id=157a008e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/single.vue?vue&type=template&id=157a008e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_157a008e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: update_meta, baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, update_menu_parent, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, view_posts, post_sidebar, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_meta", function() { return update_meta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_parent", function() { return update_menu_parent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_posts", function() { return view_posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "post_sidebar", function() { return post_sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var update_meta = function update_meta(metaJSON) {
  if (metaJSON.title) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('title').text(metaJSON.title);
  }

  if (metaJSON.keywords) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=keywords]').text(metaJSON.keywords);
  }

  if (metaJSON.description) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=description]').text(metaJSON.description);
  }
};
var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  // prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var update_menu_parent = function update_menu_parent(data) {
  var params = {
    method: 'POST',
    url: "menu-parent",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data,
    files: true
  };
  return execute(params);
};
/**
 * Blogs
 */

var view_posts = function view_posts(query) {
  var params = {
    url: "web/post/?".concat(query),
    no_auth: true
  };
  return execute(params);
};
var post_sidebar = function post_sidebar() {
  var params = {
    url: "web/post-sidebar",
    no_auth: true
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);