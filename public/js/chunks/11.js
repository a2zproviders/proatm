(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PagesComponent',
  data: function data() {
    return {
      id: '',
      data: {},
      formdata: {
        name: '',
        slug: '',
        description: '',
        seo_title: '',
        seo_keywords: '',
        seo_description: ''
      },
      setClass: '',
      deleteItems: [],
      all_select: false,
      search: '',
      action: '',
      token: localStorage.getItem('token'),
      tinyOptions: {
        'height': 300,
        'branding': false
      }
    };
  },
  validations: {
    formdata: {
      name: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["required"]
      },
      description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(500)
      },
      slug: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["required"],
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(75)
      },
      seo_title: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(100)
      },
      seo_keywords: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(150)
      },
      seo_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(250)
      }
    }
  },
  mounted: function mounted() {
    var id = this.$route.params.Tagid;

    if (typeof id != 'undefined') {
      this.id = id;
    }

    this.applymounted();
  },
  methods: {
    searchList: lodash__WEBPACK_IMPORTED_MODULE_0___default.a.debounce(function () {
      this.getTag(1);
    }, 500),
    applymounted: function applymounted() {
      if (this.id != '' && this.id != undefined) {
        this.singleRecord();
      }

      this.getTag();
    },
    getTag: function getTag() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TAG"] + '/?page=' + page + '&s=' + this.search, header).then(function (res) {
        _this.data = res.data;
      });
    },
    singleRecord: function singleRecord() {
      var _this2 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TAG"] + '/' + this.id, header).then(function (res) {
        // console.log("category data", res.data);
        _this2.formdata = res.data;
      });
    },
    addCategory: function addCategory() {
      var _this3 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append('record[name]', this.formdata.name);
        formData.append('record[slug]', this.formdata.slug);
        formData.append('record[description]', this.formdata.description);
        formData.append('record[seo_title]', this.formdata.seo_title);
        formData.append('record[seo_keywords]', this.formdata.seo_keywords);
        formData.append('record[seo_description]', this.formdata.seo_description);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TAG_ADD"], formData, header).then(function (res) {
          if (res.data.status == 2) {
            _this3.setClass = 'setcolor';
          }

          _this3.$swal(res.data.message1, res.data.message, 'Ok').then(function (result) {
            if (res.data.status == 1) {
              _this3.getTag(1);
            }
          });
        });
      }
    },
    updateCategory: function updateCategory() {
      var _this4 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append('_method', 'PUT');
        formData.append('record[name]', this.formdata.name);
        formData.append('record[slug]', this.formdata.slug);
        formData.append('record[description]', this.formdata.description);
        formData.append('record[seo_title]', this.formdata.seo_title);
        formData.append('record[seo_keywords]', this.formdata.seo_keywords);
        formData.append('record[seo_description]', this.formdata.seo_description);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TAG"] + '/' + this.id, formData, header).then(function (res) {
          if (res.data.status == 2) {
            _this4.setClass = 'setcolor';
          }

          _this4.$swal(res.data.message1, res.data.message, 'Ok').then(function (result) {
            if (res.data.status == 1) {
              _this4.getTag(1);
            }
          });
        });
      }
    },
    deleteRow: function deleteRow(id) {
      var _this5 = this;

      var msg = '',
          delete_record = '';
      msg = 'You will not be able to recover this record!';
      delete_record = 'Yes, delete it!';
      this.$swal({
        title: 'Are you sure?',
        text: msg,
        showCancelButton: true,
        confirmButtonText: delete_record,
        cancelButtonText: 'No, keep it'
      }).then(function (result) {
        if (result.value) {
          var header = {
            Authorization: "Bearer " + _this5.token
          };
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["DELETE"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_TAG"] + '/' + id, header).then(function (res) {
            _this5.getTag(1);
          });
        }
      });
    },
    multipalDelete: function multipalDelete() {
      var _this6 = this;

      if (this.action != 0) {
        var header = {
          Authorization: "Bearer " + this.token
        };

        if (this.deleteItems.length == 0) {
          this.$swal('Warning', 'please select an item', 'Ok');
        } else {
          var formData = new FormData();
          formData.append('ids', this.deleteItems);
          formData.append('action', this.action);
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(STORE_POST_DELETE, formData, header).then(function (res) {
            if (res.data.status) {
              _this6.deleteItems = [];
              _this6.all_select = false;
              _this6.action = '';

              _this6.getTag();
            } else {}
          });
        }
      } else {
        this.$swal('Warning', 'please select a valid action', 'Ok');
      }
    },
    select_all_via_check_box: function select_all_via_check_box() {
      var _this7 = this;

      if (this.all_select == false) {
        this.all_select = true;
        this.deleteItems = [];
        this.data.data.forEach(function (item) {
          _this7.deleteItems.push(item.id);
        });
      } else {
        this.all_select = false;
        this.deleteItems = [];
      }
    }
  },
  computed: {
    slug: function slug() {
      var slug = this.formdata.name.toString().trim().toLowerCase() // LowerCase
      .replace(/\s+/g, "-") // space to -
      .replace(/&/g, "-and-") // & to and
      .replace(/\?/g, "") // & to and
      .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
      return slug;
    }
  },
  watch: {
    slug: function slug() {
      this.formdata.slug = this.slug;
    },
    search: function search() {
      this.searchList();
    },
    deleteItems: function deleteItems() {
      if (this.data && this.data.data.length == this.deleteItems.length) {
        this.all_select = true;
      } else {
        this.all_select = false;
      }
    },
    "$route.params.Tagid": function $routeParamsTagid(id) {
      // console.log('cid', id)
      if (typeof id == 'undefined') {
        this.formdata = {
          name: '',
          slug: '',
          description: '',
          seo_title: '',
          seo_keywords: '',
          seo_description: ''
        };
        this.id = '';
        this.setClass = '';
      } else {
        this.id = id;
        this.applymounted();
      }
    },
    "formdata.slug": function formdataSlug(slug) {
      this.formdata.slug = slug.toString().toLowerCase() // LowerCase
      .replace(/\s+/g, "-") // space to -
      .replace(/&/g, "-and-") // & to and
      .replace(/\?/g, "") // & to and
      .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-");
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.form-check-input[type=\"checkbox\"]+label, label.btn input[type=\"checkbox\"]+label {\r\n  position: relative;\r\n  display: inline-block;\r\n  height: 1.5625rem;\r\n  padding-left: 35px;\r\n  line-height: 1.5625rem;\r\n  cursor: pointer;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\n}\n.custom-control {\r\n    position: relative;\r\n    z-index: 1;\r\n    display: inline;\r\n    min-height: 1.5rem;\r\n    padding-left: 1.5rem;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c(
        "h3",
        { staticClass: "page-title" },
        [
          _vm._v(" " + _vm._s(_vm.id ? "Edit" : "Add") + " new tag "),
          _vm.id
            ? _c(
                "router-link",
                {
                  staticClass: "btn btn-sm btn-dark text-light text-left",
                  attrs: { to: { name: "AddTag" }, name: "button" }
                },
                [_vm._v("Add new ")]
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "BlogList" } } }, [
                _vm._v("Tag")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v(_vm._s(_vm.id ? "Edit" : "Add") + "  tag")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-5" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.name.$model,
                            expression: "$v.formdata.name.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.name.$error },
                        attrs: {
                          type: "text",
                          id: "page_title",
                          placeholder: "Tag name"
                        },
                        domProps: { value: _vm.$v.formdata.name.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.name,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.name.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v("Field is required")
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.formdata.slug,
                            expression: "formdata.slug"
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.slug.$error },
                        attrs: {
                          type: "text",
                          maxlength: "75",
                          id: "page_slug",
                          placeholder: "Slug"
                        },
                        domProps: { value: _vm.formdata.slug },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.formdata, "slug", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v("Field is required")
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Max length is " +
                                _vm._s(
                                  _vm.$v.formdata.slug.$params.maxLength.max
                                )
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.description.$model,
                            expression: "$v.formdata.description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.description.$error
                        },
                        attrs: {
                          id: "description",
                          rows: "11",
                          placeholder: "Category description"
                        },
                        domProps: { value: _vm.$v.formdata.description.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Max character is " +
                                _vm._s(
                                  _vm.$v.formdata.description.$params.maxLength
                                    .max
                                )
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "py-3 pl-2 mb-3 font-weight-bold",
                        staticStyle: {
                          background:
                            "linear-gradient(to right, #da8cff, #9a55ff)"
                        }
                      },
                      [_vm._v("Meta Tag")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_title.$model,
                            expression: "$v.formdata.seo_title.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_title.$error
                        },
                        attrs: {
                          type: "text",
                          id: "meta_title",
                          placeholder: "SEO title"
                        },
                        domProps: { value: _vm.$v.formdata.seo_title.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_title,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_title.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Seo title must not have more than " +
                                _vm._s(
                                  _vm.$v.formdata.seo_title.$params.maxLength
                                    .max
                                ) +
                                " letters."
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_keywords.$model,
                            expression: "$v.formdata.seo_keywords.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_keywords.$error
                        },
                        attrs: {
                          id: "meta_keywords",
                          rows: "4",
                          placeholder: "SEO keywords"
                        },
                        domProps: {
                          value: _vm.$v.formdata.seo_keywords.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_keywords,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_keywords.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Seo keywords must not have more than " +
                                _vm._s(
                                  _vm.$v.formdata.seo_keywords.$params.maxLength
                                    .max
                                ) +
                                " letters."
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_description.$model,
                            expression: "$v.formdata.seo_description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_description.$error
                        },
                        attrs: {
                          id: "meta_description",
                          rows: "4",
                          placeholder: "SEO description"
                        },
                        domProps: {
                          value: _vm.$v.formdata.seo_description.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "Short description must not have more than " +
                                _vm._s(
                                  _vm.$v.formdata.seo_description.$params
                                    .maxLength.max
                                ) +
                                " letters."
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-1",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            _vm.id ? _vm.updateCategory() : _vm.addCategory()
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.id ? "Update" : "Add"))]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-7" }, [
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-12 grid-margin stretch-card" },
                        [
                          _c("div", { staticClass: "card" }, [
                            _c("div", [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-sm-12" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.search,
                                          expression: "search"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "search",
                                        placeholder: "Search tag here"
                                      },
                                      domProps: { value: _vm.search },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.search = $event.target.value
                                        }
                                      }
                                    })
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              !_vm.data.data || !_vm.data.data.length
                                ? _c(
                                    "div",
                                    { staticClass: "alert alert-danger" },
                                    [
                                      _vm._v(
                                        "\n                                  No records found.\n                                "
                                      )
                                    ]
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _c(
                                "div",
                                {},
                                [
                                  _c(
                                    "div",
                                    { staticClass: "float-right py-2" },
                                    [
                                      _vm._v(
                                        "\n                                        " +
                                          _vm._s(_vm.data.from) +
                                          " - " +
                                          _vm._s(_vm.data.to) +
                                          " of " +
                                          _vm._s(_vm.data.total) +
                                          " record(s) are showing.\n                                      "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "card-description form-group w-50"
                                    },
                                    [
                                      _c("div", { staticClass: "row" }, [
                                        _c("div", { staticClass: "col-sm-6" }, [
                                          _c(
                                            "select",
                                            {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.action,
                                                  expression: "action"
                                                }
                                              ],
                                              staticClass: "form-control",
                                              staticStyle: { padding: "10px" },
                                              on: {
                                                change: [
                                                  function($event) {
                                                    var $$selectedVal = Array.prototype.filter
                                                      .call(
                                                        $event.target.options,
                                                        function(o) {
                                                          return o.selected
                                                        }
                                                      )
                                                      .map(function(o) {
                                                        var val =
                                                          "_value" in o
                                                            ? o._value
                                                            : o.value
                                                        return val
                                                      })
                                                    _vm.action = $event.target
                                                      .multiple
                                                      ? $$selectedVal
                                                      : $$selectedVal[0]
                                                  },
                                                  _vm.multipalDelete
                                                ]
                                              }
                                            },
                                            [
                                              _c(
                                                "option",
                                                { attrs: { value: "" } },
                                                [_vm._v("Bulk Action")]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "option",
                                                { attrs: { value: "Delete" } },
                                                [_vm._v("Delete")]
                                              )
                                            ]
                                          )
                                        ])
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _vm.data.data && _vm.data.data.length
                                    ? _c(
                                        "table",
                                        { staticClass: "table table-striped" },
                                        [
                                          _c(
                                            "thead",
                                            {
                                              staticClass: "bg-dark text-light"
                                            },
                                            [
                                              _c("tr", [
                                                _c("th", [
                                                  _c("label", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value: _vm.all_select,
                                                          expression:
                                                            "all_select"
                                                        }
                                                      ],
                                                      attrs: {
                                                        type: "checkbox"
                                                      },
                                                      domProps: {
                                                        checked: Array.isArray(
                                                          _vm.all_select
                                                        )
                                                          ? _vm._i(
                                                              _vm.all_select,
                                                              null
                                                            ) > -1
                                                          : _vm.all_select
                                                      },
                                                      on: {
                                                        click:
                                                          _vm.select_all_via_check_box,
                                                        change: function(
                                                          $event
                                                        ) {
                                                          var $$a =
                                                              _vm.all_select,
                                                            $$el =
                                                              $event.target,
                                                            $$c = $$el.checked
                                                              ? true
                                                              : false
                                                          if (
                                                            Array.isArray($$a)
                                                          ) {
                                                            var $$v = null,
                                                              $$i = _vm._i(
                                                                $$a,
                                                                $$v
                                                              )
                                                            if ($$el.checked) {
                                                              $$i < 0 &&
                                                                (_vm.all_select = $$a.concat(
                                                                  [$$v]
                                                                ))
                                                            } else {
                                                              $$i > -1 &&
                                                                (_vm.all_select = $$a
                                                                  .slice(0, $$i)
                                                                  .concat(
                                                                    $$a.slice(
                                                                      $$i + 1
                                                                    )
                                                                  ))
                                                            }
                                                          } else {
                                                            _vm.all_select = $$c
                                                          }
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("span", [
                                                      _vm._v(
                                                        " " +
                                                          _vm._s(
                                                            _vm.all_select ==
                                                              true
                                                              ? "Uncheck All"
                                                              : "Select All"
                                                          ) +
                                                          " "
                                                      )
                                                    ])
                                                  ])
                                                ]),
                                                _vm._v(" "),
                                                _c("th", [_vm._v(" Name ")]),
                                                _vm._v(" "),
                                                _c("th", [
                                                  _vm._v(" Description ")
                                                ]),
                                                _vm._v(" "),
                                                _c("th")
                                              ])
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "tbody",
                                            _vm._l(_vm.data.data, function(
                                              page,
                                              i
                                            ) {
                                              return _c("tr", { key: i }, [
                                                _c("td", [
                                                  _c("label", [
                                                    _c("input", {
                                                      directives: [
                                                        {
                                                          name: "model",
                                                          rawName: "v-model",
                                                          value:
                                                            _vm.deleteItems,
                                                          expression:
                                                            "deleteItems"
                                                        }
                                                      ],
                                                      attrs: {
                                                        type: "checkbox"
                                                      },
                                                      domProps: {
                                                        value: page.id,
                                                        checked: Array.isArray(
                                                          _vm.deleteItems
                                                        )
                                                          ? _vm._i(
                                                              _vm.deleteItems,
                                                              page.id
                                                            ) > -1
                                                          : _vm.deleteItems
                                                      },
                                                      on: {
                                                        change: function(
                                                          $event
                                                        ) {
                                                          var $$a =
                                                              _vm.deleteItems,
                                                            $$el =
                                                              $event.target,
                                                            $$c = $$el.checked
                                                              ? true
                                                              : false
                                                          if (
                                                            Array.isArray($$a)
                                                          ) {
                                                            var $$v = page.id,
                                                              $$i = _vm._i(
                                                                $$a,
                                                                $$v
                                                              )
                                                            if ($$el.checked) {
                                                              $$i < 0 &&
                                                                (_vm.deleteItems = $$a.concat(
                                                                  [$$v]
                                                                ))
                                                            } else {
                                                              $$i > -1 &&
                                                                (_vm.deleteItems = $$a
                                                                  .slice(0, $$i)
                                                                  .concat(
                                                                    $$a.slice(
                                                                      $$i + 1
                                                                    )
                                                                  ))
                                                            }
                                                          } else {
                                                            _vm.deleteItems = $$c
                                                          }
                                                        }
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("span", [
                                                      _vm._v(
                                                        " " +
                                                          _vm._s(
                                                            i + _vm.data.from
                                                          ) +
                                                          ". "
                                                      )
                                                    ])
                                                  ])
                                                ]),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _vm._v(
                                                    _vm._s(page.name) + " "
                                                  )
                                                ]),
                                                _vm._v(" "),
                                                _c(
                                                  "td",
                                                  {
                                                    staticClass: "py-1",
                                                    staticStyle: {
                                                      "white-space": "normal"
                                                    }
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                                              " +
                                                        _vm._s(
                                                          page.description
                                                        ) +
                                                        "\n                                            "
                                                    )
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c("td", [
                                                  _c(
                                                    "div",
                                                    { staticClass: "mt-2" },
                                                    [
                                                      _c(
                                                        "button",
                                                        {
                                                          staticClass:
                                                            "btn btn-link p-0",
                                                          attrs: {
                                                            type: "button",
                                                            name: "button"
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.deleteRow(
                                                                page.id
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("i", {
                                                            staticClass:
                                                              "mdi mdi-delete text-danger"
                                                          }),
                                                          _vm._v(" Delete")
                                                        ]
                                                      ),
                                                      _vm._v(
                                                        "\n                                                | "
                                                      ),
                                                      _c(
                                                        "router-link",
                                                        {
                                                          staticClass:
                                                            "btn btn-link p-0",
                                                          attrs: {
                                                            to: {
                                                              name: "EditTag",
                                                              params: {
                                                                Tagid: page.id
                                                              }
                                                            }
                                                          }
                                                        },
                                                        [
                                                          _c("i", {
                                                            staticClass:
                                                              "mdi mdi-pencil text-info"
                                                          }),
                                                          _vm._v(" Edit ")
                                                        ]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ])
                                              ])
                                            }),
                                            0
                                          )
                                        ]
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _c("pagination", {
                                    attrs: { data: _vm.data },
                                    on: { "pagination-change-page": _vm.getTag }
                                  })
                                ],
                                1
                              )
                            ])
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/tag/Add.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/backend/tag/Add.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Add.vue?vue&type=template&id=0aaa0416& */ "./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416&");
/* harmony import */ var _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Add.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Add.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/tag/Add.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=template&id=0aaa0416& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/tag/Add.vue?vue&type=template&id=0aaa0416&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_0aaa0416___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);