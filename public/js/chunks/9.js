(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_picture_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-picture-input */ "./node_modules/vue-picture-input/PictureInput.vue");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
var _watch;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PagesComponent",
  components: {
    PictureInput: vue_picture_input__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      id: "",
      data: {},
      count: {},
      formdata: {
        name: "",
        slug: "",
        description: "",
        seo_title: "",
        seo_keywords: "",
        seo_description: ""
      },
      image: "",
      image_url: "",
      setClass: "",
      deleteItems: [],
      all_select: false,
      search: "",
      action: "",
      token: localStorage.getItem("token"),
      tinyOptions: {
        height: 300,
        branding: false
      }
    };
  },
  validations: {
    formdata: {
      name: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["required"]
      },
      description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(500)
      },
      slug: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["required"],
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(75)
      },
      seo_title: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(100)
      },
      seo_keywords: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(150)
      },
      seo_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__["maxLength"])(250)
      }
    }
  },
  mounted: function mounted() {
    var id = this.$route.params.Cid;

    if (typeof id != "undefined") {
      this.id = id;
    }

    this.applymounted();
  },
  methods: {
    searchList: lodash__WEBPACK_IMPORTED_MODULE_0___default.a.debounce(function () {
      this.getCategory(1);
    }, 500),
    applymounted: function applymounted() {
      if (this.id != "" && this.id != undefined) {
        this.singleRecord();
      }

      this.getCategory();
    },
    getCategory: function getCategory() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.data = {};
      var header = {
        Authorization: "Bearer " + this.token
      };
      var apiUrl = "".concat(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY"], "/?page=").concat(page, "&s=").concat(this.search, "&type=").concat(this.$route.params.Type);
      if (this.$route.query.trashed) apiUrl += "&trashed=".concat(this.$route.query.trashed);
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["GET"])(apiUrl, header).then(function (res) {
        _this.data = res.data.categories;
        _this.count = res.data.count;
      });
    },
    singleRecord: function singleRecord() {
      var _this2 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY"] + "/" + this.id, header).then(function (res) {
        _this2.formdata = res.data;
        _this2.image_url = res.data.image_url_prefill;
      });
    },
    addCategory: function addCategory() {
      var _this3 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append("record[name]", this.formdata.name);
        formData.append("record[slug]", this.formdata.slug);
        formData.append("record[description]", this.formdata.description);
        formData.append("record[category_type_id]", this.$route.params.Type);
        formData.append("record[seo_title]", this.formdata.seo_title);
        formData.append("record[seo_keywords]", this.formdata.seo_keywords);
        formData.append("record[seo_description]", this.formdata.seo_description);
        formData.append("image", this.image);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY_ADD"], formData, header).then(function (res) {
          if (res.data.status == 2) {
            _this3.setClass = "setcolor";
          }

          _this3.$swal(res.data.message1, res.data.message, "Ok").then(function (result) {
            if (res.data.status == 1) {
              _this3.formdata = {
                name: "",
                slug: "",
                description: "",
                seo_title: "",
                seo_keywords: "",
                seo_description: ""
              };

              _this3.$v.$reset();

              _this3.image = "";
              _this3.image_url = "";

              _this3.getCategory(1);
            }
          });
        });
      }
    },
    updateCategory: function updateCategory() {
      var _this4 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append("_method", "PUT");
        formData.append("record[name]", this.formdata.name);
        formData.append("record[slug]", this.formdata.slug);
        formData.append("record[description]", this.formdata.description);
        formData.append("record[seo_title]", this.formdata.seo_title);
        formData.append("record[seo_keywords]", this.formdata.seo_keywords);
        formData.append("record[seo_description]", this.formdata.seo_description);
        formData.append("image", this.image);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY"] + "/" + this.id, formData, header).then(function (res) {
          if (res.data.status == 2) {
            _this4.setClass = "setcolor";
          }

          _this4.$swal(res.data.message1, res.data.message, "Ok").then(function (result) {
            if (res.data.status == 1) {
              _this4.image_url = "";
              _this4.image = "";

              _this4.getCategory(1);

              _this4.formdata = {
                name: "",
                slug: "",
                description: "",
                seo_title: "",
                seo_keywords: "",
                seo_description: ""
              };

              _this4.$v.$reset();

              _this4.$router.push({
                name: "AddCategory"
              });
            }
          });
        });
      }
    },
    onChanged: function onChanged() {
      // console.log('new picture loaded', this.$refs.pictureInput.file);
      if (this.$refs.pictureInput.file) {
        this.image = this.$refs.pictureInput.file;
      } else {// console.log("old browser. No support for FilereaderApi");
      } // console.log(this.image);

    },
    onRemoved: function onRemoved() {
      this.formdata.image = ""; // console.log(this.formdata.image);
    },
    deleteRow: function deleteRow(id) {
      var _this5 = this;

      var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "delete";
      var msg = "You will not be able to recover this record!",
          delete_record = "Yes, delete it!",
          type = "warning";

      switch (action) {
        case "restore":
          msg = "You're going to restore this item";
          delete_record = "Yes, restore", type = "success";
          break;
      }

      this.$swal({
        title: "Are you sure?",
        text: msg,
        icon: type,
        showCancelButton: true,
        confirmButtonText: delete_record
      }).then(function (result) {
        if (result.value) {
          var header = {
            Authorization: "Bearer " + _this5.token
          };
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["DELETE"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY"] + "/" + id + "/?action=" + action, header).then(function (res) {
            _this5.getCategory(1);
          });
        }
      });
    },
    multipalDelete: function multipalDelete() {
      var _this6 = this;

      if (this.action != "") {
        var header = {
          Authorization: "Bearer " + this.token
        };

        if (this.deleteItems.length == 0) {
          this.$swal("Warning", "please select an item", "Ok");
        } else {
          var msg = "You will not be able to recover this record!",
              delete_record = "Yes, delete it!",
              type = "warning";

          switch (this.action) {
            case "Restore":
              msg = "You're going to restore this item";
              delete_record = "Yes, restore", type = "success";
              break;
          }

          this.$swal({
            title: "Are you sure?",
            text: msg,
            icon: type,
            showCancelButton: true,
            confirmButtonText: delete_record
          }).then(function (result) {
            if (result.value) {
              var formData = new FormData();
              formData.append("ids", _this6.deleteItems);
              formData.append("action", _this6.action);
              Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_CATEGORY_DELETE"], formData, header).then(function (res) {
                if (res.data.status) {
                  _this6.deleteItems = [];
                  _this6.all_select = false;
                  _this6.action = "";

                  _this6.getCategory();
                }
              });
            } else {
              _this6.action = "";
            }
          });
        }
      }
    },
    select_all_via_check_box: function select_all_via_check_box() {
      var _this7 = this;

      if (this.all_select == false) {
        this.all_select = true;
        this.deleteItems = [];
        this.data.data.forEach(function (item) {
          _this7.deleteItems.push(item.id);
        });
      } else {
        this.all_select = false;
        this.deleteItems = [];
      }
    }
  },
  computed: {
    slug: function slug() {
      var slug = this.formdata.name.toString().trim().toLowerCase() // LowerCase
      .replace(/\s+/g, "-") // space to -
      .replace(/&/g, "-and-") // & to and
      .replace(/\?/g, "") // & to and
      .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
      return slug;
    }
  },
  watch: (_watch = {
    slug: function slug() {
      this.formdata.slug = this.slug;
    },
    search: function search() {
      this.searchList();
    },
    deleteItems: function deleteItems() {
      if (this.data && this.data.data.length == this.deleteItems.length) {
        this.all_select = true;
      } else {
        this.all_select = false;
      }
    },
    "$route.params.Type": function $routeParamsType(type) {
      var id = this.$route.params.Cid;

      if (typeof id == "undefined") {
        this.formdata = {
          name: "",
          slug: "",
          description: "",
          seo_title: "",
          seo_keywords: "",
          seo_description: ""
        };
        this.image = "";
        this.image_url = "";
        this.id = "";
        this.setClass = "";
      } else {
        this.id = id;
        this.applymounted();
      }
    },
    "$route.params.Cid": function $routeParamsCid(id) {
      if (typeof id == "undefined") {
        this.formdata = {
          name: "",
          slug: "",
          description: "",
          seo_title: "",
          seo_keywords: "",
          seo_description: ""
        };
        this.image = "";
        this.image_url = "";
        this.id = "";
        this.setClass = "";
      } else {
        this.id = id;
        this.applymounted();
      }
    },
    "formdata.slug": function formdataSlug(slug) {
      this.formdata.slug = slug.toString().toLowerCase() // LowerCase
      .replace(/\s+/g, "-") // space to -
      .replace(/&/g, "-and-") // & to and
      .replace(/\?/g, "") // & to and
      .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-");
    }
  }, _defineProperty(_watch, "$route.params.Type", function $routeParamsType() {
    this.id = "";
    this.applymounted();
  }), _defineProperty(_watch, "$route.query", function $routeQuery() {
    this.getCategory();
  }), _watch)
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.form-check-input[type=\"checkbox\"] + label,\r\nlabel.btn input[type=\"checkbox\"] + label {\r\n  position: relative;\r\n  display: inline-block;\r\n  height: 1.5625rem;\r\n  padding-left: 35px;\r\n  line-height: 1.5625rem;\r\n  cursor: pointer;\r\n  -webkit-user-select: none;\r\n  -moz-user-select: none;\r\n  -ms-user-select: none;\r\n  user-select: none;\n}\n.custom-control {\r\n  position: relative;\r\n  z-index: 1;\r\n  display: inline;\r\n  min-height: 1.5rem;\r\n  padding-left: 1.5rem;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c(
        "h3",
        { staticClass: "page-title" },
        [
          _vm._v(
            "\n      " +
              _vm._s(_vm.id ? "Edit" : "Add") +
              " new category\n      "
          ),
          _vm.id
            ? _c(
                "router-link",
                {
                  staticClass: "btn btn-sm btn-dark text-light text-left",
                  attrs: { to: { name: "AddCategory" }, name: "button" }
                },
                [_vm._v("Add new\n      ")]
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c(
                "router-link",
                {
                  staticStyle: { "text-transform": "capitalize" },
                  attrs: {
                    to: {
                      name:
                        _vm.$route.params.Type == "service"
                          ? "ServiceMaster"
                          : _vm.$route.params.Type == "post"
                          ? "BlogMaster"
                          : "PortfolioMaster"
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$route.params.Type))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [
              _vm._v(
                "\n          " +
                  _vm._s(_vm.id ? "Edit" : "Add") +
                  " category\n        "
              )
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.name.$model,
                            expression: "$v.formdata.name.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.name.$error },
                        attrs: {
                          type: "text",
                          id: "page_title",
                          placeholder: "Category name"
                        },
                        domProps: { value: _vm.$v.formdata.name.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.name,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.name.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.formdata.slug,
                            expression: "formdata.slug"
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.slug.$error },
                        attrs: {
                          type: "text",
                          maxlength: "75",
                          id: "page_slug",
                          placeholder: "Slug"
                        },
                        domProps: { value: _vm.formdata.slug },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.formdata, "slug", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Max length is " +
                                _vm._s(
                                  _vm.$v.formdata.slug.$params.maxLength.max
                                ) +
                                "\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-sm-8" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model.trim",
                                value: _vm.$v.formdata.description.$model,
                                expression: "$v.formdata.description.$model",
                                modifiers: { trim: true }
                              }
                            ],
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.$v.formdata.description.$error
                            },
                            attrs: {
                              id: "description",
                              rows: "11",
                              placeholder: "Category description"
                            },
                            domProps: {
                              value: _vm.$v.formdata.description.$model
                            },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.$v.formdata.description,
                                  "$model",
                                  $event.target.value.trim()
                                )
                              },
                              blur: function($event) {
                                return _vm.$forceUpdate()
                              }
                            }
                          }),
                          _vm._v(" "),
                          !_vm.$v.formdata.description.maxLength
                            ? _c("div", { staticClass: "invalid-feedback" }, [
                                _vm._v(
                                  "\n                        Max character is\n                        " +
                                    _vm._s(
                                      _vm.$v.formdata.description.$params
                                        .maxLength.max
                                    ) +
                                    "\n                      "
                                )
                              ])
                            : _vm._e()
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4 mb-1" },
                        [
                          _c("picture-input", {
                            ref: "pictureInput",
                            attrs: {
                              zIndex: -1,
                              crop: false,
                              removable: false,
                              width: 364,
                              height: 280,
                              removeButtonClass: "btn btn-danger",
                              accept: "image/*",
                              prefill: _vm.image_url,
                              buttonClass: "btn btn-primary",
                              customStrings: {
                                upload: "<h1>Upload it!</h1>",
                                drag: "Drag and drop your image here"
                              }
                            },
                            on: { change: _vm.onChanged, remove: _vm.onRemoved }
                          })
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "py-3 pl-2 mb-3 font-weight-bold",
                        staticStyle: {
                          background:
                            "linear-gradient(to right, #da8cff, #9a55ff)"
                        }
                      },
                      [_vm._v("\n                  Meta Tag\n                ")]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_title.$model,
                            expression: "$v.formdata.seo_title.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_title.$error
                        },
                        attrs: {
                          type: "text",
                          id: "meta_title",
                          placeholder: "SEO title"
                        },
                        domProps: { value: _vm.$v.formdata.seo_title.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_title,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_title.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Seo title must not have more than\n                    " +
                                _vm._s(
                                  _vm.$v.formdata.seo_title.$params.maxLength
                                    .max
                                ) +
                                " letters.\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_keywords.$model,
                            expression: "$v.formdata.seo_keywords.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_keywords.$error
                        },
                        attrs: {
                          id: "meta_keywords",
                          rows: "4",
                          placeholder: "SEO keywords"
                        },
                        domProps: {
                          value: _vm.$v.formdata.seo_keywords.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_keywords,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_keywords.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Seo keywords must not have more than\n                    " +
                                _vm._s(
                                  _vm.$v.formdata.seo_keywords.$params.maxLength
                                    .max
                                ) +
                                "\n                    letters.\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.seo_description.$model,
                            expression: "$v.formdata.seo_description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.seo_description.$error
                        },
                        attrs: {
                          id: "meta_description",
                          rows: "4",
                          placeholder: "SEO description"
                        },
                        domProps: {
                          value: _vm.$v.formdata.seo_description.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.seo_description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.seo_description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Short description must not have more than\n                    " +
                                _vm._s(
                                  _vm.$v.formdata.seo_description.$params
                                    .maxLength.max
                                ) +
                                "\n                    letters.\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-1",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            _vm.id ? _vm.updateCategory() : _vm.addCategory()
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n                  " +
                            _vm._s(_vm.id ? "Update" : "Add") +
                            "\n                "
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "row" }, [
                      _c(
                        "div",
                        { staticClass: "col-12 grid-margin stretch-card" },
                        [
                          _c("div", { staticClass: "card" }, [
                            _c("div", [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-sm-12" }, [
                                  _c("div", { staticClass: "form-group" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.search,
                                          expression: "search"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        type: "search",
                                        placeholder: "Search category here"
                                      },
                                      domProps: { value: _vm.search },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.search = $event.target.value
                                        }
                                      }
                                    })
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              !_vm.data.data
                                ? _c(
                                    "div",
                                    { staticClass: "text-center my-3" },
                                    [
                                      _c("b-spinner", {
                                        staticStyle: {
                                          width: "50",
                                          height: "50"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                : _vm._e(),
                              _vm._v(" "),
                              _vm.data.data
                                ? _c("div", [
                                    _c(
                                      "div",
                                      { staticClass: "mb-3" },
                                      [
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "AddCategory"
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "All (" +
                                                _vm._s(_vm.count.all) +
                                                ")"
                                            )
                                          ]
                                        ),
                                        _vm._v(
                                          "\n                            |\n                            "
                                        ),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: {
                                                name: "AddCategory",
                                                query: {
                                                  trashed: "true"
                                                }
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "Trashed (" +
                                                _vm._s(_vm.count.trash) +
                                                ")"
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    !_vm.data.data.length
                                      ? _c(
                                          "div",
                                          { staticClass: "alert alert-danger" },
                                          [
                                            _vm._v(
                                              "\n                            No records found.\n                          "
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm.data.data.length
                                      ? _c(
                                          "div",
                                          [
                                            _c(
                                              "div",
                                              {
                                                staticClass: "float-right py-2"
                                              },
                                              [
                                                _vm._v(
                                                  "\n                              " +
                                                    _vm._s(_vm.data.from) +
                                                    " - " +
                                                    _vm._s(_vm.data.to) +
                                                    " of\n                              " +
                                                    _vm._s(_vm.data.total) +
                                                    " record(s) are showing.\n                            "
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "card-description form-group w-50"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "row" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass: "col-sm-6"
                                                      },
                                                      [
                                                        _c(
                                                          "select",
                                                          {
                                                            directives: [
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  _vm.action,
                                                                expression:
                                                                  "action"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "form-control",
                                                            staticStyle: {
                                                              padding: "10px"
                                                            },
                                                            on: {
                                                              change: [
                                                                function(
                                                                  $event
                                                                ) {
                                                                  var $$selectedVal = Array.prototype.filter
                                                                    .call(
                                                                      $event
                                                                        .target
                                                                        .options,
                                                                      function(
                                                                        o
                                                                      ) {
                                                                        return o.selected
                                                                      }
                                                                    )
                                                                    .map(
                                                                      function(
                                                                        o
                                                                      ) {
                                                                        var val =
                                                                          "_value" in
                                                                          o
                                                                            ? o._value
                                                                            : o.value
                                                                        return val
                                                                      }
                                                                    )
                                                                  _vm.action = $event
                                                                    .target
                                                                    .multiple
                                                                    ? $$selectedVal
                                                                    : $$selectedVal[0]
                                                                },
                                                                _vm.multipalDelete
                                                              ]
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "option",
                                                              {
                                                                attrs: {
                                                                  value: ""
                                                                }
                                                              },
                                                              [
                                                                _vm._v(
                                                                  "Bulk Action"
                                                                )
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            !_vm.$route.query
                                                              .trashed
                                                              ? _c(
                                                                  "option",
                                                                  {
                                                                    attrs: {
                                                                      value:
                                                                        "Delete"
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "\n                                      Delete\n                                    "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e(),
                                                            _vm._v(" "),
                                                            _vm.$route.query
                                                              .trashed
                                                              ? _c(
                                                                  "option",
                                                                  {
                                                                    attrs: {
                                                                      value:
                                                                        "PDelete"
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "\n                                      Delete Permanently\n                                    "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e(),
                                                            _vm._v(" "),
                                                            _vm.$route.query
                                                              .trashed
                                                              ? _c(
                                                                  "option",
                                                                  {
                                                                    attrs: {
                                                                      value:
                                                                        "Restore"
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      "\n                                      Restore\n                                    "
                                                                    )
                                                                  ]
                                                                )
                                                              : _vm._e()
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _vm.data.data.length
                                              ? _c(
                                                  "table",
                                                  {
                                                    staticClass:
                                                      "table table-striped"
                                                  },
                                                  [
                                                    _c(
                                                      "thead",
                                                      {
                                                        staticClass:
                                                          "bg-dark text-light"
                                                      },
                                                      [
                                                        _c("tr", [
                                                          _c("th", [
                                                            _c("label", [
                                                              _c("input", {
                                                                directives: [
                                                                  {
                                                                    name:
                                                                      "model",
                                                                    rawName:
                                                                      "v-model",
                                                                    value:
                                                                      _vm.all_select,
                                                                    expression:
                                                                      "all_select"
                                                                  }
                                                                ],
                                                                attrs: {
                                                                  type:
                                                                    "checkbox"
                                                                },
                                                                domProps: {
                                                                  checked: Array.isArray(
                                                                    _vm.all_select
                                                                  )
                                                                    ? _vm._i(
                                                                        _vm.all_select,
                                                                        null
                                                                      ) > -1
                                                                    : _vm.all_select
                                                                },
                                                                on: {
                                                                  click:
                                                                    _vm.select_all_via_check_box,
                                                                  change: function(
                                                                    $event
                                                                  ) {
                                                                    var $$a =
                                                                        _vm.all_select,
                                                                      $$el =
                                                                        $event.target,
                                                                      $$c = $$el.checked
                                                                        ? true
                                                                        : false
                                                                    if (
                                                                      Array.isArray(
                                                                        $$a
                                                                      )
                                                                    ) {
                                                                      var $$v = null,
                                                                        $$i = _vm._i(
                                                                          $$a,
                                                                          $$v
                                                                        )
                                                                      if (
                                                                        $$el.checked
                                                                      ) {
                                                                        $$i <
                                                                          0 &&
                                                                          (_vm.all_select = $$a.concat(
                                                                            [
                                                                              $$v
                                                                            ]
                                                                          ))
                                                                      } else {
                                                                        $$i >
                                                                          -1 &&
                                                                          (_vm.all_select = $$a
                                                                            .slice(
                                                                              0,
                                                                              $$i
                                                                            )
                                                                            .concat(
                                                                              $$a.slice(
                                                                                $$i +
                                                                                  1
                                                                              )
                                                                            ))
                                                                      }
                                                                    } else {
                                                                      _vm.all_select = $$c
                                                                    }
                                                                  }
                                                                }
                                                              }),
                                                              _vm._v(" "),
                                                              _c("span", [
                                                                _vm._v(
                                                                  "\n                                        " +
                                                                    _vm._s(
                                                                      _vm.all_select ==
                                                                        true
                                                                        ? "Uncheck All"
                                                                        : "Select All"
                                                                    ) +
                                                                    "\n                                      "
                                                                )
                                                              ])
                                                            ])
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("th", [
                                                            _vm._v("Image")
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("th", [
                                                            _vm._v("Name")
                                                          ]),
                                                          _vm._v(" "),
                                                          _c("th")
                                                        ])
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "tbody",
                                                      _vm._l(
                                                        _vm.data.data,
                                                        function(page, i) {
                                                          return _c(
                                                            "tr",
                                                            { key: i },
                                                            [
                                                              _c("td", [
                                                                _c("label", [
                                                                  _c("input", {
                                                                    directives: [
                                                                      {
                                                                        name:
                                                                          "model",
                                                                        rawName:
                                                                          "v-model",
                                                                        value:
                                                                          _vm.deleteItems,
                                                                        expression:
                                                                          "deleteItems"
                                                                      }
                                                                    ],
                                                                    attrs: {
                                                                      type:
                                                                        "checkbox"
                                                                    },
                                                                    domProps: {
                                                                      value:
                                                                        page.id,
                                                                      checked: Array.isArray(
                                                                        _vm.deleteItems
                                                                      )
                                                                        ? _vm._i(
                                                                            _vm.deleteItems,
                                                                            page.id
                                                                          ) > -1
                                                                        : _vm.deleteItems
                                                                    },
                                                                    on: {
                                                                      change: function(
                                                                        $event
                                                                      ) {
                                                                        var $$a =
                                                                            _vm.deleteItems,
                                                                          $$el =
                                                                            $event.target,
                                                                          $$c = $$el.checked
                                                                            ? true
                                                                            : false
                                                                        if (
                                                                          Array.isArray(
                                                                            $$a
                                                                          )
                                                                        ) {
                                                                          var $$v =
                                                                              page.id,
                                                                            $$i = _vm._i(
                                                                              $$a,
                                                                              $$v
                                                                            )
                                                                          if (
                                                                            $$el.checked
                                                                          ) {
                                                                            $$i <
                                                                              0 &&
                                                                              (_vm.deleteItems = $$a.concat(
                                                                                [
                                                                                  $$v
                                                                                ]
                                                                              ))
                                                                          } else {
                                                                            $$i >
                                                                              -1 &&
                                                                              (_vm.deleteItems = $$a
                                                                                .slice(
                                                                                  0,
                                                                                  $$i
                                                                                )
                                                                                .concat(
                                                                                  $$a.slice(
                                                                                    $$i +
                                                                                      1
                                                                                  )
                                                                                ))
                                                                          }
                                                                        } else {
                                                                          _vm.deleteItems = $$c
                                                                        }
                                                                      }
                                                                    }
                                                                  }),
                                                                  _vm._v(" "),
                                                                  _c("span", [
                                                                    _vm._v(
                                                                      " " +
                                                                        _vm._s(
                                                                          i +
                                                                            _vm
                                                                              .data
                                                                              .from
                                                                        ) +
                                                                        ". "
                                                                    )
                                                                  ])
                                                                ])
                                                              ]),
                                                              _vm._v(" "),
                                                              _c(
                                                                "td",
                                                                {
                                                                  staticClass:
                                                                    "py-1"
                                                                },
                                                                [
                                                                  _c("img", {
                                                                    attrs: {
                                                                      src:
                                                                        page
                                                                          .image_url
                                                                          .thumb,
                                                                      alt:
                                                                        "image"
                                                                    }
                                                                  })
                                                                ]
                                                              ),
                                                              _vm._v(" "),
                                                              _c("td", [
                                                                _vm._v(
                                                                  _vm._s(
                                                                    page.name
                                                                  )
                                                                )
                                                              ]),
                                                              _vm._v(" "),
                                                              _c("td", [
                                                                _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "mt-2"
                                                                  },
                                                                  [
                                                                    _vm.$route
                                                                      .query
                                                                      .trashed
                                                                      ? _c(
                                                                          "button",
                                                                          {
                                                                            staticClass:
                                                                              "btn btn-link p-0",
                                                                            attrs: {
                                                                              type:
                                                                                "button",
                                                                              name:
                                                                                "button"
                                                                            },
                                                                            on: {
                                                                              click: function(
                                                                                $event
                                                                              ) {
                                                                                return _vm.deleteRow(
                                                                                  page.id,
                                                                                  "delete-permanent"
                                                                                )
                                                                              }
                                                                            }
                                                                          },
                                                                          [
                                                                            _c(
                                                                              "i",
                                                                              {
                                                                                staticClass:
                                                                                  "mdi mdi-delete text-danger"
                                                                              }
                                                                            ),
                                                                            _vm._v(
                                                                              "\n                                        Delete Permanent\n                                      "
                                                                            )
                                                                          ]
                                                                        )
                                                                      : _vm._e(),
                                                                    _vm._v(" "),
                                                                    _vm.$route
                                                                      .query
                                                                      .trashed
                                                                      ? _c(
                                                                          "button",
                                                                          {
                                                                            staticClass:
                                                                              "btn btn-link p-0",
                                                                            attrs: {
                                                                              type:
                                                                                "button",
                                                                              name:
                                                                                "button"
                                                                            },
                                                                            on: {
                                                                              click: function(
                                                                                $event
                                                                              ) {
                                                                                return _vm.deleteRow(
                                                                                  page.id,
                                                                                  "restore"
                                                                                )
                                                                              }
                                                                            }
                                                                          },
                                                                          [
                                                                            _c(
                                                                              "i",
                                                                              {
                                                                                staticClass:
                                                                                  "mdi mdi-undo text-danger"
                                                                              }
                                                                            ),
                                                                            _vm._v(
                                                                              "\n                                        Restore\n                                      "
                                                                            )
                                                                          ]
                                                                        )
                                                                      : _vm._e(),
                                                                    _vm._v(" "),
                                                                    !_vm.$route
                                                                      .query
                                                                      .trashed
                                                                      ? _c(
                                                                          "button",
                                                                          {
                                                                            staticClass:
                                                                              "btn btn-link p-0",
                                                                            attrs: {
                                                                              type:
                                                                                "button",
                                                                              name:
                                                                                "button"
                                                                            },
                                                                            on: {
                                                                              click: function(
                                                                                $event
                                                                              ) {
                                                                                return _vm.deleteRow(
                                                                                  page.id
                                                                                )
                                                                              }
                                                                            }
                                                                          },
                                                                          [
                                                                            _c(
                                                                              "i",
                                                                              {
                                                                                staticClass:
                                                                                  "mdi mdi-delete text-danger"
                                                                              }
                                                                            ),
                                                                            _vm._v(
                                                                              "\n                                        Delete\n                                      "
                                                                            )
                                                                          ]
                                                                        )
                                                                      : _vm._e(),
                                                                    _vm._v(" "),
                                                                    !_vm.$route
                                                                      .query
                                                                      .trashed
                                                                      ? _c(
                                                                          "router-link",
                                                                          {
                                                                            staticClass:
                                                                              "btn btn-link p-0",
                                                                            attrs: {
                                                                              to: {
                                                                                name:
                                                                                  "EditCategory",
                                                                                params: {
                                                                                  Type:
                                                                                    _vm
                                                                                      .$route
                                                                                      .params
                                                                                      .Type,
                                                                                  Cid:
                                                                                    page.id
                                                                                }
                                                                              }
                                                                            }
                                                                          },
                                                                          [
                                                                            _c(
                                                                              "i",
                                                                              {
                                                                                staticClass:
                                                                                  "mdi mdi-pencil text-info"
                                                                              }
                                                                            ),
                                                                            _vm._v(
                                                                              "\n                                        Edit\n                                      "
                                                                            )
                                                                          ]
                                                                        )
                                                                      : _vm._e()
                                                                  ],
                                                                  1
                                                                )
                                                              ])
                                                            ]
                                                          )
                                                        }
                                                      ),
                                                      0
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _c("pagination", {
                                              attrs: { data: _vm.data },
                                              on: {
                                                "pagination-change-page":
                                                  _vm.getCategory
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      : _vm._e()
                                  ])
                                : _vm._e()
                            ])
                          ])
                        ]
                      )
                    ])
                  ])
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/category/Add.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/backend/category/Add.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Add.vue?vue&type=template&id=bcaf1254& */ "./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254&");
/* harmony import */ var _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Add.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Add.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/category/Add.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=template&id=bcaf1254& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/category/Add.vue?vue&type=template&id=bcaf1254&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_bcaf1254___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);