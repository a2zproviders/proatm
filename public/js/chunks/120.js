(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[120],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'FaqView',
  data: function data() {
    return {
      formdata: {
        data: []
      },
      deleteItems: [],
      all_select: false,
      status: [],
      action: '',
      token: localStorage.getItem('token'),
      search: ''
    };
  },
  methods: {
    searchList: lodash__WEBPACK_IMPORTED_MODULE_0___default.a.debounce(function () {
      this.listFaq(1);
    }, 500),
    listFaq: function listFaq() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_FAQS"] + '/?page=' + page + '&s=' + this.search, header).then(function (res) {
        console.log("response", res);
        _this.formdata.data = res.data;
      });
    },
    updateStatus: function updateStatus(i) {
      var id = this.formdata.data.data[i].id;
      var status = this.formdata.data.data[i].is_visible;
      var params = {
        record: {
          is_visible: status
        }
      };
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["PUT"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_FAQS"] + '/' + id, params, header).then(function (res) {// this.listPages()
      });
    },
    deleteRow: function deleteRow(id) {
      var _this2 = this;

      this.$swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this record!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then(function (result) {
        if (result.value) {
          var header = {
            Authorization: "Bearer " + _this2.token
          };
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["DELETE"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_FAQS"] + '/' + id, header).then(function (res) {
            _this2.listFaq(1);
          });
        }
      });
    },
    multipalDelete: function multipalDelete() {
      var _this3 = this;

      if (this.action != 0) {
        var header = {
          Authorization: "Bearer " + this.token
        };

        if (this.deleteItems.length == 0) {
          this.$swal('Warning', 'please select an item', 'Ok');
        } else {
          var formData = new FormData();
          formData.append('ids', this.deleteItems);
          formData.append('action', this.action);
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_FAQ_DELETE"], formData, header).then(function (res) {
            if (res.data.status) {
              _this3.deleteItems = [];
              _this3.all_select = false;
              _this3.action = '';

              _this3.listFaq();
            } else {}
          });
        }
      } else {
        this.$swal('Warning', 'please select a valid action', 'Ok');
      }
    },
    select_all_via_check_box: function select_all_via_check_box() {
      var _this4 = this;

      if (this.all_select == false) {
        this.all_select = true;
        this.deleteItems = [];
        this.formdata.data.data.forEach(function (item) {
          _this4.deleteItems.push(item.id);
        });
      } else {
        this.all_select = false;
        this.deleteItems = [];
      }
    }
  },
  mounted: function mounted() {
    this.listFaq();
  },
  watch: {
    deleteItems: function deleteItems() {
      if (this.formdata.data && this.formdata.data.data.length == this.deleteItems.length) {
        this.all_select = true;
      } else {
        this.all_select = false;
      }
    },
    search: function search() {
      this.searchList();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v(" Faq List ")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "FaqMaster" } } }, [
                _vm._v("Faq")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("Faqs List")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "col-lg-12 grid-margin stretch-card" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-sm-8" }, [
                      _c("div", { staticClass: "form-group" }, [
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.search,
                              expression: "search"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "search",
                            placeholder: "search here..."
                          },
                          domProps: { value: _vm.search },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.search = $event.target.value
                            }
                          }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "col-sm-4 text-right" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "btn btn-sm btn-dark",
                            attrs: { to: { name: "AddFaq" } }
                          },
                          [
                            _c("i", { staticClass: "mdi mdi-plus" }),
                            _vm._v(" Add new ")
                          ]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  !_vm.formdata.data.data || !_vm.formdata.data.data.length
                    ? _c("div", { staticClass: "alert alert-danger" }, [
                        _vm._v(
                          "\n                          No records found.\n                        "
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.formdata.data.data && _vm.formdata.data.data.length
                    ? _c(
                        "div",
                        {},
                        [
                          _c("div", { staticClass: "float-right py-2" }, [
                            _vm._v(
                              "\n                            " +
                                _vm._s(_vm.formdata.data.from) +
                                " - " +
                                _vm._s(_vm.formdata.data.to) +
                                " of " +
                                _vm._s(_vm.formdata.data.total) +
                                " record(s) are showing.\n                          "
                            )
                          ]),
                          _vm._v(" "),
                          _c("p", { staticClass: "card-description" }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.action,
                                    expression: "action"
                                  }
                                ],
                                staticStyle: { padding: "10px" },
                                on: {
                                  change: [
                                    function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.action = $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    },
                                    _vm.multipalDelete
                                  ]
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("Bulk Action")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "Delete" } }, [
                                  _vm._v("Delete")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "Enable" } }, [
                                  _vm._v("Enable")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "Disable" } }, [
                                  _vm._v("Disable")
                                ])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("table", { staticClass: "table table-striped" }, [
                            _c("thead", { staticClass: "bg-dark text-light" }, [
                              _c("tr", [
                                _c("th", [
                                  _c("label", [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.all_select,
                                          expression: "all_select"
                                        }
                                      ],
                                      attrs: { type: "checkbox" },
                                      domProps: {
                                        checked: Array.isArray(_vm.all_select)
                                          ? _vm._i(_vm.all_select, null) > -1
                                          : _vm.all_select
                                      },
                                      on: {
                                        click: _vm.select_all_via_check_box,
                                        change: function($event) {
                                          var $$a = _vm.all_select,
                                            $$el = $event.target,
                                            $$c = $$el.checked ? true : false
                                          if (Array.isArray($$a)) {
                                            var $$v = null,
                                              $$i = _vm._i($$a, $$v)
                                            if ($$el.checked) {
                                              $$i < 0 &&
                                                (_vm.all_select = $$a.concat([
                                                  $$v
                                                ]))
                                            } else {
                                              $$i > -1 &&
                                                (_vm.all_select = $$a
                                                  .slice(0, $$i)
                                                  .concat($$a.slice($$i + 1)))
                                            }
                                          } else {
                                            _vm.all_select = $$c
                                          }
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("span", [
                                      _vm._v(
                                        " " +
                                          _vm._s(
                                            _vm.all_select == true
                                              ? "Uncheck All"
                                              : "Select All"
                                          ) +
                                          " "
                                      )
                                    ])
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("th", [_vm._v(" Question ")]),
                                _vm._v(" "),
                                _c("th", [_vm._v(" Answer ")]),
                                _vm._v(" "),
                                _c("th", [_vm._v(" Visibility ")])
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(_vm.formdata.data.data, function(
                                faqlist,
                                i
                              ) {
                                return _c("tr", { key: i }, [
                                  _c("td", [
                                    _c("label", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: _vm.deleteItems,
                                            expression: "deleteItems"
                                          }
                                        ],
                                        attrs: { type: "checkbox" },
                                        domProps: {
                                          value: faqlist.id,
                                          checked: Array.isArray(
                                            _vm.deleteItems
                                          )
                                            ? _vm._i(
                                                _vm.deleteItems,
                                                faqlist.id
                                              ) > -1
                                            : _vm.deleteItems
                                        },
                                        on: {
                                          change: function($event) {
                                            var $$a = _vm.deleteItems,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = faqlist.id,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  (_vm.deleteItems = $$a.concat(
                                                    [$$v]
                                                  ))
                                              } else {
                                                $$i > -1 &&
                                                  (_vm.deleteItems = $$a
                                                    .slice(0, $$i)
                                                    .concat($$a.slice($$i + 1)))
                                              }
                                            } else {
                                              _vm.deleteItems = $$c
                                            }
                                          }
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", [
                                        _vm._v(
                                          " " +
                                            _vm._s(i + _vm.formdata.data.from) +
                                            ". "
                                        )
                                      ])
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-link",
                                        attrs: {
                                          type: "button",
                                          name: "button"
                                        },
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteRow(faqlist.id)
                                          }
                                        }
                                      },
                                      [
                                        _c("i", {
                                          staticClass:
                                            "mdi mdi-delete text-danger"
                                        })
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    [
                                      _vm._v(_vm._s(faqlist.question) + " "),
                                      _c(
                                        "router-link",
                                        {
                                          attrs: {
                                            to: {
                                              name: "EditFaq",
                                              params: { Fid: faqlist.id }
                                            }
                                          }
                                        },
                                        [_vm._v("Edit ")]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("td", {
                                    staticStyle: { "white-space": "inherit" },
                                    domProps: {
                                      innerHTML: _vm._s(faqlist.answer)
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("td", [
                                    _c("label", { staticClass: "switch" }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: faqlist.is_visible,
                                            expression: "faqlist.is_visible"
                                          }
                                        ],
                                        attrs: { type: "checkbox" },
                                        domProps: {
                                          checked: Array.isArray(
                                            faqlist.is_visible
                                          )
                                            ? _vm._i(faqlist.is_visible, null) >
                                              -1
                                            : faqlist.is_visible
                                        },
                                        on: {
                                          change: [
                                            function($event) {
                                              var $$a = faqlist.is_visible,
                                                $$el = $event.target,
                                                $$c = $$el.checked
                                                  ? true
                                                  : false
                                              if (Array.isArray($$a)) {
                                                var $$v = null,
                                                  $$i = _vm._i($$a, $$v)
                                                if ($$el.checked) {
                                                  $$i < 0 &&
                                                    _vm.$set(
                                                      faqlist,
                                                      "is_visible",
                                                      $$a.concat([$$v])
                                                    )
                                                } else {
                                                  $$i > -1 &&
                                                    _vm.$set(
                                                      faqlist,
                                                      "is_visible",
                                                      $$a
                                                        .slice(0, $$i)
                                                        .concat(
                                                          $$a.slice($$i + 1)
                                                        )
                                                    )
                                                }
                                              } else {
                                                _vm.$set(
                                                  faqlist,
                                                  "is_visible",
                                                  $$c
                                                )
                                              }
                                            },
                                            function($event) {
                                              return _vm.updateStatus(i)
                                            }
                                          ]
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("span", {
                                        staticClass: "slider round"
                                      })
                                    ])
                                  ])
                                ])
                              }),
                              0
                            )
                          ]),
                          _vm._v(" "),
                          _c("pagination", {
                            attrs: { data: _vm.formdata.data },
                            on: { "pagination-change-page": _vm.listFaq }
                          })
                        ],
                        1
                      )
                    : _vm._e()
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/faqs/View.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/backend/faqs/View.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./View.vue?vue&type=template&id=06b0d9fe& */ "./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe&");
/* harmony import */ var _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./View.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/faqs/View.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/faqs/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=template&id=06b0d9fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/faqs/View.vue?vue&type=template&id=06b0d9fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_06b0d9fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);