(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[49],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    parameters: {
      type: Object
    }
  },
  data: function data() {
    return {
      faq: {},
      limit: this.parameters.limit,
      exclude: "",
      type: ""
    };
  },
  mounted: function mounted() {
    this.getFaq();
  },
  methods: {
    getFaq: function getFaq() {
      var _this = this;

      this.$emit("loader_token", true);
      var header = {};
      var data = {
        limit: this.limit
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_FRONT_FAQ"], data, header).then(function (res) {
        _this.faq = res.data.data;

        _this.$emit("loader_token", false);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.faq-group[data-v-263745f6] {\r\n  border-bottom: 1px solid #ccc;\r\n  padding: 15px 0;\n}\n.faq-group .faq-question[data-v-263745f6] {\r\n  display: flex;\r\n  margin-bottom: 10px;\r\n  font-weight: bold;\n}\n.faq-group .faq-question .box[data-v-263745f6] {\r\n  width: 40px;\r\n  height: 40px;\r\n  line-height: 40px;\r\n  text-align: center;\r\n  background: var(--primary);\r\n  color: var(--white);\r\n  margin-right: 15px;\n}\n.faq-group .faq-question > div[data-v-263745f6] {\r\n  flex-grow: 1;\n}\n.faq-group .faq-answer[data-v-263745f6],\r\n.faq-group .faq-answer p[data-v-263745f6] {\r\n  text-align: left !important;\n}\n.card-header[data-v-263745f6] {\r\n  background-color: var(--primary);\r\n  color: var(--white);\n}\n.card-header h5[data-v-263745f6] {\r\n  font-size: 18px;\r\n  cursor: pointer;\n}\n.card-header h5:not(.collapsed) [class^=\"icon-\"][data-v-263745f6] {\r\n  transition: all ease 300ms;\n}\n.card-header h5:not(.collapsed) [class^=\"icon-\"][data-v-263745f6] {\r\n  transform: rotateX(180deg);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "suncity_services home-section" }, [
    _c("div", { staticClass: "container py-5" }, [
      _c("h3", [_vm._v(_vm._s(_vm.parameters.heading))]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("p", { staticClass: "py-3 w-75 mx-auto" }, [
        _vm._v(
          "\n      " +
            _vm._s(_vm.parameters.subheading) +
            " " +
            _vm._s(_vm.parameters.type) +
            "\n    "
        )
      ]),
      _vm._v(" "),
      _vm.parameters.type == "grid"
        ? _c(
            "div",
            _vm._l(_vm.faq, function(q, i) {
              return _c("div", { key: i, staticClass: "faq-group" }, [
                _c("div", { staticClass: "faq-question" }, [
                  _c("span", { staticClass: "box" }, [_vm._v("Q.")]),
                  _vm._v(" "),
                  _c("div", [_vm._v(_vm._s(q.question))])
                ]),
                _vm._v(" "),
                _c("div", {
                  staticClass: "faq-answer",
                  domProps: { innerHTML: _vm._s(q.answer) }
                })
              ])
            }),
            0
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.parameters.type == "slider"
        ? _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c(
                "div",
                { staticClass: "accordion", attrs: { id: "faqExample" } },
                _vm._l(_vm.faq, function(q, i) {
                  return _c("div", { key: i, staticClass: "card" }, [
                    _c(
                      "div",
                      {
                        staticClass: "card-header p-2",
                        attrs: { id: "headingOne" }
                      },
                      [
                        _c(
                          "h5",
                          {
                            staticClass: "mb-0 collapsed",
                            attrs: {
                              "data-toggle": "collapse",
                              "data-target": "#panel" + i
                            }
                          },
                          [
                            _c("i", {
                              staticClass: "icon-angle-down float-right"
                            }),
                            _vm._v(" "),
                            _c("span", [_vm._v("Q:")]),
                            _vm._v(
                              " " + _vm._s(q.question) + "\n              "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "collapse",
                        attrs: {
                          id: "panel" + i,
                          "aria-labelledby": "headingOne",
                          "data-parent": "#faqExample"
                        }
                      },
                      [
                        _c("div", {
                          staticClass: "card-body",
                          domProps: { innerHTML: _vm._s(q.answer) }
                        })
                      ]
                    )
                  ])
                }),
                0
              )
            ])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "circle_dot" }, [
      _c("span"),
      _vm._v(" "),
      _c("span"),
      _vm._v(" "),
      _c("span")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeFaq.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeFaq.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomeFaq.vue?vue&type=template&id=263745f6&scoped=true& */ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true&");
/* harmony import */ var _HomeFaq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomeFaq.vue?vue&type=script&lang=js& */ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& */ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _HomeFaq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "263745f6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/homeComponent/HomeFaq.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeFaq.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=style&index=0&id=263745f6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_style_index_0_id_263745f6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeFaq.vue?vue&type=template&id=263745f6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeFaq.vue?vue&type=template&id=263745f6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeFaq_vue_vue_type_template_id_263745f6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);