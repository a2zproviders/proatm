(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Layout.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-tinymce-editor */ "./node_modules/vue-tinymce-editor/src/index.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // const baseDir = "/proAtm/" //development

var baseDir = "/"; //production



vue__WEBPACK_IMPORTED_MODULE_0___default.a.component("pagination", __webpack_require__(/*! laravel-vue-pagination */ "./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js"));
vue__WEBPACK_IMPORTED_MODULE_0___default.a.component("tinymce", vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_2__["default"]);
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    basicDetails: Object
  },
  data: function data() {
    return {
      sidebar: "AdminSidebar",
      user: JSON.parse(localStorage.getItem("user_info")),
      token: localStorage.getItem("token"),
      baseDir: baseDir
    };
  },
  mounted: function mounted() {
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("li.nav-item").click(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("li.nav-item").children("a").attr("aria-expanded", false);
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("li.nav-item").closest("div.collapse").removeClass("show");
    });
    jquery__WEBPACK_IMPORTED_MODULE_1___default()(".navbar-toggler").click(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("body").toggleClass("sidebar-icon-only");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#sidebar").toggleClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#overlay").toggleClass("overlay");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).children("span").toggleClass("mdi-menu mdi-close");
    });
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("li.nav-item").hover(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).toggleClass("hover-open");
    });
  },
  methods: {
    logout: function logout() {
      var _this = this;

      if (confirm("Are you sure want to logout?")) {
        localStorage.removeItem("token");
        localStorage.removeItem("user_info");
        var header = {
          Authorization: "Bearer " + this.token
        };
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_LOGOUT"], header).then(function (response) {
          _this.$router.push({
            name: "adminLogin"
          });
        })["catch"](function (error) {});
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/vendors/mdi/css/materialdesignicons.min.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/vendors/mdi/css/materialdesignicons.min.css"), "");
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/vendors/css/vendor.bundle.base.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/vendors/css/vendor.bundle.base.css"), "");
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/css/style.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/css/style.css"), "");

// module
exports.push([module.i, "\n/* The switch - the box around the slider */\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\n}\r\n\r\n/* Hide default HTML checkbox */\n.switch input {\r\n  opacity: 0;\r\n  width: 0;\r\n  height: 0;\n}\r\n\r\n/* The slider */\n.slider {\r\n  position: absolute;\r\n  cursor: pointer;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: #ccc;\r\n  transition: 0.4s;\n}\n.slider:before {\r\n  position: absolute;\r\n  content: \"\";\r\n  height: 26px;\r\n  width: 26px;\r\n  left: 4px;\r\n  bottom: 4px;\r\n  background-color: white;\r\n  transition: 0.4s;\n}\ninput:checked + .slider {\r\n  background-color: #2196f3;\n}\ninput:focus + .slider {\r\n  box-shadow: 0 0 1px #2196f3;\n}\ninput:checked + .slider:before {\r\n  transform: translateX(26px);\n}\r\n\r\n/* Rounded sliders */\n.slider.round {\r\n  border-radius: 34px;\n}\n.slider.round:before {\r\n  border-radius: 50%;\n}\n.sidebar .nav .nav-item.active {\r\n  background: #f2edf3 !important;\n}\nul.nav::-webkit-scrollbar {\r\n  display: none !important;\n}\r\n/* width */\n::-webkit-scrollbar {\r\n  width: 7px;\r\n  display: none;\r\n  color: #3498db;\n}\r\n\r\n/* Track */\n::-webkit-scrollbar-track {\r\n  background: #f1f1f1;\n}\r\n\r\n/* Handle */\n::-webkit-scrollbar-thumb {\r\n  background: #3498db;\n}\r\n\r\n/* Handle on hover */\n::-webkit-scrollbar-thumb:hover {\r\n  background: #555;\n}\n.breadcrumb {\r\n  padding: 0.56rem 0;\n}\n.page-header .page-title {\r\n  display: none;\n}\n@media (max-width: 768px) {\n.sidebar .nav::-webkit-scrollbar {\r\n    display: none !important;\n}\n.sidebar-offcanvas {\r\n    z-index: 9999;\n}\n.overlay {\r\n    background-color: #000;\r\n    opacity: 0.5;\r\n    z-index: 999;\r\n    position: fixed;\r\n    top: 0;\r\n    left: 0;\r\n    right: 0;\r\n    bottom: 0;\n}\n}\n@media all and (min-width: 768px) {\nbody.sidebar-icon-only li.nav-item.heading {\r\n    display: none;\n}\nbody.sidebar-icon-only li.nav-item.button {\r\n    display: none;\n}\nul.nav {\r\n    position: fixed;\r\n    top: 70px;\r\n    left: 0;\r\n    width: 260px;\r\n    overflow-y: scroll !important;\r\n    height: 100vh;\r\n    padding-bottom: 100px;\r\n    -webkit-transition: width 0.25s ease, background 0.25s ease;\n}\nbody.sidebar-icon-only ul.nav {\r\n    position: fixed;\r\n    top: 70px;\r\n    left: 0;\r\n    width: 70px;\r\n    overflow-y: scroll !important;\r\n    height: 100vh;\r\n    padding-bottom: 100px;\r\n    -webkit-transition: width 0.25s ease, background 0.25s ease;\n}\n}\n@media all and (min-width: 992px) {\n.sidebar-icon-only .nav-item.hover-open .nav-link .menu-title {\r\n    display: block !important;\r\n    align-items: center;\r\n    background: #fcfcfc;\r\n    padding: 01.5rem 1.4rem;\r\n    left: 70px;\r\n    position: absolute;\r\n    text-align: center;\r\n    top: 0;\r\n    bottom: 0;\r\n    width: 190px;\r\n    line-height: 1.8;\r\n    z-index: 1;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-scroller" }, [
    _c(
      "nav",
      {
        staticClass:
          "\n      navbar\n      default-layout-navbar\n      col-lg-12 col-12\n      p-0\n      fixed-top\n      d-flex\n      flex-row\n    "
      },
      [
        _c(
          "div",
          {
            staticClass:
              "\n        text-center\n        navbar-brand-wrapper\n        d-flex\n        align-items-center\n        justify-content-center\n      "
          },
          [
            _c(
              "router-link",
              {
                staticClass: "navbar-brand brand-logo font-weight-bold",
                attrs: { to: { name: "adminDashboard" } }
              },
              [
                _vm._v(
                  "\n        " +
                    _vm._s(_vm.basicDetails.store_name) +
                    "\n      "
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "navbar-brand brand-logo-mini",
                attrs: { to: { name: "adminDashboard" } }
              },
              [
                _c("img", {
                  attrs: {
                    src: _vm.baseDir + "assets/images/logo-mini.svg",
                    alt: "logo"
                  }
                })
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "navbar-menu-wrapper d-flex align-items-stretch",
            staticStyle: { background: "#fff" }
          },
          [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("ul", { staticClass: "navbar-nav navbar-nav-right" }, [
              _vm._m(2),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "nav-item d-lg-block full-screen-link" },
                [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link",
                      attrs: { title: "Logout" },
                      on: {
                        click: function($event) {
                          $event.preventDefault()
                          return _vm.logout($event)
                        }
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "mdi mdi-logout",
                        attrs: { id: "fullscreen-button" }
                      })
                    ]
                  )
                ]
              ),
              _vm._v(" "),
              _c("li", { staticClass: "nav-item nav-profile dropdown" }, [
                _c(
                  "a",
                  {
                    staticClass: "nav-link dropdown-toggle",
                    attrs: {
                      id: "profileDropdown",
                      href: "#",
                      "data-toggle": "dropdown",
                      "aria-expanded": "false"
                    }
                  },
                  [
                    _c("div", { staticClass: "nav-profile-img" }, [
                      _c("img", {
                        attrs: {
                          src: _vm.baseDir + "assets/images/faces/face1.jpg",
                          alt: "image"
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "availability-status online" })
                    ]),
                    _vm._v(" "),
                    _vm._m(3)
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "dropdown-menu navbar-dropdown",
                    attrs: { "aria-labelledby": "profileDropdown" }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "dropdown-item",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.logout($event)
                          }
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "mdi mdi-logout mr-2 text-primary"
                        }),
                        _vm._v(" Sign Out\n            ")
                      ]
                    )
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass:
                  "\n          navbar-toggler navbar-toggler-right\n          d-lg-none\n          align-self-center\n        ",
                attrs: { type: "button", "data-toggle": "offcanvas" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.sidebar($event)
                  }
                }
              },
              [_c("span", { staticClass: "mdi mdi-menu" })]
            )
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "container-fluid page-body-wrapper" }, [
      _c(
        "nav",
        { staticClass: "sidebar sidebar-offcanvas", attrs: { id: "sidebar" } },
        [
          _c("ul", { staticClass: "nav" }, [
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "adminDashboard" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Dashboard")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-home menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm._m(4),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "SliderMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Slider")
                    ]),
                    _vm._v(" "),
                    _c("i", {
                      staticClass: "mdi mdi-folder-multiple-image menu-icon"
                    })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "PageMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Pages")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-library-books menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "ServiceMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Services")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-settings-box menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "TestimonialMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Testimonial")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-message-text menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "FaqMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [_vm._v("Faqs")]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-help-circle menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "BlogMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [_vm._v("Blog")]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-tooltip-edit menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "PortfolioMaster" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Portfolio")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-briefcase menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  { staticClass: "nav-link", attrs: { to: { name: "Menu" } } },
                  [
                    _c("span", { staticClass: "menu-title" }, [_vm._v("Menu")]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-menu menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "ViewInquery" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Contact Inquery")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-view-list menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "Settings" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Setting")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-settings menu-icon" })
                  ]
                )
              ],
              1
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "main-panel" },
        [_c("router-view"), _vm._v(" "), _vm._m(5)],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { attrs: { id: "overlay" } })
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler navbar-toggler align-self-center",
        attrs: { type: "button", "data-toggle": "minimize" }
      },
      [_c("span", { staticClass: "mdi mdi-menu" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-field d-none d-md-block" }, [
      _c(
        "form",
        {
          staticClass: "d-flex align-items-center h-100",
          attrs: { action: "#" }
        },
        [
          _c("div", { staticClass: "input-group" }, [
            _c("div", { staticClass: "input-group-prepend bg-transparent" }, [
              _c("i", {
                staticClass: "input-group-text border-0 mdi mdi-magnify"
              })
            ]),
            _vm._v(" "),
            _c("input", {
              staticClass: "form-control bg-transparent border-0",
              attrs: { type: "text", placeholder: "Search projects" }
            })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item d-lg-block full-screen-link" }, [
      _c(
        "a",
        {
          staticClass: "nav-link",
          attrs: {
            href: "/plateform-xpanel/preview",
            target: "_blank",
            title: "Preview"
          }
        },
        [
          _c("i", {
            staticClass: "mdi mdi-eye",
            attrs: { id: "fullscreen-button" }
          })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "nav-profile-text" }, [
      _c("p", { staticClass: "mb-1 text-black" }, [_vm._v("Admin")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item heading" }, [
      _c("div", { staticStyle: { "font-weight": "bold" } }, [
        _vm._v("Website")
      ]),
      _vm._v(" "),
      _c("hr", { staticClass: "m-0 p-0" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "footer" }, [
      _c(
        "div",
        {
          staticClass:
            "d-sm-flex justify-content-center justify-content-sm-between"
        },
        [
          _c(
            "span",
            {
              staticClass:
                "\n              text-muted text-center text-sm-left\n              d-block d-sm-inline-block\n            "
            },
            [
              _vm._v("Copyright © 2017 "),
              _c("a", { attrs: { href: "#", target: "_blank" } }, [
                _vm._v("ProATM")
              ]),
              _vm._v(". All\n            rights reserved.")
            ]
          ),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass:
                "float-none float-sm-right d-block mt-1 mt-sm-0 text-center"
            },
            [_vm._v("Developed by A2Z Providers")]
          )
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/Layout.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/backend/Layout.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=62e2f0a0& */ "./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Layout.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/Layout.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/backend/Layout.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=62e2f0a0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Layout.vue?vue&type=template&id=62e2f0a0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_62e2f0a0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);