(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[71],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      data: {},
      home_settings: [],
      token: localStorage.getItem("token")
    };
  },
  mounted: function mounted() {
    this.getInfo();
  },
  methods: {
    getInfo: function getInfo() {
      var _this = this;

      // console.log(id)
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_HOMEAPPEARANCE"], header).then(function (res) {
        _this.data = res.data;
        _this.home_settings = _this.data;
      });
    },
    updatedata: function updatedata(setting_id, arrayIndex) {
      var _this2 = this;

      var data = this.home_settings[arrayIndex];
      delete data.templates; // delete data.id

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["PUT"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_HOMEAPPEARANCE"] + "/" + setting_id, {
        record: data
      }, header).then(function (res) {
        _this2.$swal("Sucecess", "Record updated successfully", "Ok").then(function (result) {
          if (result.value) {// this.$router.push({name: 'ViewPage'})
          }
        });
      });
    },
    storePage: function storePage() {
      var _this3 = this;

      var id = this.$route.params.Pid;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["PUT"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_TESTIMONIAL"] + "/" + id, {
        record: this.formdata
      }, header).then(function (res) {
        _this3.$swal("Sucecess", "Record updated successfully", "Ok").then(function (result) {
          if (result.value) {
            _this3.$router.push({
              name: "ViewPage"
            });
          }
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [
        _vm._v("Home Appearance Setting")
      ]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "adminDashboard" } } }, [
                _vm._v("\n            Dasbhoard\n          ")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "Settings" } } }, [
                _vm._v(" Setting ")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("\n          Home Appearance Settings\n        ")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c(
      "form",
      {
        staticClass: "forms-sample row",
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.storePage($event)
          }
        }
      },
      _vm._l(_vm.home_settings, function(h_app, k) {
        return _c("div", { key: k, staticClass: "col-sm-4" }, [
          _c("div", { staticClass: "card mb-3" }, [
            _c("div", { staticClass: "card-header" }, [
              _c(
                "label",
                {
                  staticClass: "float-right",
                  staticStyle: { color: "#3498db", cursor: "pointer" },
                  attrs: { title: "Update" },
                  on: {
                    click: function($event) {
                      return _vm.updatedata(h_app.id, k)
                    }
                  }
                },
                [_c("i", { staticClass: "mdi mdi-content-save" })]
              ),
              _vm._v(" "),
              _c("label", { staticClass: "float-right mr-2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: h_app.status,
                      expression: "h_app.status"
                    }
                  ],
                  attrs: {
                    type: "radio",
                    name: "status_" + h_app.id,
                    value: "no"
                  },
                  domProps: { checked: _vm._q(h_app.status, "no") },
                  on: {
                    change: function($event) {
                      return _vm.$set(h_app, "status", "no")
                    }
                  }
                }),
                _vm._v("\n            No\n          ")
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "float-right mr-2" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: h_app.status,
                      expression: "h_app.status"
                    }
                  ],
                  attrs: {
                    type: "radio",
                    name: "status_" + h_app.id,
                    value: "yes"
                  },
                  domProps: { checked: _vm._q(h_app.status, "yes") },
                  on: {
                    change: function($event) {
                      return _vm.$set(h_app, "status", "yes")
                    }
                  }
                }),
                _vm._v("\n            Yes\n          ")
              ]),
              _vm._v(" "),
              _c("div", [
                _vm._v("\n            Do you want to show\n            "),
                _c("strong", [_vm._v(_vm._s(h_app.heading.toLowerCase()))]),
                _vm._v(" on home page?\n          ")
              ])
            ]),
            _vm._v(" "),
            h_app.status == "yes"
              ? _c("div", { staticClass: "card-body" }, [
                  h_app.templates.component !== "Slider"
                    ? _c("div", [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col-sm-12 form-group" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: h_app.heading,
                                  expression: "h_app.heading"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                id: "heading",
                                placeholder: "Heading"
                              },
                              domProps: { value: h_app.heading },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    h_app,
                                    "heading",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col-sm-12 form-group" }, [
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model.trim",
                                  value: h_app.subheading,
                                  expression: "h_app.subheading",
                                  modifiers: { trim: true }
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                name: "name",
                                rows: "5",
                                placeholder:
                                  "Enter sub-heading or summary about heading."
                              },
                              domProps: { value: h_app.subheading },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    h_app,
                                    "subheading",
                                    $event.target.value.trim()
                                  )
                                },
                                blur: function($event) {
                                  return _vm.$forceUpdate()
                                }
                              }
                            })
                          ])
                        ]),
                        _vm._v(" "),
                        h_app.templates.type == "list"
                          ? _c("div", { staticClass: "row" }, [
                              _c(
                                "div",
                                { staticClass: "col-sm-4 form-group" },
                                [
                                  _c("label", [_vm._v("No. of item?")]),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: h_app.limit,
                                          expression: "h_app.limit"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            h_app,
                                            "limit",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("option", { attrs: { value: "4" } }, [
                                        _vm._v("4")
                                      ]),
                                      _vm._v(" "),
                                      _c("option", { attrs: { value: "6" } }, [
                                        _vm._v("6")
                                      ]),
                                      _vm._v(" "),
                                      _c("option", { attrs: { value: "9" } }, [
                                        _vm._v("9")
                                      ])
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-4 form-group" },
                                [
                                  _c("label", [_vm._v("Slider / Grid")]),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: h_app.type,
                                          expression: "h_app.type"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            h_app,
                                            "type",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        { attrs: { value: "slider" } },
                                        [_vm._v("Slider")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "option",
                                        { attrs: { value: "grid" } },
                                        [_vm._v("Grid")]
                                      )
                                    ]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-4 form-group" },
                                [
                                  _c("label", [_vm._v("Sort By")]),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: h_app.order_by,
                                          expression: "h_app.order_by"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            h_app,
                                            "order_by",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        { attrs: { value: "a-z" } },
                                        [_vm._v("A - Z")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "option",
                                        { attrs: { value: "z-a" } },
                                        [_vm._v("Z - A")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "option",
                                        { attrs: { value: "latest" } },
                                        [_vm._v("Newest")]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "option",
                                        { attrs: { value: "oldest" } },
                                        [_vm._v("Oldest")]
                                      )
                                    ]
                                  )
                                ]
                              )
                            ])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  h_app.templates.component === "Slider"
                    ? _c(
                        "div",
                        [
                          _c("b-form-group", [
                            _c("label", { staticClass: "w-100" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: h_app.show_text,
                                    expression: "h_app.show_text"
                                  }
                                ],
                                attrs: { type: "checkbox" },
                                domProps: {
                                  value: 1,
                                  checked: Array.isArray(h_app.show_text)
                                    ? _vm._i(h_app.show_text, 1) > -1
                                    : h_app.show_text
                                },
                                on: {
                                  change: function($event) {
                                    var $$a = h_app.show_text,
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = 1,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          _vm.$set(
                                            h_app,
                                            "show_text",
                                            $$a.concat([$$v])
                                          )
                                      } else {
                                        $$i > -1 &&
                                          _vm.$set(
                                            h_app,
                                            "show_text",
                                            $$a
                                              .slice(0, $$i)
                                              .concat($$a.slice($$i + 1))
                                          )
                                      }
                                    } else {
                                      _vm.$set(h_app, "show_text", $$c)
                                    }
                                  }
                                }
                              }),
                              _vm._v(
                                "\n                Do you want to show overlay text from slider?\n              "
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("b-form-group", [
                            _c("label", { staticClass: "w-100" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: h_app.show_thumb,
                                    expression: "h_app.show_thumb"
                                  }
                                ],
                                attrs: { type: "checkbox" },
                                domProps: {
                                  value: 1,
                                  checked: Array.isArray(h_app.show_thumb)
                                    ? _vm._i(h_app.show_thumb, 1) > -1
                                    : h_app.show_thumb
                                },
                                on: {
                                  change: function($event) {
                                    var $$a = h_app.show_thumb,
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = 1,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          _vm.$set(
                                            h_app,
                                            "show_thumb",
                                            $$a.concat([$$v])
                                          )
                                      } else {
                                        $$i > -1 &&
                                          _vm.$set(
                                            h_app,
                                            "show_thumb",
                                            $$a
                                              .slice(0, $$i)
                                              .concat($$a.slice($$i + 1))
                                          )
                                      }
                                    } else {
                                      _vm.$set(h_app, "show_thumb", $$c)
                                    }
                                  }
                                }
                              }),
                              _vm._v(
                                "\n                Do you want to show thumbnail slider?\n              "
                              )
                            ])
                          ])
                        ],
                        1
                      )
                    : _vm._e()
                ])
              : _vm._e()
          ])
        ])
      }),
      0
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/appearance/HomePageAppearance.vue":
/*!***************************************************************************!*\
  !*** ./resources/js/components/backend/appearance/HomePageAppearance.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomePageAppearance.vue?vue&type=template&id=51e2f447& */ "./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447&");
/* harmony import */ var _HomePageAppearance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomePageAppearance.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HomePageAppearance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/appearance/HomePageAppearance.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageAppearance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomePageAppearance.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageAppearance_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomePageAppearance.vue?vue&type=template&id=51e2f447& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/appearance/HomePageAppearance.vue?vue&type=template&id=51e2f447&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageAppearance_vue_vue_type_template_id_51e2f447___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);