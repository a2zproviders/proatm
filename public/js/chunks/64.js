(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[64],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/api.js */ "./resources/js/services/api.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "StoreProfile",
  data: function data() {
    return {
      store: {
        store_name: "",
        site_tagline: "",
        store_url: "",
        category_id: "",
        logo: "",
        favicon: "",
        aadhar_file: "",
        pancard_file: "",
        aadhar_number: "",
        pan_number: "",
        gstin: "",
        phone: "",
        address: "",
        footer_script: "",
        google_map: "",
        social_links: [],
        show_title: "",
        primary_color: "",
        secondary_color: ""
      },
      logo: null,
      favicon: null,
      logo_src: null,
      favicon_src: null,
      socials: ["Facebook", "Instagram", "Linkedin", "YouTube", "Twitter", "Pinterest", "RSS"],
      loading: false,
      token: localStorage.getItem("token")
    };
  },
  mounted: function mounted() {
    this.getInfo();
    this.$emit("changeTab", this.$route.name);
  },
  methods: {
    dataURLToBlob: function dataURLToBlob(dataURL) {
      var BASE64_MARKER = ";base64,";

      if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(",");
        var contentType = parts[0].split(":")[1];
        var raw = parts[1];
        return new Blob([raw], {
          type: contentType
        });
      }

      var parts = dataURL.split(BASE64_MARKER);
      var contentType = parts[0].split(":")[1];
      var raw = window.atob(parts[1]);
      var rawLength = raw.length;
      var uInt8Array = new Uint8Array(rawLength);

      for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
      }

      return new Blob([uInt8Array], {
        type: contentType
      });
    },
    resizeImage: function resizeImage(file, MAX_WIDTH, MAX_HEIGHT, next) {
      var _this = this;

      if (file.type.match("image/*")) {
        var reader = new FileReader();

        reader.onload = function (readerEvent) {
          var image = new Image();

          image.onload = function (imageEvent) {
            // Resize the image
            var canvas = document.createElement("canvas"),
                width = image.width,
                height = image.height;

            if (width > MAX_WIDTH) {
              height *= MAX_WIDTH / width;
              width = MAX_WIDTH;
            }

            if (height > MAX_HEIGHT) {
              width *= MAX_HEIGHT / height;
              height = MAX_HEIGHT;
            }

            canvas.width = width;
            canvas.height = height;
            canvas.getContext("2d").drawImage(image, 0, 0, width, height);
            var dataUrl = canvas.toDataURL("image/png");

            var resizedImage = _this.dataURLToBlob(dataUrl);

            next(null, {
              type: "imageResized",
              blob: resizedImage,
              url: dataUrl
            });
          };

          image.src = readerEvent.target.result;
        };

        reader.readAsDataURL(file);
      }
    },
    handleLogo: function handleLogo(e) {
      var _this2 = this;

      var file = e.target.files[0];
      this.resizeImage(file, 250, 50, function (err, image) {
        if (image) {
          _this2.logo = image.blob;
          _this2.logo_src = image.url;
        }
      });
    },
    handleFavicon: function handleFavicon(e) {
      var _this3 = this;

      var file = e.target.files[0];
      this.resizeImage(file, 128, 128, function (err, image) {
        if (image) {
          _this3.favicon = image.blob;
          _this3.favicon_src = image.url;
        }
      });
    },
    addSocials: function addSocials() {
      var count = this.store.social_links.length;

      if (count < this.socials.length) {
        this.store.social_links.push({
          name: this.socials[count],
          url: "https://"
        });
      }
    },
    removeSocials: function removeSocials(index) {
      this.store.social_links.splice(index, 1);
    },
    getInfo: function getInfo() {
      var _this4 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_USER_DETAILS"], header).then(function (res) {
        var _res$data$social_link;

        _this4.logo_src = res.data.logo;
        _this4.favicon_src = res.data.favicon;
        delete res.data.user;
        delete res.data.category;
        delete res.data.category_name;
        delete res.data.logo;
        delete res.data.favicon;
        res.data.social_links = (_res$data$social_link = res.data.social_links) !== null && _res$data$social_link !== void 0 ? _res$data$social_link : [];
        _this4.store = res.data;
      });
    },
    submitForm: function submitForm() {
      var _this5 = this;

      var data = {
        store: this.store
      };
      var form_data = new FormData();

      for (var key in this.store) {
        if (key !== "social_links") form_data.append("store[".concat(key, "]"), this.store[key]);
      }

      if (this.store.social_links) {
        for (var _key in this.store.social_links) {
          for (var key2 in this.store.social_links[_key]) {
            form_data.append("store[social_links][".concat(_key, "][").concat(key2, "]"), this.store.social_links[_key][key2]);
          }
        }
      }

      if (this.logo) {
        form_data.append("logo", this.logo);
      }

      if (this.favicon) {
        form_data.append("favicon", this.favicon);
      }

      this.loading = true;
      Object(_services_api_js__WEBPACK_IMPORTED_MODULE_0__["update_store_profile"])(form_data).then(function (res) {
        _this5.loading = false;

        _this5.$swal("Done!", res.data.message, "success");
      })["catch"](function () {
        _this5.loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("Site Settings")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "adminDashboard" } } }, [
                _vm._v("\n            Dashboard\n          ")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "Settings" } } }, [
                _vm._v("Settings")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("\n          Site Settings\n        ")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            !_vm.store.id
              ? _c(
                  "div",
                  { staticClass: "text-center" },
                  [
                    _c("b-spinner", {
                      staticStyle: { width: "100px", height: "100px" }
                    })
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.store.id
              ? _c(
                  "form",
                  {
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitForm($event)
                      }
                    }
                  },
                  [
                    _c(
                      "b-row",
                      [
                        _c("b-col", { attrs: { lg: "9", sm: "8" } }, [
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("label", { attrs: { for: "store_name" } }, [
                                  _vm._v("Site Title")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.store_name,
                                      expression: "store.store_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "store_name",
                                    placeholder: "Enter Site Title"
                                  },
                                  domProps: { value: _vm.store.store_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "store_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "label",
                                  { attrs: { for: "site_tagline" } },
                                  [_vm._v("Site tagline")]
                                ),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.site_tagline,
                                      expression: "store.site_tagline"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "site_tagline",
                                    placeholder: "Enter site tagline"
                                  },
                                  domProps: { value: _vm.store.site_tagline },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "site_tagline",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c(
                                  "label",
                                  { attrs: { for: "aadhar_number" } },
                                  [_vm._v("Aadhaar No.")]
                                ),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.aadhar_number,
                                      expression: "store.aadhar_number"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", id: "aadhar_number" },
                                  domProps: { value: _vm.store.aadhar_number },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "aadhar_number",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("label", { attrs: { for: "pan_number" } }, [
                                  _vm._v("Pan No.")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.pan_number,
                                      expression: "store.pan_number"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", id: "pan_number" },
                                  domProps: { value: _vm.store.pan_number },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "pan_number",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "row" }, [
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("label", { attrs: { for: "gstin" } }, [
                                  _vm._v("GSTIN")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.gstin,
                                      expression: "store.gstin"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "gstin",
                                    placeholder: "Enter GSTIN"
                                  },
                                  domProps: { value: _vm.store.gstin },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "gstin",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "col-md-6" }, [
                              _c("div", { staticClass: "form-group" }, [
                                _c("label", { attrs: { for: "phone" } }, [
                                  _vm._v("Office Contact Number")
                                ]),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.store.phone,
                                      expression: "store.phone"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    id: "phone",
                                    placeholder: "Enter office contact number"
                                  },
                                  domProps: { value: _vm.store.phone },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.store,
                                        "phone",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "footer_script" } }, [
                              _vm._v("Address")
                            ]),
                            _vm._v(" "),
                            _c("textarea", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.store.address,
                                  expression: "store.address"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                id: "footer_script",
                                rows: "4",
                                placeholder: "Enter address"
                              },
                              domProps: { value: _vm.store.address },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.store,
                                    "address",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ]),
                          _vm._v(" "),
                          _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "footer_script" } }, [
                                _vm._v("Footer script")
                              ]),
                              _vm._v(" "),
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.store.footer_script,
                                    expression: "store.footer_script"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  id: "footer_script",
                                  rows: "4",
                                  placeholder: "Enter footer script"
                                },
                                domProps: { value: _vm.store.footer_script },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.store,
                                      "footer_script",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "google_map" } }, [
                                _vm._v("Google map script")
                              ]),
                              _vm._v(" "),
                              _c("textarea", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.store.google_map,
                                    expression: "store.google_map"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  id: "google_map",
                                  rows: "4",
                                  placeholder: "Enter google map script"
                                },
                                domProps: { value: _vm.store.google_map },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.store,
                                      "google_map",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "b-col",
                          { attrs: { sm: "4", lg: "3" } },
                          [
                            _c(
                              "b-form-group",
                              { attrs: { label: "Upload Logo" } },
                              [
                                _vm.logo_src
                                  ? _c("img", {
                                      staticClass: "mb-1",
                                      attrs: { src: _vm.logo_src }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("label", { staticClass: "w-100" }, [
                                  _c("input", {
                                    attrs: { type: "file", accept: "image/*" },
                                    on: { change: _vm.handleLogo }
                                  })
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-form-group",
                              { attrs: { label: "Upload Favicon" } },
                              [
                                _vm.favicon_src
                                  ? _c("img", {
                                      staticClass: "mb-1",
                                      attrs: { src: _vm.favicon_src }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("label", { staticClass: "w-100" }, [
                                  _c("input", {
                                    attrs: { type: "file", accept: "image/*" },
                                    on: { change: _vm.handleFavicon }
                                  })
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c("label", { staticClass: "w-100" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.store.show_title,
                                    expression: "store.show_title"
                                  }
                                ],
                                attrs: { type: "checkbox" },
                                domProps: {
                                  value: 1,
                                  checked: Array.isArray(_vm.store.show_title)
                                    ? _vm._i(_vm.store.show_title, 1) > -1
                                    : _vm.store.show_title
                                },
                                on: {
                                  change: function($event) {
                                    var $$a = _vm.store.show_title,
                                      $$el = $event.target,
                                      $$c = $$el.checked ? true : false
                                    if (Array.isArray($$a)) {
                                      var $$v = 1,
                                        $$i = _vm._i($$a, $$v)
                                      if ($$el.checked) {
                                        $$i < 0 &&
                                          _vm.$set(
                                            _vm.store,
                                            "show_title",
                                            $$a.concat([$$v])
                                          )
                                      } else {
                                        $$i > -1 &&
                                          _vm.$set(
                                            _vm.store,
                                            "show_title",
                                            $$a
                                              .slice(0, $$i)
                                              .concat($$a.slice($$i + 1))
                                          )
                                      }
                                    } else {
                                      _vm.$set(_vm.store, "show_title", $$c)
                                    }
                                  }
                                }
                              }),
                              _vm._v(
                                "\n                  Show Site Title in Header\n                "
                              )
                            ]),
                            _vm._v(" "),
                            _c("hr"),
                            _vm._v(" "),
                            _c("b-form-group", { attrs: { label: "Color" } }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.store.primary_color,
                                    expression: "store.primary_color"
                                  }
                                ],
                                attrs: { type: "color" },
                                domProps: { value: _vm.store.primary_color },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.store,
                                      "primary_color",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.store.secondary_color,
                                    expression: "store.secondary_color"
                                  }
                                ],
                                attrs: { type: "color" },
                                domProps: { value: _vm.store.secondary_color },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.store,
                                      "secondary_color",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", [
                      _c(
                        "div",
                        { staticClass: "form-group" },
                        [
                          _c("label", { attrs: { for: "social_links" } }, [
                            _vm._v("Social links")
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "mb-3" }, [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-sm btn-dark",
                                attrs: { type: "button" },
                                on: { click: _vm.addSocials }
                              },
                              [
                                _vm._v(
                                  "\n                    Add\n                  "
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.store.social_links, function(s, i) {
                            return _c(
                              "div",
                              { key: i, staticClass: "row mb-3" },
                              [
                                _c("div", { staticClass: "col-4" }, [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.store.social_links[i].name,
                                          expression:
                                            "store.social_links[i].name"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            _vm.store.social_links[i],
                                            "name",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    _vm._l(_vm.socials, function(sc, j) {
                                      return _c(
                                        "option",
                                        { key: j, domProps: { value: sc } },
                                        [
                                          _vm._v(
                                            "\n                        " +
                                              _vm._s(sc) +
                                              "\n                      "
                                          )
                                        ]
                                      )
                                    }),
                                    0
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-6" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.store.social_links[i].url,
                                        expression: "store.social_links[i].url"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { type: "text" },
                                    domProps: {
                                      value: _vm.store.social_links[i].url
                                    },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.store.social_links[i],
                                          "url",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "col-2" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-link text-dark",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.removeSocials(i)
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass: "mdi mdi-delete"
                                      }),
                                      _vm._v(" Remove\n                    ")
                                    ]
                                  )
                                ])
                              ]
                            )
                          })
                        ],
                        2
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success float-right ml-3",
                          attrs: { type: "submit", disabled: _vm.loading }
                        },
                        [_c("span", [_vm._v("Update")])]
                      ),
                      _vm._v(" "),
                      _vm.loading
                        ? _c(
                            "div",
                            [
                              _vm.loading ? _c("b-spinner") : _vm._e(),
                              _vm._v(
                                " In Progress, please\n                wait…\n              "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ])
                  ],
                  1
                )
              : _vm._e()
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/setting/StoreProfile.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/backend/setting/StoreProfile.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StoreProfile.vue?vue&type=template&id=efbb6726& */ "./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726&");
/* harmony import */ var _StoreProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StoreProfile.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _StoreProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/setting/StoreProfile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StoreProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./StoreProfile.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StoreProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./StoreProfile.vue?vue&type=template&id=efbb6726& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/StoreProfile.vue?vue&type=template&id=efbb6726&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StoreProfile_vue_vue_type_template_id_efbb6726___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: update_meta, baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, update_menu_parent, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, view_posts, post_sidebar, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_meta", function() { return update_meta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_parent", function() { return update_menu_parent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_posts", function() { return view_posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "post_sidebar", function() { return post_sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var update_meta = function update_meta(metaJSON) {
  if (metaJSON.title) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('title').text(metaJSON.title);
  }

  if (metaJSON.keywords) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=keywords]').text(metaJSON.keywords);
  }

  if (metaJSON.description) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=description]').text(metaJSON.description);
  }
};
var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  // prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var update_menu_parent = function update_menu_parent(data) {
  var params = {
    method: 'POST',
    url: "menu-parent",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data,
    files: true
  };
  return execute(params);
};
/**
 * Blogs
 */

var view_posts = function view_posts(query) {
  var params = {
    url: "web/post/?".concat(query),
    no_auth: true
  };
  return execute(params);
};
var post_sidebar = function post_sidebar() {
  var params = {
    url: "web/post-sidebar",
    no_auth: true
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);