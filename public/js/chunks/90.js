(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[90],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var hooper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hooper */ "./node_modules/hooper/dist/hooper.esm.js");
/* harmony import */ var hooper_dist_hooper_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! hooper/dist/hooper.css */ "./node_modules/hooper/dist/hooper.css");
/* harmony import */ var hooper_dist_hooper_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(hooper_dist_hooper_css__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    parameters: {
      type: Object
    }
  },
  components: {
    Hooper: hooper__WEBPACK_IMPORTED_MODULE_2__["Hooper"],
    Slide: hooper__WEBPACK_IMPORTED_MODULE_2__["Slide"],
    HooperNavigation: hooper__WEBPACK_IMPORTED_MODULE_2__["Navigation"],
    HooperPagination: hooper__WEBPACK_IMPORTED_MODULE_2__["Pagination"]
  },
  data: function data() {
    return {
      services: {},
      limit: this.parameters.limit,
      exclude: "",
      type: this.parameters.type,
      orderby: this.parameters.order_by
    };
  },
  mounted: function mounted() {
    this.getServices();
  },
  methods: {
    getServices: function getServices() {
      var _this = this;

      this.$emit("loader_token", true);
      var header = {};
      var data = {
        limit: this.limit,
        order_by: this.orderby
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_FRONT_SERVICE_LIST"], data, header).then(function (res) {
        _this.services = res.data.service;

        _this.$emit("loader_token", false);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.services && _vm.services.length
    ? _c("div", { staticClass: "suncity_services home-section" }, [
        _c("div", { staticClass: "container py-5" }, [
          _c("h3", [_vm._v(_vm._s(_vm.parameters.heading))]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _vm.parameters.subheading
            ? _c("p", { staticClass: "py-3 w-75 mx-auto" }, [
                _vm._v(
                  "\n      " + _vm._s(_vm.parameters.subheading) + "\n    "
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", [
            _c("div", { staticClass: "container py-5" }, [
              _vm.parameters.type == "slider"
                ? _c(
                    "div",
                    { staticClass: "row mt-3" },
                    [
                      _c(
                        "hooper",
                        {
                          staticStyle: { height: "320px" },
                          attrs: { itemsToShow: 3, infiniteScroll: true }
                        },
                        [
                          _vm._l(_vm.services, function(service, i) {
                            return _c(
                              "slide",
                              { key: i, staticClass: "px-2" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "card h-100",
                                    staticStyle: { border: "1px solid #ccc" }
                                  },
                                  [
                                    _c(
                                      "div",
                                      { staticClass: "card-body" },
                                      [
                                        _c("img", {
                                          staticClass: "card-img-top",
                                          staticStyle: {
                                            height: "200px",
                                            "object-fit": "cover"
                                          },
                                          attrs: {
                                            src: service.image_url.thumb,
                                            alt: service.title
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "router-link",
                                          {
                                            attrs: {
                                              to: "service/" + service.slug
                                            }
                                          },
                                          [
                                            _c(
                                              "h6",
                                              {
                                                staticClass: "card-title pt-3",
                                                staticStyle: {
                                                  overflow: "hidden",
                                                  "white-space": "nowrap",
                                                  "text-overflow": "ellipsis"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                      " +
                                                    _vm._s(service.title) +
                                                    "\n                    "
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            )
                          }),
                          _vm._v(" "),
                          _c("hooper-navigation", {
                            attrs: { slot: "hooper-addons" },
                            slot: "hooper-addons"
                          })
                        ],
                        2
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.parameters.type == "grid"
                ? _c(
                    "div",
                    { staticClass: "row mt-3" },
                    _vm._l(_vm.services, function(service, i) {
                      return _c(
                        "div",
                        { key: i, staticClass: "col-md-4 px-2 mb-3" },
                        [
                          _c(
                            "div",
                            {
                              staticClass: "card h-100",
                              staticStyle: { border: "1px solid #ccc" }
                            },
                            [
                              _c(
                                "div",
                                { staticClass: "card-body" },
                                [
                                  _c("img", {
                                    staticClass: "card-img-top",
                                    staticStyle: {
                                      height: "200px",
                                      "object-fit": "cover"
                                    },
                                    attrs: {
                                      src: service.image_url.thumb,
                                      alt: service.title
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "router-link",
                                    {
                                      attrs: { to: "service/" + service.slug }
                                    },
                                    [
                                      _c(
                                        "h6",
                                        {
                                          staticClass: "card-title pt-3",
                                          staticStyle: {
                                            overflow: "hidden",
                                            "white-space": "nowrap",
                                            "text-overflow": "ellipsis"
                                          }
                                        },
                                        [
                                          _vm._v(
                                            "\n                    " +
                                              _vm._s(service.title) +
                                              "\n                  "
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        ]
                      )
                    }),
                    0
                  )
                : _vm._e()
            ])
          ])
        ])
      ])
    : _vm._e()
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "circle_dot" }, [
      _c("span"),
      _vm._v(" "),
      _c("span"),
      _vm._v(" "),
      _c("span")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeService.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeService.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomeService.vue?vue&type=template&id=57beada4& */ "./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4&");
/* harmony import */ var _HomeService_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomeService.vue?vue&type=script&lang=js& */ "./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HomeService_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/homeComponent/HomeService.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeService_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeService.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeService_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeService.vue?vue&type=template&id=57beada4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeService.vue?vue&type=template&id=57beada4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeService_vue_vue_type_template_id_57beada4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);