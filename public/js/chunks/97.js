(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[97],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-tinymce-editor */ "./node_modules/vue-tinymce-editor/src/index.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // const baseDir = "/proAtm/" //development

var baseDir = "/"; //production



vue__WEBPACK_IMPORTED_MODULE_0___default.a.component("pagination", __webpack_require__(/*! laravel-vue-pagination */ "./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js"));
vue__WEBPACK_IMPORTED_MODULE_0___default.a.component("tinymce", vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_2__["default"]);
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    basicDetails: Object
  },
  data: function data() {
    return {
      sidebar: "AdminSidebar",
      user: JSON.parse(localStorage.getItem("user_info")),
      token: localStorage.getItem("token"),
      baseDir: baseDir
    };
  },
  mounted: function mounted() {
    console.log('this is running.');
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n*,\r\n::after,\r\n::before {\r\n  box-sizing: border-box;\n}\nbody,\r\nhtml {\r\n  height: 100%;\n}\nbody {\r\n  margin: 0;\r\n  overflow: hidden;\r\n  position: relative;\r\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Helvetica,\r\n    Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";\n}\n#header {\r\n  background: #fff;\r\n  height: 65px;\r\n  overflow: hiddent;\r\n  border-bottom: 1px solid #ddd;\r\n  position: absolute;\r\n  left: 0;\r\n  right: 0;\r\n  top: 0;\r\n  display: flex;\r\n  align-items: center;\r\n  padding: 0 10px;\n}\n#header .logo img {\r\n  height: 30px;\n}\n#header .logo a {\r\n  color: #1e5f8d;\r\n  display: flex;\r\n  align-items: center;\r\n  text-decoration: none;\r\n  font-size: 24px;\r\n  letter-spacing: -0.8px;\r\n  font-weight: 500;\n}\n#header .logo strong {\r\n  margin-left: 6px;\r\n  text-transform: uppercase;\n}\n#header .logo strong span {\r\n  color: #00a6eb;\n}\n.icon {\r\n  width: 24px;\r\n  height: 24px;\n}\n#preview {\r\n  position: absolute;\r\n  left: 0;\r\n  right: 0;\r\n  top: 65px;\r\n  bottom: 0;\r\n  transition: all 0.2s;\n}\n#preview-frame {\r\n  border: 0;\r\n  position: absolute;\r\n  transition: 0.5s;\n}\n.preview-desktop {\r\n  left: 0;\r\n  width: 100%;\r\n  height: 100%;\n}\n.preview-tablet {\r\n  width: 768px;\r\n  height: 100%;\r\n  left: calc(50% - 384px);\n}\n.preview-mobile {\r\n  width: 380px;\r\n  height: 680px;\r\n  left: calc(50% - 190px);\r\n  top: 0;\r\n  margin-top: 20px;\n}\n.preview-devices ul {\r\n  margin: 0 0 0 20px;\r\n  padding: 0;\r\n  list-style: none;\r\n  list-style-type: none;\r\n  display: flex;\r\n  align-items: center;\n}\n.preview-devices a {\r\n  transition: 0.3s;\r\n  border-bottom: 2px solid #fff;\r\n  color: #1e5f8d;\r\n  display: inline-block;\r\n  padding: 5px 10px;\r\n  margin: 0 5px;\n}\n.preview-devices a:hover {\r\n  color: #00a6eb;\n}\n.preview-devices .preview-devices-active a {\r\n  border-bottom: 2px solid #00a6eb;\r\n  color: #00a6eb;\n}\n.navigate {\r\n  margin-left: auto;\n}\n.navigate .icon-chevron-left,\r\n.navigate .icon-chevron-right {\r\n  width: 36px;\r\n  height: 36px;\n}\n.navigate ul {\r\n  padding: 0;\r\n  margin: 0;\r\n  list-style: none;\r\n  display: flex;\r\n  align-items: center;\n}\n.navigate li {\r\n  margin: 0 0 0 5px;\n}\n.navigate li:last-child {\r\n  margin-right: 0;\n}\n.navigate a {\r\n  transition: 0.3s;\r\n  padding: 0 10px;\r\n  border: solid 1px #dfdfdf;\r\n  border-radius: 4px;\r\n  color: #828282;\r\n  display: flex;\r\n  align-items: center;\r\n  height: 40px;\r\n  justify-content: center;\r\n  text-decoration: none;\n}\n.navigate a:hover {\r\n  border: solid 1px #00a6eb;\r\n  color: #00a6eb;\n}\n.navigate a.download {\r\n  background: #00a6eb;\r\n  border: solid 1px #00a6eb;\r\n  color: #fff;\r\n  font-size: 15px;\r\n  padding: 0 16px;\n}\n.navigate a.download span {\r\n  padding-left: 4px;\n}\n.navigate a.download:hover {\r\n  background: #00b4ff;\r\n  border-color: #00b4ff;\n}\n.navigate a.first-latest {\r\n  background: #e6e6e6;\r\n  border: 1px solid #e6e6e6;\r\n  color: #fff;\n}\n@media (max-width: 1024px) {\n.preview-devices {\r\n    display: none;\n}\n}\n@media (max-width: 768px) {\n.logo {\r\n    display: none;\n}\n.navigate {\r\n    margin: 0;\r\n    width: calc(100% - 4px);\n}\n.navigate ul {\r\n    justify-content: center;\n}\n.navigate li {\r\n    flex-grow: 1;\r\n    flex-basis: 0;\n}\n.navigate a.download span {\r\n    display: none;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("header", { attrs: { id: "header" } }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "preview-devices" }, [
        _c("ul", [
          _c(
            "li",
            {
              staticClass: "preview-test",
              attrs: {
                id: "preview-test-desktop",
                title: "Desktop preview of the  Presento template"
              }
            },
            [
              _c("a", { attrs: { href: "" } }, [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-preview",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#preview"
                      }
                    })
                  ]
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "preview-test",
              attrs: {
                id: "preview-test-tablet",
                title: "Tablet preview of the Presento template"
              }
            },
            [
              _c("a", { attrs: { href: "" } }, [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-tablet",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#tablet"
                      }
                    })
                  ]
                )
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "preview-test preview-devices-active",
              attrs: {
                id: "preview-test-mobile",
                title: "Mobile preview of the Presento template"
              }
            },
            [
              _c("a", { attrs: { href: "" } }, [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-smartphone",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#smartphone"
                      }
                    })
                  ]
                )
              ])
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "navigate" }, [
        _c("ul", [
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href: "https://bootstrapmade.com/demo/templates/Presento/",
                  target: "_top",
                  title: "Hide the demo bar"
                }
              },
              [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-external-link",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#external-link"
                      }
                    })
                  ]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href:
                    "https://bootstrapmade.com/presento-bootstrap-corporate-template/",
                  title: "Presento Template Home Page"
                }
              },
              [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-home",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#home"
                      }
                    })
                  ]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                staticClass: "download",
                attrs: {
                  href:
                    "https://bootstrapmade.com/presento-bootstrap-corporate-template/#download",
                  title: "Download the Presento Template"
                }
              },
              [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-download",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#download"
                      }
                    })
                  ]
                ),
                _vm._v(" "),
                _c("span", [_vm._v("Free Download")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href: "https://bootstrapmade.com/demo/FlexStart/",
                  title: "Previous Template: FlexStart",
                  target: "_top"
                }
              },
              [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-chevron-left",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#chevron-left"
                      }
                    })
                  ]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", [
            _c(
              "a",
              {
                attrs: {
                  href: "https://bootstrapmade.com/demo/BizLand/",
                  title: "Next Template: BizLand",
                  target: "_top"
                }
              },
              [
                _c(
                  "svg",
                  {
                    staticClass: "icon icon-chevron-right",
                    attrs: { fill: "currentColor" }
                  },
                  [
                    _c("use", {
                      attrs: {
                        "xlink:href":
                          "https://bootstrapmade.com/theme/img/icons-2.7.0.svg#chevron-right"
                      }
                    })
                  ]
                )
              ]
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _vm._m(1)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "logo" }, [
      _c(
        "a",
        {
          attrs: {
            href: "https://bootstrapmade.com/",
            rel: "home",
            title: "Free Bootstrap Themes and Website Templates"
          }
        },
        [
          _c("img", {
            attrs: {
              alt: "BootstrapMade",
              src: "https://bootstrapmade.com/theme/img/logo.png"
            }
          }),
          _c("strong", [_vm._v("Bootstrap"), _c("span", [_vm._v("Made")])])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "preview" } }, [
      _c("iframe", {
        staticClass: "preview-desktop",
        attrs: {
          id: "preview-frame",
          src: "http://localhost:8000/",
          frameborder: "0"
        }
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/preview.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/backend/preview.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preview.vue?vue&type=template&id=44c7ad5c& */ "./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&");
/* harmony import */ var _preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preview.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/preview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./preview.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/preview.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=template&id=44c7ad5c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);