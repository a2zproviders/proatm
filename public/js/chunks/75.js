(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[75],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      type: "pending",
      formdata: {
        data: {}
      },
      deleteItems: [],
      all_select: false,
      status: [],
      statusArr: ["pending", "complete", "cancel"],
      action: "",
      token: localStorage.getItem("token"),
      search: "",
      trash: 0,
      drafted: false,
      counts: {},
      reply: ""
    };
  },
  methods: {
    searchList: lodash__WEBPACK_IMPORTED_MODULE_0___default.a.debounce(function () {
      this.listPost(1);
    }, 500),
    listPost: function listPost() {
      var _this = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_INQUERY"] + "/?page=" + page + "&type=admin-list&s=" + this.search + "&status=" + this.type, header).then(function (res) {
        // console.log("page data", res.data.page);
        _this.formdata.data = res.data.post;
        _this.counts = res.data.counts;
      });
    },
    ToggleTrash: function ToggleTrash() {
      this.trash = !this.trash;
      this.listPost();
    },
    showDrafted: function showDrafted(drafted) {
      this.trash = false;
      this.drafted = drafted;
      this.listPost();
    },
    updateStatus: function updateStatus(i, event) {
      var _this2 = this;

      var status = event.target.value;
      this.$swal({
        title: "Are you sure?",
        text: "You will change status this record!",
        input: "text",
        inputPlaceholder: "Write something",
        showCancelButton: true,
        confirmButtonText: "Yes",
        cancelButtonText: "No, keep it"
      }).then(function (result) {
        if (result.isConfirmed) {
          var id = _this2.formdata.data.data[i].id;
          _this2.formdata.data.data[i].status = status;
          var params = {
            record: {
              status: status,
              reply: result.value
            }
          };
          var header = {
            Authorization: "Bearer " + _this2.token
          };
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_INQUERY_STATUS"] + "/" + id, params, header).then(function (res) {
            _this2.listPost();
          });
        }
      });
    },
    deleteRow: function deleteRow(id, flag) {
      var _this3 = this;

      var msg = "",
          delete_record = "";

      if (flag == "Delete") {
        msg = "You will not be able to recover this record!";
        delete_record = "Yes, delete it!";
      } else if (flag == "Trash") {
        msg = "You will move this record to trash!";
        delete_record = "Yes, trash it!";
      } else {
        msg = "You will restore this record!";
        delete_record = "Yes, restore it!";
      }

      this.$swal({
        title: "Are you sure?",
        text: msg,
        showCancelButton: true,
        confirmButtonText: delete_record,
        cancelButtonText: "No, keep it"
      }).then(function (result) {
        if (result.value) {
          var header = {
            Authorization: "Bearer " + _this3.token
          };
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["DELETE"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_INQUERY"] + "/" + id + "?flag=" + flag, header).then(function (res) {
            _this3.trash = false;

            _this3.listPost(1);
          });
        }
      });
    },
    multipalDelete: function multipalDelete() {
      var _this4 = this;

      if (this.action != 0) {
        var header = {
          Authorization: "Bearer " + this.token
        };

        if (this.deleteItems.length == 0) {
          this.$swal("Warning", "please select an item", "Ok");
        } else {
          var formData = new FormData();
          formData.append("ids", this.deleteItems);
          formData.append("action", this.action);
          Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_INQUERY_DELETE"], formData, header).then(function (res) {
            if (res.data.status) {
              _this4.deleteItems = [];
              _this4.all_select = false;
              _this4.action = "";

              _this4.listPost();
            } else {}
          });
        }
      } else {
        this.$swal("Warning", "please select a valid action", "Ok");
      }
    },
    typeChange: function typeChange() {
      this.listPost();
    },
    select_all_via_check_box: function select_all_via_check_box() {
      var _this5 = this;

      if (this.all_select == false) {
        this.all_select = true;
        this.deleteItems = [];
        this.formdata.data.data.forEach(function (item) {
          _this5.deleteItems.push(item.id);
        });
      } else {
        this.all_select = false;
        this.deleteItems = [];
      }
    }
  },
  mounted: function mounted() {
    this.listPost();
  },
  watch: {
    deleteItems: function deleteItems() {
      if (this.formdata.data && this.formdata.data.data.length == this.deleteItems.length) {
        this.all_select = true;
      } else {
        this.all_select = false;
      }
    },
    search: function search() {
      this.searchList();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("Inquery List")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "adminDashboard" } } }, [
                _vm._v("Home")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("\n          Inquery List\n        ")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-sm-8" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: { type: "search", placeholder: "search here..." },
                    domProps: { value: _vm.search },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.search = $event.target.value
                      }
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-4 text-right" })
            ]),
            _vm._v(" "),
            !_vm.formdata.data.data || !_vm.formdata.data.data.length
              ? _c("div", { staticClass: "alert alert-danger" }, [
                  _vm._v("\n            No records found.\n          ")
                ])
              : _vm._e(),
            _vm._v(" "),
            _c(
              "div",
              {},
              [
                _c("div", { staticClass: "float-right py-2" }, [
                  _vm._v(
                    "\n              " +
                      _vm._s(_vm.formdata.data.from) +
                      " - " +
                      _vm._s(_vm.formdata.data.to) +
                      " of\n              " +
                      _vm._s(_vm.formdata.data.total) +
                      " record(s) are showing.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-description form-group w-50" }, [
                  _c("div", { staticClass: "row" }, [
                    _c("div", { staticClass: "col-sm-4" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.action,
                              expression: "action"
                            }
                          ],
                          staticClass: "form-control",
                          staticStyle: { padding: "10px" },
                          on: {
                            change: [
                              function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.action = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              },
                              _vm.multipalDelete
                            ]
                          }
                        },
                        [
                          _c("option", { attrs: { value: "" } }, [
                            _vm._v("Bulk Action")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "Delete" } }, [
                            _vm._v("Delete")
                          ])
                        ]
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "col-sm-4" }, [
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.type,
                              expression: "type"
                            }
                          ],
                          staticClass: "form-control",
                          staticStyle: { padding: "10px" },
                          on: {
                            change: [
                              function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.type = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              },
                              _vm.typeChange
                            ]
                          }
                        },
                        [
                          _c("option", { attrs: { value: "pending" } }, [
                            _vm._v("Pending")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "complete" } }, [
                            _vm._v("Complete")
                          ]),
                          _vm._v(" "),
                          _c("option", { attrs: { value: "cancel" } }, [
                            _vm._v("Cancel")
                          ])
                        ]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "table-responsive" }, [
                  _vm.formdata.data.data && _vm.formdata.data.data.length
                    ? _c("table", { staticClass: "table table-striped" }, [
                        _c("thead", { staticClass: "bg-dark text-light" }, [
                          _c("tr", [
                            _c("th", [
                              _c("label", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.all_select,
                                      expression: "all_select"
                                    }
                                  ],
                                  attrs: { type: "checkbox" },
                                  domProps: {
                                    checked: Array.isArray(_vm.all_select)
                                      ? _vm._i(_vm.all_select, null) > -1
                                      : _vm.all_select
                                  },
                                  on: {
                                    click: _vm.select_all_via_check_box,
                                    change: function($event) {
                                      var $$a = _vm.all_select,
                                        $$el = $event.target,
                                        $$c = $$el.checked ? true : false
                                      if (Array.isArray($$a)) {
                                        var $$v = null,
                                          $$i = _vm._i($$a, $$v)
                                        if ($$el.checked) {
                                          $$i < 0 &&
                                            (_vm.all_select = $$a.concat([$$v]))
                                        } else {
                                          $$i > -1 &&
                                            (_vm.all_select = $$a
                                              .slice(0, $$i)
                                              .concat($$a.slice($$i + 1)))
                                        }
                                      } else {
                                        _vm.all_select = $$c
                                      }
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("span", [
                                  _vm._v(
                                    "\n                          " +
                                      _vm._s(
                                        _vm.all_select == true
                                          ? "Uncheck All"
                                          : "Select All"
                                      ) +
                                      "\n                        "
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("th", [_vm._v("Name")]),
                            _vm._v(" "),
                            _c("th", [_vm._v("Email")]),
                            _vm._v(" "),
                            _c("th", [_vm._v("Mobile")]),
                            _vm._v(" "),
                            _c("th", [_vm._v("Message")]),
                            _vm._v(" "),
                            _c("th", [_vm._v("Status")]),
                            _vm._v(" "),
                            _vm.type != "pending"
                              ? _c("th", [_vm._v("Reply")])
                              : _vm._e()
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(this.formdata.data.data, function(page, i) {
                            return _c("tr", { key: i }, [
                              _c("td", [
                                _c("label", [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.deleteItems,
                                        expression: "deleteItems"
                                      }
                                    ],
                                    attrs: { type: "checkbox" },
                                    domProps: {
                                      value: page.id,
                                      checked: Array.isArray(_vm.deleteItems)
                                        ? _vm._i(_vm.deleteItems, page.id) > -1
                                        : _vm.deleteItems
                                    },
                                    on: {
                                      change: function($event) {
                                        var $$a = _vm.deleteItems,
                                          $$el = $event.target,
                                          $$c = $$el.checked ? true : false
                                        if (Array.isArray($$a)) {
                                          var $$v = page.id,
                                            $$i = _vm._i($$a, $$v)
                                          if ($$el.checked) {
                                            $$i < 0 &&
                                              (_vm.deleteItems = $$a.concat([
                                                $$v
                                              ]))
                                          } else {
                                            $$i > -1 &&
                                              (_vm.deleteItems = $$a
                                                .slice(0, $$i)
                                                .concat($$a.slice($$i + 1)))
                                          }
                                        } else {
                                          _vm.deleteItems = $$c
                                        }
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("span", [
                                    _vm._v(
                                      " " +
                                        _vm._s(i + _vm.formdata.data.from) +
                                        ". "
                                    )
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  "\n                      " +
                                    _vm._s(page.name) +
                                    "\n                      "
                                ),
                                _c("div", { staticClass: "mt-2" }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-link p-0",
                                      attrs: { type: "button", name: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteRow(
                                            page.id,
                                            "Delete"
                                          )
                                        }
                                      }
                                    },
                                    [
                                      _c("i", {
                                        staticClass:
                                          "mdi mdi-delete text-danger"
                                      }),
                                      _vm._v(
                                        " Delete\n                        "
                                      )
                                    ]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c("td", { staticClass: "py-1" }, [
                                _vm._v(
                                  "\n                      " +
                                    _vm._s(page.email) +
                                    "\n                    "
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  "\n                      " +
                                    _vm._s(page.mobile) +
                                    "\n                    "
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  "\n                      " +
                                    _vm._s(page.message) +
                                    "\n                    "
                                )
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "select",
                                  {
                                    staticClass: "form-control",
                                    on: {
                                      change: function($event) {
                                        return _vm.updateStatus(i, $event)
                                      }
                                    }
                                  },
                                  [
                                    page.status == "pending"
                                      ? _c(
                                          "option",
                                          {
                                            attrs: {
                                              selected: "",
                                              disabled: "",
                                              value: "pending"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          Pending\n                        "
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    page.status == "complete"
                                      ? _c(
                                          "option",
                                          {
                                            attrs: {
                                              selected: "",
                                              disabled: "",
                                              value: "complete"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          Complete\n                        "
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    page.status == "complete" ||
                                    page.status != "cancel"
                                      ? _c(
                                          "option",
                                          { attrs: { value: "complete" } },
                                          [
                                            _vm._v(
                                              "\n                          Complete\n                        "
                                            )
                                          ]
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    page.status == "cancel"
                                      ? _c(
                                          "option",
                                          {
                                            attrs: {
                                              selected: "",
                                              disabled: "",
                                              value: "cancel"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          Cancel\n                        "
                                            )
                                          ]
                                        )
                                      : _c(
                                          "option",
                                          { attrs: { value: "cancel" } },
                                          [_vm._v("Cancel")]
                                        )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              page.status != "pending"
                                ? _c("td", [
                                    _vm._v(
                                      "\n                      " +
                                        _vm._s(
                                          page.reply ? page.reply : "N/A"
                                        ) +
                                        "\n                    "
                                    )
                                  ])
                                : _vm._e()
                            ])
                          }),
                          0
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("pagination", {
                  attrs: { data: _vm.formdata.data },
                  on: { "pagination-change-page": _vm.listPost }
                })
              ],
              1
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/inquery/View.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/backend/inquery/View.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./View.vue?vue&type=template&id=595b98f7& */ "./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7&");
/* harmony import */ var _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./View.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/inquery/View.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/inquery/View.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./View.vue?vue&type=template&id=595b98f7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/inquery/View.vue?vue&type=template&id=595b98f7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_View_vue_vue_type_template_id_595b98f7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);