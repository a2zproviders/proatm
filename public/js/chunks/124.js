(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[124],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_picture_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-picture-input */ "./node_modules/vue-picture-input/PictureInput.vue");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  title: "AddService",
  components: {
    PictureInput: vue_picture_input__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      id: null,
      formdata: {
        title: "",
        slug: "",
        description: "",
        short_description: "",
        seo_title: "",
        seo_keywords: "",
        seo_description: "",
        category_id: "",
        icon: ""
      },
      categories: {},
      image: {},
      token: localStorage.getItem("token"),
      tinyOptions: {
        height: 300,
        branding: false
      }
    };
  },
  validations: {
    formdata: {
      title: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      slug: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      short_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(300)
      },
      seo_title: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(75)
      },
      seo_keywords: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(150)
      },
      seo_description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(250)
      }
    }
  },
  mounted: function mounted() {
    var id = this.$route.params.Tid;

    if (id) {
      this.id = id;
      this.getInfo();
    }

    this.getCategory();
  },
  computed: {
    slug: function slug() {
      if (this.formdata.title) {
        var slug = this.formdata.title.toString().trim().toLowerCase() // LowerCase
        .replace(/\s+/g, "-") // space to -
        .replace(/&/g, "-and-") // & to and
        .replace(/\?/g, "") // & to and
        .replace(/--/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
        return slug;
      } else {
        return "";
      }
    }
  },
  watch: {
    slug: function slug() {
      this.formdata.slug = this.slug;
    }
  },
  methods: {
    getInfo: function getInfo() {
      var _this = this;

      var id = this.$route.params.Tid;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_SERVICE"] + "/" + id, header).then(function (res) {
        _this.formdata = res.data;
        console.log("response", res.data);
        _this.formdata.category_id = res.data.category_id && res.data.category_id != "null" ? res.data.category_id : "";
      });
    },
    getCategory: function getCategory() {
      var _this2 = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_CATEGORY_ALL"] + "/Service", header).then(function (res) {
        _this2.categories = res.data;
      });
    },
    storeService: function storeService() {
      var _this3 = this;

      // let id = this.$route.params.Pid
      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token,
          "Content-Type": "multipart/form-data"
        };
        var formData = new FormData();
        formData.append("record[title]", this.formdata.title);
        formData.append("record[slug]", this.formdata.slug);
        formData.append("record[short_description]", this.formdata.short_description);
        formData.append("record[description]", this.formdata.description);
        formData.append("record[icon]", this.formdata.icon);
        formData.append("image", this.image);
        formData.append("record[seo_title]", this.formdata.seo_title);
        formData.append("record[seo_keywords]", this.formdata.seo_keywords);
        formData.append("record[seo_description]", this.formdata.seo_description);
        formData.append("record[category_id]", this.formdata.category_id);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_SERVICE"], formData, header).then(function (res) {
          _this3.$swal("Sucecess", "Record added successfully", "Ok").then(function (result) {
            if (result.value) {
              _this3.$router.push({
                name: "ViewService"
              });
            }
          });
        });
      }
    },
    updateData: function updateData() {
      var _this4 = this;

      this.$v.formdata.$touch();

      if (!this.$v.formdata.$invalid) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        var formData = new FormData();
        formData.append("_method", "put");
        formData.append("record[title]", this.formdata.title);
        formData.append("record[slug]", this.formdata.slug);
        formData.append("record[short_description]", this.formdata.short_description);
        formData.append("record[description]", this.formdata.description);
        formData.append("record[icon]", this.formdata.icon);
        formData.append("image", this.image);
        formData.append("record[seo_title]", this.formdata.seo_title);
        formData.append("record[seo_keywords]", this.formdata.seo_keywords);
        formData.append("record[seo_description]", this.formdata.seo_description);
        formData.append("record[category_id]", this.formdata.category_id);
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_SERVICE"] + "/" + this.id, formData, header).then(function (res) {
          _this4.$swal("Success", "Record updated successfully", "Ok").then(function (result) {
            if (result.value) {
              _this4.$router.push({
                name: "ViewTestimonial"
              });
            }
          });
        });
      }
    },
    onChanged: function onChanged() {
      console.log("new picture loaded", this.$refs.pictureInput.file);

      if (this.$refs.pictureInput.file) {
        this.image = this.$refs.pictureInput.file;
      } else {
        console.log("old browser. No support for FilereaderApi");
      }

      console.log(this.image);
    },
    onRemoved: function onRemoved() {
      this.formdata.image = "";
      console.log(this.formdata.image);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [
        _vm._v(_vm._s(_vm.id ? "Edit" : "Add") + " Services")
      ]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "ViewService" } } }, [
                _vm._v("View service")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("\n          Service page\n        ")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    !_vm.id ? _vm.storeService() : _vm.updateData()
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-8" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "title" } }, [
                        _vm._v("Title")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.title.$model,
                            expression: "$v.formdata.title.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.title.$error },
                        attrs: {
                          type: "text",
                          id: "title",
                          placeholder: "Service title",
                          autocomplete: "off"
                        },
                        domProps: { value: _vm.$v.formdata.title.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.title,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.title.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "slug" } }, [_vm._v("Slug")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.slug.$model,
                            expression: "$v.formdata.slug.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.$v.formdata.slug.$error },
                        attrs: {
                          type: "text",
                          id: "slug",
                          placeholder: "Service slug",
                          autocomplete: "off"
                        },
                        domProps: { value: _vm.$v.formdata.slug.$model },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.slug,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.slug.required
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Field is required\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "short_description" } }, [
                        _vm._v("Short description")
                      ]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model.trim",
                            value: _vm.$v.formdata.short_description.$model,
                            expression: "$v.formdata.short_description.$model",
                            modifiers: { trim: true }
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.$v.formdata.short_description.$error
                        },
                        attrs: {
                          id: "short_description",
                          rows: "4",
                          placeholder: "Enter short description"
                        },
                        domProps: {
                          value: _vm.$v.formdata.short_description.$model
                        },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.$v.formdata.short_description,
                              "$model",
                              $event.target.value.trim()
                            )
                          },
                          blur: function($event) {
                            return _vm.$forceUpdate()
                          }
                        }
                      }),
                      _vm._v(" "),
                      !_vm.$v.formdata.short_description.maxLength
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(
                              "\n                    Short description must not have more than\n                    " +
                                _vm._s(
                                  _vm.$v.formdata.short_description.$params
                                    .maxLength.max
                                ) +
                                "\n                    letters.\n                  "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "icon" } }, [
                        _vm._v("Service icon")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.formdata.icon,
                            expression: "formdata.icon"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "icon",
                          placeholder: "Service icon ex. fa fa-facebook"
                        },
                        domProps: { value: _vm.formdata.icon },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.formdata, "icon", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "icon" } }, [
                        _vm._v("Select Category")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.formdata.category_id,
                              expression: "formdata.category_id"
                            }
                          ],
                          staticClass: "form-control",
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.formdata,
                                "category_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        [
                          _c("option", { attrs: { value: "" } }, [
                            _vm._v("select category")
                          ]),
                          _vm._v(" "),
                          _vm._l(_vm.categories, function(name, id) {
                            return _c(
                              "option",
                              { key: id, domProps: { value: id } },
                              [
                                _vm._v(
                                  "\n                      " +
                                    _vm._s(name) +
                                    "\n                    "
                                )
                              ]
                            )
                          })
                        ],
                        2
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Image upload")]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "input-group col-xs-12 bg-light" },
                        [
                          _c("picture-input", {
                            ref: "pictureInput",
                            attrs: {
                              zIndex: -1,
                              crop: false,
                              removable: false,
                              width: 364,
                              height: 280,
                              removeButtonClass: "btn btn-danger",
                              accept: "image/jpeg, image/png, image/gif",
                              buttonClass: "btn btn-primary",
                              customStrings: {
                                upload: "<h1>Upload it!</h1>",
                                drag: "Drag and drop your image here"
                              }
                            },
                            on: { change: _vm.onChanged, remove: _vm.onRemoved }
                          })
                        ],
                        1
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "form-group" },
                  [
                    _c("label", { attrs: { for: "description" } }, [
                      _vm._v("Description")
                    ]),
                    _vm._v(" "),
                    _c("tinymce", {
                      attrs: { id: "d1", other_options: _vm.tinyOptions },
                      model: {
                        value: _vm.formdata.description,
                        callback: function($$v) {
                          _vm.$set(_vm.formdata, "description", $$v)
                        },
                        expression: "formdata.description"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "py-3 pl-2 mb-3 font-weight-bold",
                    staticStyle: {
                      background: "linear-gradient(to right, #da8cff, #9a55ff)"
                    }
                  },
                  [_vm._v("\n              Meta Tag\n            ")]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_title" } }, [
                    _vm._v("Meta title(75)")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_title.$model,
                        expression: "$v.formdata.seo_title.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: { "is-invalid": _vm.$v.formdata.seo_title.$error },
                    attrs: {
                      type: "text",
                      id: "meta_title",
                      placeholder: "SEO title"
                    },
                    domProps: { value: _vm.$v.formdata.seo_title.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_title,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_title.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Seo title must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_title.$params.maxLength.max
                            ) +
                            " letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_keywords" } }, [
                    _vm._v("Meta keywords(150)")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_keywords.$model,
                        expression: "$v.formdata.seo_keywords.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: {
                      "is-invalid": _vm.$v.formdata.seo_keywords.$error
                    },
                    attrs: {
                      id: "meta_keywords",
                      rows: "4",
                      placeholder: "seo keywords"
                    },
                    domProps: { value: _vm.$v.formdata.seo_keywords.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_keywords,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_keywords.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Seo keywords must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_keywords.$params.maxLength.max
                            ) +
                            " letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "meta_description" } }, [
                    _vm._v("Meta description(250)")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model.trim",
                        value: _vm.$v.formdata.seo_description.$model,
                        expression: "$v.formdata.seo_description.$model",
                        modifiers: { trim: true }
                      }
                    ],
                    staticClass: "form-control",
                    class: {
                      "is-invalid": _vm.$v.formdata.seo_description.$error
                    },
                    attrs: {
                      id: "meta_description",
                      rows: "4",
                      placeholder: "seo description"
                    },
                    domProps: { value: _vm.$v.formdata.seo_description.$model },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.$v.formdata.seo_description,
                          "$model",
                          $event.target.value.trim()
                        )
                      },
                      blur: function($event) {
                        return _vm.$forceUpdate()
                      }
                    }
                  }),
                  _vm._v(" "),
                  !_vm.$v.formdata.seo_description.maxLength
                    ? _c("div", { staticClass: "invalid-feedback" }, [
                        _vm._v(
                          "\n                Short description must not have more than\n                " +
                            _vm._s(
                              _vm.$v.formdata.seo_description.$params.maxLength
                                .max
                            ) +
                            "\n                letters.\n              "
                        )
                      ])
                    : _vm._e()
                ]),
                _vm._v(" "),
                !_vm.id
                  ? _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-2",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("\n              Submit\n            ")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.id
                  ? _c(
                      "button",
                      {
                        staticClass: "btn btn-gradient-primary mr-2",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("\n              Update\n            ")]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c("button", { staticClass: "btn btn-light" }, [
                  _vm._v("Cancel")
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/services/Add.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/backend/services/Add.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Add.vue?vue&type=template&id=68816a36& */ "./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36&");
/* harmony import */ var _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Add.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/services/Add.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/services/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=template&id=68816a36& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/services/Add.vue?vue&type=template&id=68816a36&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_68816a36___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);