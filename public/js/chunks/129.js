(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[129],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/api.js */ "./resources/js/services/api.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "GeneralSetting",
  data: function data() {
    return {
      user: {
        title: "Mr.",
        name: "",
        first_name: "",
        last_name: "",
        email: "",
        mobile: "",
        gender: ""
      },
      loading: false,
      token: localStorage.getItem("token")
    };
  },
  computed: {
    name: function name() {
      return (this.user.title + " " + this.user.first_name + " " + this.user.last_name).trim();
    }
  },
  watch: {
    name: function name() {
      this.user.name = this.name;
    }
  },
  mounted: function mounted() {
    this.getInfo();
    this.$emit("changeTab", this.$route.name);
  },
  methods: {
    getInfo: function getInfo() {
      var _this = this;

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_2__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_1__["STORE_USER_DETAILS"], header).then(function (res) {
        _this.user = res.data.user;
      });
    },
    submitForm: function submitForm() {
      var _this2 = this;

      var data = {
        user: this.user
      };
      this.loading = true;
      Object(_services_api_js__WEBPACK_IMPORTED_MODULE_0__["update_profile"])(data).then(function (res) {
        _this2.loading = false;

        _this2.$swal("Success!", res.data.message, "success");
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("General Settings")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "adminDashboard" } } }, [
                _vm._v("Dashboard")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "Settings" } } }, [
                _vm._v("Settings")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("\n          General Settings\n        ")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            !_vm.user.id
              ? _c(
                  "div",
                  { staticClass: "text-center" },
                  [
                    _c("b-spinner", {
                      staticStyle: { width: "100px", height: "100px" }
                    })
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.user.id
              ? _c(
                  "form",
                  {
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submitForm($event)
                      }
                    }
                  },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-2" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "title" } }, [
                            _vm._v("Title")
                          ]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.title,
                                  expression: "user.title"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "title" },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.user,
                                    "title",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "Mr." } }, [
                                _vm._v("Mr.")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Ms." } }, [
                                _vm._v("Ms.")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Mrs." } }, [
                                _vm._v("Mrs.")
                              ])
                            ]
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-5" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "first_name" } }, [
                            _vm._v("First Name")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.first_name,
                                expression: "user.first_name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "first_name",
                              placeholder: "Enter first name"
                            },
                            domProps: { value: _vm.user.first_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.user,
                                  "first_name",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-5" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "last_name" } }, [
                            _vm._v("Last Name")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.last_name,
                                expression: "user.last_name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "last_name",
                              placeholder: "Enter last name"
                            },
                            domProps: { value: _vm.user.last_name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.user,
                                  "last_name",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "name" } }, [
                            _vm._v("Display Name")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.name,
                                expression: "user.name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "name",
                              placeholder: "Enter first name"
                            },
                            domProps: { value: _vm.user.name },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.user, "name", $event.target.value)
                              }
                            }
                          })
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "user_email" } }, [
                            _vm._v("Email")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.email,
                                expression: "user.email"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "email",
                              id: "user_email",
                              placeholder: "Enter email"
                            },
                            domProps: { value: _vm.user.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.user, "email", $event.target.value)
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "user_mobile" } }, [
                            _vm._v("Mobile number")
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.user.mobile,
                                expression: "user.mobile"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "user_mobile",
                              placeholder: "Enter mobile no."
                            },
                            domProps: { value: _vm.user.mobile },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.user,
                                  "mobile",
                                  $event.target.value
                                )
                              }
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", { attrs: { for: "gender" } }, [
                            _vm._v("Gender")
                          ]),
                          _vm._v(" "),
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.user.gender,
                                  expression: "user.gender"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "gender" },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.$set(
                                    _vm.user,
                                    "gender",
                                    $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  )
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "" } }, [
                                _vm._v("Select gender")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Male" } }, [
                                _vm._v("Male")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Female" } }, [
                                _vm._v("Female")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Other" } }, [
                                _vm._v("Other")
                              ])
                            ]
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success float-right ml-3",
                          attrs: { type: "submit", disabled: _vm.loading }
                        },
                        [_c("span", [_vm._v("Update")])]
                      ),
                      _vm._v(" "),
                      _vm.loading
                        ? _c(
                            "div",
                            [
                              _vm.loading ? _c("b-spinner") : _vm._e(),
                              _vm._v(
                                " In Progress, please\n                wait…\n              "
                              )
                            ],
                            1
                          )
                        : _vm._e()
                    ])
                  ]
                )
              : _vm._e()
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/setting/Edit.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/backend/setting/Edit.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=6dadacef& */ "./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/setting/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=6dadacef& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/Edit.vue?vue&type=template&id=6dadacef&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_6dadacef___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  //prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);