(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Loader",
  props: {
    isVisible: {
      type: Boolean,
      required: true
    },
    text: {
      type: String,
      required: false,
      "default": ""
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    page: Object,
    items: Array
  },
  computed: {
    backgroundImage: function backgroundImage() {
      return this.page.image ? this.page.image : "";
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/service/single.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery_ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery-ui */ "./node_modules/jquery-ui/ui/widget.js");
/* harmony import */ var jquery_ui__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_ui__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery_ui_ui_widgets_accordion__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery-ui/ui/widgets/accordion */ "./node_modules/jquery-ui/ui/widgets/accordion.js");
/* harmony import */ var jquery_ui_ui_widgets_accordion__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery_ui_ui_widgets_accordion__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var _common_LoaderComponent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../common/LoaderComponent */ "./resources/js/components/common/LoaderComponent.vue");
/* harmony import */ var _components_PageHeader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/PageHeader */ "./resources/js/components/front/components/PageHeader.vue");
/* harmony import */ var _services_api_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/api.js */ "./resources/js/services/api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Loader: _common_LoaderComponent__WEBPACK_IMPORTED_MODULE_5__["default"],
    PageHeader: _components_PageHeader__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      service: {},
      points: {},
      categories: {},
      isLoading: false
    };
  },
  metaInfo: function metaInfo() {
    var title = this.service.seo_title !== null && typeof this.service.seo_title !== "undefined" ? this.service.seo_title : this.service.title; // console.log('seo title', this.service.seo_title);

    return {
      title: "".concat(title),
      meta: [{
        name: "description",
        content: this.service.seo_description
      }, {
        property: "og:title",
        content: this.service.seo_title + " - ProATM"
      }, {
        property: "og:site_name",
        content: "ProATM"
      }, {
        property: "og:description",
        content: this.service.seo_description
      }, {
        property: "og:type",
        content: "profile"
      }, {
        property: "og:url",
        content: ""
      }, {
        property: "og:image",
        content: ""
      }]
    };
  },
  mounted: function mounted() {
    setTimeout(function () {
      jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).on("click", ".ul_accordion>li>a", function (e) {
        e.preventDefault();
        var sel = e.currentTarget;
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(sel).closest("li").find("ul").slideToggle();
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(sel).find(".icon").toggleClass("icon-minus2 icon-plus2");
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(".ul_accordion>li>a").not(sel).closest("li").find("ul").slideUp();
        jquery__WEBPACK_IMPORTED_MODULE_0___default()(".ul_accordion>li>a").not(sel).find(".icon").removeClass("icon-minus2").addClass("icon-plus2");
      });
    }, 10);
    this.getServices();
    this.getCategory();
  },
  methods: {
    getCategory: function getCategory() {
      var _this = this;

      var header = {};
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_SERVICE_CATEGORY_ALL"] + "/Service", header).then(function (res) {
        _this.categories = res.data;
      });
    },
    getServices: function getServices() {
      var _this2 = this;

      this.isLoading = true;
      var id = this.$route.params.slug;
      var header = {};
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_4__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_3__["STORE_FRONT_SERVICE"] + "/" + id, header).then(function (res) {
        var _res$data$data$seo_ti;

        _this2.service = res.data.data;
        _this2.isLoading = false;
        _this2.points = res.data.points;
        Object(_services_api_js__WEBPACK_IMPORTED_MODULE_7__["update_meta"])({
          title: (_res$data$data$seo_ti = res.data.data.seo_title) !== null && _res$data$data$seo_ti !== void 0 ? _res$data$data$seo_ti : res.data.data.title,
          keywords: res.data.data.keywords,
          description: res.data.data.description
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".loader-overlay {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  background-color: rgba(0, 0, 0, 0.7);\n  z-index: 999;\n  cursor: pointer;\n}\n.loader-overlay span.text {\n  display: inline-block;\n  position: relative;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  color: #fff;\n}\n.loader-overlay .loader {\n  -webkit-animation: loader-animate 1.5s linear infinite;\n          animation: loader-animate 1.5s linear infinite;\n  clip: rect(0, 80px, 80px, 0px);\n  height: 80px;\n  width: 80px;\n  position: absolute;\n  left: calc(50% - 40px);\n  top: calc(50% - 40px);\n}\n.loader-overlay .loader:after {\n  -webkit-animation: loader-animate-after 1.5s ease-in-out infinite;\n          animation: loader-animate-after 1.5s ease-in-out infinite;\n  clip: rect(0, 80px, 80px, 0px);\n  content: \"\";\n  border-radius: 50%;\n  height: 80px;\n  width: 80px;\n  position: absolute;\n}\n@-webkit-keyframes loader-animate {\n0% {\n    transform: rotate(0deg);\n}\n100% {\n    transform: rotate(220deg);\n}\n}\n@keyframes loader-animate {\n0% {\n    transform: rotate(0deg);\n}\n100% {\n    transform: rotate(220deg);\n}\n}\n@-webkit-keyframes loader-animate-after {\n0% {\n    box-shadow: inset #3498db 0 0 0 17px;\n    transform: rotate(-140deg);\n}\n50% {\n    box-shadow: inset #8e44ad 0 0 0 2px;\n}\n100% {\n    box-shadow: inset #e74c3c 0 0 0 17px;\n    transform: rotate(140deg);\n}\n}\n@keyframes loader-animate-after {\n0% {\n    box-shadow: inset #3498db 0 0 0 17px;\n    transform: rotate(-140deg);\n}\n50% {\n    box-shadow: inset #8e44ad 0 0 0 2px;\n}\n100% {\n    box-shadow: inset #e74c3c 0 0 0 17px;\n    transform: rotate(140deg);\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.top-header[data-v-d651fe58] {\r\n  background-color: var(--primary);\r\n  text-align: center;\r\n  color: var(--white);\r\n  text-transform: capitalize;\n}\n.top-header #overlay[data-v-d651fe58] {\r\n  padding: 40px 0;\r\n  position: relative;\r\n  background-color: rgba(0, 0, 0, 0.5);\n}\n.breadcrumb[data-v-d651fe58] {\r\n  justify-content: center;\n}\n.breadcrumb a[data-v-d651fe58] {\r\n  color: var(--white);\r\n  font-weight: bold;\n}\n.breadcrumb-item.active[data-v-d651fe58],\r\n.breadcrumb-item + .breadcrumb-item[data-v-d651fe58]::before {\r\n  color: var(--white);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#overlay {\r\n  position: relative;\r\n  display: block;\r\n  width: 100%;\r\n  height: 100%;\r\n  top: 0;\r\n  left: 0;\r\n  right: 0;\r\n  bottom: 0;\r\n  background-color: rgba(0, 0, 0, 0.5);\r\n  z-index: 2;\r\n  cursor: pointer;\n}\n#text {\r\n  position: absolute;\r\n  top: 60%;\r\n  left: 50%;\r\n  font-size: 50px;\r\n  color: white;\r\n  transform: translate(-50%, -50%);\r\n  -ms-transform: translate(-50%, -50%);\n}\n.line_height p {\r\n  line-height: 2;\n}\n.ul_accordion > li > ul {\r\n  display: none;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoaderComponent.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.isVisible
    ? _c("div", { staticClass: "loader-overlay" }, [
        _c("div", { staticClass: "loader" }),
        _vm._v(" "),
        _c("span", {
          staticClass: "text",
          domProps: { innerHTML: _vm._s(_vm.text) }
        })
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.page.title && _vm.page.title !== "Home"
    ? _c(
        "section",
        {
          staticClass: "top-header",
          style: {
            backgroundImage: "url('" + _vm.backgroundImage + "')"
          }
        },
        [
          _c(
            "div",
            { attrs: { id: "overlay" } },
            [
              _c("h1", [_vm._v(_vm._s(_vm.page.title))]),
              _vm._v(" "),
              _c("b-breadcrumb", { attrs: { items: _vm.items } })
            ],
            1
          )
        ]
      )
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("loader", { attrs: { "is-visible": _vm.isLoading } }),
      _vm._v(" "),
      _c(
        "div",
        {},
        [
          _c("page-header", {
            attrs: {
              page: _vm.service,
              items: [
                {
                  text: "Home",
                  href: "/"
                },
                {
                  text: "Services",
                  href: "/service"
                },
                {
                  text: _vm.service.title,
                  active: true
                }
              ]
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "container py-5" }, [
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-sm-9" }, [
                _c(
                  "div",
                  {
                    staticClass: "p-3",
                    staticStyle: { background: "#f9f9f9" }
                  },
                  [
                    _vm.service.image
                      ? _c("img", {
                          staticClass: "card-img-top p-2",
                          attrs: {
                            src: _vm.service.image,
                            alt: _vm.service.title
                          }
                        })
                      : _vm._e()
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "p-3",
                    staticStyle: { background: "#f9f9f9" }
                  },
                  [
                    _c("div", {
                      staticClass: "text-left line_height texteditor-text",
                      domProps: { innerHTML: _vm._s(_vm.service.description) }
                    })
                  ]
                ),
                _vm._v(" "),
                Object.entries(_vm.points).length > 0
                  ? _c("h2", { staticClass: "mt-4" }, [
                      _vm._v("\n            Why people choose us\n          ")
                    ])
                  : _vm._e(),
                _vm._v(" "),
                Object.entries(_vm.points).length > 0
                  ? _c(
                      "div",
                      { staticClass: "row mt-4" },
                      _vm._l(_vm.points, function(p, i) {
                        return _c(
                          "div",
                          { key: i, staticClass: "col-sm-4 mb-3" },
                          [
                            _c(
                              "div",
                              {
                                staticClass: "card h-100",
                                staticStyle: { border: "1px solid #ccc" }
                              },
                              [
                                _c("div", { staticClass: "card-header" }, [
                                  _c("h5", [_vm._v(_vm._s(p.title))])
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticStyle: { padding: "10px" } },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(p.short_description) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            )
                          ]
                        )
                      }),
                      0
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-3 sidemenu-border m-0 p-0" }, [
                Object.entries(_vm.categories).length > 0
                  ? _c(
                      "ul",
                      { staticClass: "ul_accordion p-0" },
                      _vm._l(_vm.categories, function(category, id) {
                        return _c("li", { key: id }, [
                          _c("a", { attrs: { href: "javascript:" } }, [
                            _c("i", {
                              staticClass: "icon-plus2 icon float-right"
                            }),
                            _vm._v("\n                " + _vm._s(category.name))
                          ]),
                          _vm._v(" "),
                          _c(
                            "ul",
                            { staticClass: "m-0 p-0" },
                            _vm._l(category.service, function(sr, id) {
                              return _c("li", { key: id }, [
                                _c(
                                  "a",
                                  {
                                    staticClass: "bg-dark",
                                    attrs: { href: sr.slug }
                                  },
                                  [
                                    _c("i", {
                                      staticClass: "icon-angle-right"
                                    }),
                                    _vm._v(" " + _vm._s(sr.title))
                                  ]
                                )
                              ])
                            }),
                            0
                          )
                        ])
                      }),
                      0
                    )
                  : _vm._e()
              ])
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/common/LoaderComponent.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/common/LoaderComponent.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./LoaderComponent.vue?vue&type=template&id=38e702f1& */ "./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1&");
/* harmony import */ var _LoaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LoaderComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./LoaderComponent.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _LoaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/common/LoaderComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoaderComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoaderComponent.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./LoaderComponent.vue?vue&type=template&id=38e702f1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/LoaderComponent.vue?vue&type=template&id=38e702f1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_LoaderComponent_vue_vue_type_template_id_38e702f1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&");
/* harmony import */ var _PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=script&lang=js& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d651fe58",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/components/PageHeader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=style&index=0&id=d651fe58&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_style_index_0_id_d651fe58_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&":
/*!************************************************************************************************************!*\
  !*** ./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& ***!
  \************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PageHeader.vue?vue&type=template&id=d651fe58&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/components/PageHeader.vue?vue&type=template&id=d651fe58&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PageHeader_vue_vue_type_template_id_d651fe58_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/front/service/single.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/front/service/single.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./single.vue?vue&type=template&id=55b44e53& */ "./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53&");
/* harmony import */ var _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./single.vue?vue&type=script&lang=js& */ "./resources/js/components/front/service/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./single.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__["render"],
  _single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/service/single.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/service/single.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/front/service/single.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./single.vue?vue&type=template&id=55b44e53& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/service/single.vue?vue&type=template&id=55b44e53&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_single_vue_vue_type_template_id_55b44e53___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: update_meta, baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, update_menu_parent, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, view_posts, post_sidebar, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_meta", function() { return update_meta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_parent", function() { return update_menu_parent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_posts", function() { return view_posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "post_sidebar", function() { return post_sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var update_meta = function update_meta(metaJSON) {
  if (metaJSON.title) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('title').text(metaJSON.title);
  }

  if (metaJSON.keywords) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=keywords]').text(metaJSON.keywords);
  }

  if (metaJSON.description) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=description]').text(metaJSON.description);
  }
};
var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  // prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var update_menu_parent = function update_menu_parent(data) {
  var params = {
    method: 'POST',
    url: "menu-parent",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data,
    files: true
  };
  return execute(params);
};
/**
 * Blogs
 */

var view_posts = function view_posts(query) {
  var params = {
    url: "web/post/?".concat(query),
    no_auth: true
  };
  return execute(params);
};
var post_sidebar = function post_sidebar() {
  var params = {
    url: "web/post-sidebar",
    no_auth: true
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);