(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[92],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_lazyload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-lazyload */ "./node_modules/vue-lazyload/vue-lazyload.esm.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var vue_cool_lightbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-cool-lightbox */ "./node_modules/vue-cool-lightbox/dist/vue-cool-lightbox.esm.js");
/* harmony import */ var vue_cool_lightbox_dist_vue_cool_lightbox_min_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-cool-lightbox/dist/vue-cool-lightbox.min.css */ "./node_modules/vue-cool-lightbox/dist/vue-cool-lightbox.min.css");
/* harmony import */ var vue_cool_lightbox_dist_vue_cool_lightbox_min_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_cool_lightbox_dist_vue_cool_lightbox_min_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_lazyload__WEBPACK_IMPORTED_MODULE_1__["default"]);




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    CoolLightBox: vue_cool_lightbox__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  props: ['page'],
  data: function data() {
    return {
      media: ['https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', 'https://s3-us-west-1.amazonaws.com/powr/defaults/image-slider2.jpg', {
        title: 'A beautiful mountain view',
        description: "Photo by Facundo",
        src: 'https://vimeo.com/43338103',
        thumb: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/nature-quotes-1557340276.jpg?crop=0.666xw:1.00xh;0.168xw,0&resize=640:*'
      }],
      index: null,
      services: {}
    };
  },
  mounted: function mounted() {
    this.getServices();
  },
  methods: {
    getServices: function getServices() {
      var _this = this;

      var header = {};
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_5__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_FRONT_SERVICE"], header).then(function (res) {
        _this.services = res.data.data;
        console.log(_this.services);
      });
    }
  },
  computed: {
    backgroundImage: function backgroundImage() {
      return this.page.image ? this.page.image : '';
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      {
        staticClass: "top-header",
        style: { backgroundImage: "url('" + _vm.backgroundImage + "')" }
      },
      [
        _c("div", { attrs: { id: "overlay" } }, [
          _c("div", { attrs: { id: "text" } }, [
            _c("h1", { staticClass: "text-warning" }, [
              _vm._v(_vm._s(_vm.page.title))
            ]),
            _vm._v(" "),
            _c(
              "ul",
              { staticClass: "breadcrumb", staticStyle: { border: "none" } },
              [
                _c(
                  "li",
                  [_c("router-link", { attrs: { to: "#" } }, [_vm._v("Home")])],
                  1
                ),
                _vm._v(" "),
                _c("li", [_vm._v(_vm._s(_vm.page.title))])
              ]
            )
          ])
        ])
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "suncity_services" }, [
      _c(
        "div",
        { staticClass: "container py-4" },
        [
          _c("CoolLightBox", {
            attrs: { items: _vm.media, index: _vm.index },
            on: {
              close: function($event) {
                _vm.index = null
              }
            }
          }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "row" },
            _vm._l(_vm.media, function(m, i) {
              return _c(
                "div",
                {
                  key: i,
                  staticClass: "col-md-4 mb-4 rounded",
                  on: {
                    click: function($event) {
                      _vm.index = i
                    }
                  }
                },
                [
                  _c("div", { staticClass: "card" }, [
                    _c("img", {
                      staticStyle: { "min-height": "338px" },
                      attrs: { src: m.thumb ? m.thumb : m }
                    })
                  ])
                ]
              )
            }),
            0
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/template/gallery.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/front/template/gallery.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery.vue?vue&type=template&id=1ffe246e& */ "./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e&");
/* harmony import */ var _gallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery.vue?vue&type=script&lang=js& */ "./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _gallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/template/gallery.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./gallery.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/gallery.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./gallery.vue?vue&type=template&id=1ffe246e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/template/gallery.vue?vue&type=template&id=1ffe246e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_gallery_vue_vue_type_template_id_1ffe246e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);