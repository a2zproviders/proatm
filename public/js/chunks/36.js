(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Layout.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-tinymce-editor */ "./node_modules/vue-tinymce-editor/src/index.js");
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var vue_sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-sweetalert2 */ "./node_modules/vue-sweetalert2/dist/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

 // const baseDir = "/proAtm/" //development

var baseDir = "/"; //production




vue__WEBPACK_IMPORTED_MODULE_0___default.a.component('pagination', __webpack_require__(/*! laravel-vue-pagination */ "./node_modules/laravel-vue-pagination/dist/laravel-vue-pagination.common.js"));
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_sweetalert2__WEBPACK_IMPORTED_MODULE_4__["default"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.component('tinymce', vue_tinymce_editor__WEBPACK_IMPORTED_MODULE_1__["default"]); // let AdminSidebar from ''
// let StoreSidebar from ''

/* harmony default export */ __webpack_exports__["default"] = ({
  // components: {
  //   AdminSidebar,
  //   StoreSidebar
  // },
  data: function data() {
    return {
      sidebar: 'AdminSidebar',
      user: JSON.parse(localStorage.getItem('user_info')),
      token: localStorage.getItem('token'),
      baseDir: baseDir
    };
  },
  mounted: function mounted() {// this.sidebar = `${this.user.role_info.name}Sidebar`
    // this.loadJs()
  },
  methods: {
    loadJs: function loadJs() {
      var src_urls = ['vendors/js/vendor.bundle.base.js', 'vendors/chart/Chart.min.js', 'js/off-canvas.js', 'js/hoverable-collapse.js', 'js/misc.js', 'js/dashboard.js', 'js/todolist.js'];
      var scriptTag;
      src_urls.forEach(function (row, i) {
        scriptTag = document.createElement('script');
        scriptTag.src = "http://localhost/proAtm/public/assets/".concat(row); // scriptTag.defer = true

        var jsFile = document.getElementById('mainJs');
        document.body.insertBefore(scriptTag, jsFile);
      });
    },
    logout: function logout() {
      var _this = this;

      if (confirm("Are you sure want to logout?")) {
        var header = {
          Authorization: "Bearer " + this.token
        };
        Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_3__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_2__["STORE_LOGOUT"], header).then(function (response) {
          localStorage.removeItem('admin_token');
          localStorage.removeItem('user_info');

          _this.$router.push({
            name: 'SuperAdminLogin'
          });
        })["catch"](function (error) {});
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/vendors/mdi/css/materialdesignicons.min.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/vendors/mdi/css/materialdesignicons.min.css"), "");
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/vendors/css/vendor.bundle.base.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/vendors/css/vendor.bundle.base.css"), "");
exports.i(__webpack_require__(/*! -!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../public/assets/css/style.css */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./public/assets/css/style.css"), "");

// module
exports.push([module.i, "\n/* The switch - the box around the slider */\n.switch {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 60px;\r\n  height: 34px;\n}\r\n\r\n/* Hide default HTML checkbox */\n.switch input {\r\n  opacity: 0;\r\n  width: 0;\r\n  height: 0;\n}\r\n\r\n/* The slider */\n.slider {\r\n position: absolute;\r\n cursor: pointer;\r\n top: 0;\r\n left: 0;\r\n right: 0;\r\n bottom: 0;\r\n background-color: #ccc;\r\n transition: .4s;\n}\n.slider:before {\r\n position: absolute;\r\n content: \"\";\r\n height: 26px;\r\n width: 26px;\r\n left: 4px;\r\n bottom: 4px;\r\n background-color: white;\r\n transition: .4s;\n}\ninput:checked + .slider {\r\n background-color: #2196F3;\n}\ninput:focus + .slider {\r\n box-shadow: 0 0 1px #2196F3;\n}\ninput:checked + .slider:before {\r\n transform: translateX(26px);\n}\r\n\r\n/* Rounded sliders */\n.slider.round {\r\n border-radius: 34px;\n}\n.slider.round:before {\r\n border-radius: 50%;\n}\n.sidebar .nav .nav-item.active{\r\n  background: #f2edf3 !important;\n}\r\n/* width */\n::-webkit-scrollbar {\r\n  width: 7px;\r\n  color: #3498db;\n}\r\n\r\n/* Track */\n::-webkit-scrollbar-track {\r\n  background: #f1f1f1;\n}\r\n\r\n/* Handle */\n::-webkit-scrollbar-thumb {\r\n  background: #3498db;\n}\r\n\r\n/* Handle on hover */\n::-webkit-scrollbar-thumb:hover {\r\n  background: #555;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-scroller" }, [
    _c(
      "nav",
      {
        staticClass:
          "navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row"
      },
      [
        _c(
          "div",
          {
            staticClass:
              "text-center navbar-brand-wrapper d-flex align-items-center justify-content-center"
          },
          [
            _c(
              "router-link",
              {
                staticClass: "navbar-brand brand-logo",
                attrs: { to: { name: "adminDashboard" } }
              },
              [
                _c("img", {
                  attrs: {
                    src: _vm.baseDir + "assets/images/logo.svg",
                    alt: "logo"
                  }
                })
              ]
            ),
            _vm._v(" "),
            _c(
              "router-link",
              {
                staticClass: "navbar-brand brand-logo-mini",
                attrs: { to: { name: "adminDashboard" } }
              },
              [
                _c("img", {
                  attrs: {
                    src: _vm.baseDir + "assets/images/logo-mini.svg",
                    alt: "logo"
                  }
                })
              ]
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "navbar-menu-wrapper d-flex align-items-stretch",
            staticStyle: { background: "#fff" }
          },
          [
            _vm._m(0),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("ul", { staticClass: "navbar-nav navbar-nav-right" }, [
              _c("li", { staticClass: "nav-item nav-profile dropdown" }, [
                _c(
                  "a",
                  {
                    staticClass: "nav-link dropdown-toggle",
                    attrs: {
                      id: "profileDropdown",
                      href: "#",
                      "data-toggle": "dropdown",
                      "aria-expanded": "false"
                    }
                  },
                  [
                    _c("div", { staticClass: "nav-profile-img" }, [
                      _c("img", {
                        attrs: {
                          src: _vm.baseDir + "assets/images/faces/face1.jpg",
                          alt: "image"
                        }
                      }),
                      _vm._v(" "),
                      _c("span", { staticClass: "availability-status online" })
                    ]),
                    _vm._v(" "),
                    _vm._m(2)
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "dropdown-menu navbar-dropdown",
                    attrs: { "aria-labelledby": "profileDropdown" }
                  },
                  [
                    _vm._m(3),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "dropdown-item",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.logout($event)
                          }
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "mdi mdi-logout mr-2 text-primary"
                        }),
                        _vm._v(" Signout ")
                      ]
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _vm._m(4),
              _vm._v(" "),
              _c("li", { staticClass: "nav-item dropdown" }, [
                _vm._m(5),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass:
                      "dropdown-menu dropdown-menu-right navbar-dropdown preview-list",
                    attrs: { "aria-labelledby": "messageDropdown" }
                  },
                  [
                    _c("h6", { staticClass: "p-3 mb-0" }, [_vm._v("Messages")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _vm._m(6),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _c("a", { staticClass: "dropdown-item preview-item" }, [
                      _c("div", { staticClass: "preview-thumbnail" }, [
                        _c("img", {
                          staticClass: "profile-pic",
                          attrs: {
                            src: _vm.baseDir + "assets/images/faces/face2.jpg",
                            alt: "image"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm._m(7)
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _c("a", { staticClass: "dropdown-item preview-item" }, [
                      _c("div", { staticClass: "preview-thumbnail" }, [
                        _c("img", {
                          staticClass: "profile-pic",
                          attrs: {
                            src: _vm.baseDir + "assets/images/faces/face3.jpg",
                            alt: "image"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _vm._m(8)
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-divider" }),
                    _vm._v(" "),
                    _c("h6", { staticClass: "p-3 mb-0 text-center" }, [
                      _vm._v("4 new messages")
                    ])
                  ]
                )
              ]),
              _vm._v(" "),
              _vm._m(9),
              _vm._v(" "),
              _vm._m(10),
              _vm._v(" "),
              _vm._m(11)
            ]),
            _vm._v(" "),
            _vm._m(12)
          ]
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "container-fluid page-body-wrapper" }, [
      _c(
        "nav",
        { staticClass: "sidebar sidebar-offcanvas", attrs: { id: "sidebar" } },
        [
          _c("ul", { staticClass: "nav" }, [
            _vm._m(13),
            _vm._v(" "),
            _c(
              "li",
              { staticClass: "nav-item" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "nav-link",
                    attrs: { to: { name: "adminDashboard" } }
                  },
                  [
                    _c("span", { staticClass: "menu-title" }, [
                      _vm._v("Dashboard")
                    ]),
                    _vm._v(" "),
                    _c("i", { staticClass: "mdi mdi-home menu-icon" })
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("li", { staticClass: "nav-item" }, [
              _vm._m(14),
              _vm._v(" "),
              _c("div", { staticClass: "collapse", attrs: { id: "Store" } }, [
                _c("ul", { staticClass: "nav flex-column sub-menu" }, [
                  _c(
                    "li",
                    { staticClass: "nav-item" },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "nav-link",
                          attrs: {
                            to: {
                              name: "AddStore"
                            }
                          }
                        },
                        [_vm._v("Create Store")]
                      )
                    ],
                    1
                  )
                ])
              ])
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "main-panel" },
        [_c("router-view"), _vm._v(" "), _vm._m(15)],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler navbar-toggler align-self-center",
        attrs: { type: "button", "data-toggle": "minimize" }
      },
      [_c("span", { staticClass: "mdi mdi-menu" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "search-field d-none d-md-block" }, [
      _c(
        "form",
        {
          staticClass: "d-flex align-items-center h-100",
          attrs: { action: "#" }
        },
        [
          _c("div", { staticClass: "input-group" }, [
            _c("div", { staticClass: "input-group-prepend bg-transparent" }, [
              _c("i", {
                staticClass: "input-group-text border-0 mdi mdi-magnify"
              })
            ]),
            _vm._v(" "),
            _c("input", {
              staticClass: "form-control bg-transparent border-0",
              attrs: { type: "text", placeholder: "Search projects" }
            })
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "nav-profile-text" }, [
      _c("p", { staticClass: "mb-1 text-black" }, [_vm._v("David Greymaax")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
      _c("i", { staticClass: "mdi mdi-cached mr-2 text-success" }),
      _vm._v(" Activity Log ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "li",
      { staticClass: "nav-item d-none d-lg-block full-screen-link" },
      [
        _c("a", { staticClass: "nav-link" }, [
          _c("i", {
            staticClass: "mdi mdi-fullscreen",
            attrs: { id: "fullscreen-button" }
          })
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "nav-link count-indicator dropdown-toggle",
        attrs: {
          id: "messageDropdown",
          href: "#",
          "data-toggle": "dropdown",
          "aria-expanded": "false"
        }
      },
      [
        _c("i", { staticClass: "mdi mdi-email-outline" }),
        _vm._v(" "),
        _c("span", { staticClass: "count-symbol bg-warning" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { staticClass: "dropdown-item preview-item" }, [
      _c("div", { staticClass: "preview-thumbnail" }, [
        _c("img", {
          staticClass: "profile-pic",
          attrs: { src: "assets/images/faces/face4.jpg", alt: "image" }
        })
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "preview-item-content d-flex align-items-start flex-column justify-content-center"
        },
        [
          _c(
            "h6",
            { staticClass: "preview-subject ellipsis mb-1 font-weight-normal" },
            [_vm._v("Mark send you a message")]
          ),
          _vm._v(" "),
          _c("p", { staticClass: "text-gray mb-0" }, [
            _vm._v(" 1 Minutes ago ")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "preview-item-content d-flex align-items-start flex-column justify-content-center"
      },
      [
        _c(
          "h6",
          { staticClass: "preview-subject ellipsis mb-1 font-weight-normal" },
          [_vm._v("Cregh send you a message")]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "text-gray mb-0" }, [_vm._v(" 15 Minutes ago ")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "preview-item-content d-flex align-items-start flex-column justify-content-center"
      },
      [
        _c(
          "h6",
          { staticClass: "preview-subject ellipsis mb-1 font-weight-normal" },
          [_vm._v("Profile picture updated")]
        ),
        _vm._v(" "),
        _c("p", { staticClass: "text-gray mb-0" }, [_vm._v(" 18 Minutes ago ")])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item dropdown" }, [
      _c(
        "a",
        {
          staticClass: "nav-link count-indicator dropdown-toggle",
          attrs: {
            id: "notificationDropdown",
            href: "#",
            "data-toggle": "dropdown"
          }
        },
        [
          _c("i", { staticClass: "mdi mdi-bell-outline" }),
          _vm._v(" "),
          _c("span", { staticClass: "count-symbol bg-danger" })
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass:
            "dropdown-menu dropdown-menu-right navbar-dropdown preview-list",
          attrs: { "aria-labelledby": "notificationDropdown" }
        },
        [
          _c("h6", { staticClass: "p-3 mb-0" }, [_vm._v("Notifications")]),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-divider" }),
          _vm._v(" "),
          _c("a", { staticClass: "dropdown-item preview-item" }, [
            _c("div", { staticClass: "preview-thumbnail" }, [
              _c("div", { staticClass: "preview-icon bg-success" }, [
                _c("i", { staticClass: "mdi mdi-calendar" })
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "preview-item-content d-flex align-items-start flex-column justify-content-center"
              },
              [
                _c(
                  "h6",
                  { staticClass: "preview-subject font-weight-normal mb-1" },
                  [_vm._v("Event today")]
                ),
                _vm._v(" "),
                _c("p", { staticClass: "text-gray ellipsis mb-0" }, [
                  _vm._v(" Just a reminder that you have an event today ")
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-divider" }),
          _vm._v(" "),
          _c("a", { staticClass: "dropdown-item preview-item" }, [
            _c("div", { staticClass: "preview-thumbnail" }, [
              _c("div", { staticClass: "preview-icon bg-warning" }, [
                _c("i", { staticClass: "mdi mdi-settings" })
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "preview-item-content d-flex align-items-start flex-column justify-content-center"
              },
              [
                _c(
                  "h6",
                  { staticClass: "preview-subject font-weight-normal mb-1" },
                  [_vm._v("Settings")]
                ),
                _vm._v(" "),
                _c("p", { staticClass: "text-gray ellipsis mb-0" }, [
                  _vm._v(" Update dashboard ")
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-divider" }),
          _vm._v(" "),
          _c("a", { staticClass: "dropdown-item preview-item" }, [
            _c("div", { staticClass: "preview-thumbnail" }, [
              _c("div", { staticClass: "preview-icon bg-info" }, [
                _c("i", { staticClass: "mdi mdi-link-variant" })
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass:
                  "preview-item-content d-flex align-items-start flex-column justify-content-center"
              },
              [
                _c(
                  "h6",
                  { staticClass: "preview-subject font-weight-normal mb-1" },
                  [_vm._v("Launch Admin")]
                ),
                _vm._v(" "),
                _c("p", { staticClass: "text-gray ellipsis mb-0" }, [
                  _vm._v(" New admin wow! ")
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-divider" }),
          _vm._v(" "),
          _c("h6", { staticClass: "p-3 mb-0 text-center" }, [
            _vm._v("See all notifications")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item nav-logout d-none d-lg-block" }, [
      _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
        _c("i", { staticClass: "mdi mdi-power" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "li",
      { staticClass: "nav-item nav-settings d-none d-lg-block" },
      [
        _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
          _c("i", { staticClass: "mdi mdi-format-line-spacing" })
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass:
          "navbar-toggler navbar-toggler-right d-lg-none align-self-center",
        attrs: { type: "button", "data-toggle": "offcanvas" }
      },
      [_c("span", { staticClass: "mdi mdi-menu" })]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "nav-item nav-profile" }, [
      _c("a", { staticClass: "nav-link", attrs: { href: "#" } }, [
        _c("div", { staticClass: "nav-profile-image" }, [
          _c("img", {
            attrs: {
              src: "/proAtm/assets/images/faces/face1.jpg",
              alt: "profile"
            }
          }),
          _vm._v(" "),
          _c("span", { staticClass: "login-status online" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "nav-profile-text d-flex flex-column" }, [
          _c("span", { staticClass: "font-weight-bold mb-2" }, [
            _vm._v("David Grey. H")
          ]),
          _vm._v(" "),
          _c("span", { staticClass: "text-secondary text-small" }, [
            _vm._v("Project Manager")
          ])
        ]),
        _vm._v(" "),
        _c("i", {
          staticClass: "mdi mdi-bookmark-check text-success nav-profile-badge"
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      {
        staticClass: "nav-link",
        attrs: {
          "data-toggle": "collapse",
          href: "#Store",
          "aria-expanded": "false",
          "aria-controls": "general-pages"
        }
      },
      [
        _c("span", { staticClass: "menu-title" }, [_vm._v("Store")]),
        _vm._v(" "),
        _c("i", { staticClass: "menu-arrow" }),
        _vm._v(" "),
        _c("i", { staticClass: "mdi mdi-store menu-icon" })
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "footer" }, [
      _c(
        "div",
        {
          staticClass:
            "d-sm-flex justify-content-center justify-content-sm-between"
        },
        [
          _c(
            "span",
            {
              staticClass:
                "text-muted text-center text-sm-left d-block d-sm-inline-block"
            },
            [
              _vm._v("Copyright © 2017 "),
              _c("a", { attrs: { href: "#", target: "_blank" } }, [
                _vm._v("ProATM")
              ]),
              _vm._v(". All rights reserved.")
            ]
          ),
          _vm._v(" "),
          _c(
            "span",
            {
              staticClass:
                "float-none float-sm-right d-block mt-1 mt-sm-0 text-center"
            },
            [
              _vm._v("Hand-crafted & made with "),
              _c("i", { staticClass: "mdi mdi-heart text-danger" })
            ]
          )
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/admin/Layout.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/admin/Layout.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=5daf58d5& */ "./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/components/admin/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Layout.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/admin/Layout.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/admin/Layout.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/admin/Layout.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Layout.vue?vue&type=template&id=5daf58d5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/admin/Layout.vue?vue&type=template&id=5daf58d5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_5daf58d5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);