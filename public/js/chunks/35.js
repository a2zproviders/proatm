(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _public_img_preview_bg_jpg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../public/img/preview-bg.jpg */ "./public/img/preview-bg.jpg");
/* harmony import */ var _public_img_preview_bg_jpg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_public_img_preview_bg_jpg__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    console.log("this is running.", _public_img_preview_bg_jpg__WEBPACK_IMPORTED_MODULE_2___default.a);
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("body").css("background", "#000 url(" + _public_img_preview_bg_jpg__WEBPACK_IMPORTED_MODULE_2___default.a + ") 0 0 repeat");
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview-test-desktop").click(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(".preview-test").removeClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).addClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").removeClass();
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").addClass("preview-desktop");
    });
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview-test-tablet").click(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(".preview-test").removeClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).addClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").removeClass();
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").addClass("preview-tablet");
    });
    jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview-test-mobile").click(function () {
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(".preview-test").removeClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).addClass("active");
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").removeClass();
      jquery__WEBPACK_IMPORTED_MODULE_1___default()("#preview").children("iframe").addClass("preview-mobile");
    });
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n*,\r\n::after,\r\n::before {\r\n  box-sizing: border-box;\n}\nbody,\r\nhtml {\r\n  height: 100%;\n}\nbody {\r\n  margin: 0;\r\n  overflow: hidden;\r\n  position: relative;\r\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Helvetica,\r\n    Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";\n}\nnav.navbar {\r\n  display: none !important;\n}\n#sidebar {\r\n  display: none !important;\n}\n.main-panel footer {\r\n  display: none !important;\n}\n#header {\r\n  background: #fff;\r\n  height: 65px;\r\n  overflow: hiddent;\r\n  border-bottom: 1px solid #ddd;\r\n  position: absolute;\r\n  left: 0;\r\n  right: 0;\r\n  top: 0;\r\n  display: flex;\r\n  align-items: center;\r\n  padding: 0 10px;\n}\n#header .logo img {\r\n  height: 30px;\n}\n#header .logo a {\r\n  color: #1e5f8d;\r\n  display: flex;\r\n  align-items: center;\r\n  text-decoration: none;\r\n  font-size: 24px;\r\n  letter-spacing: -0.8px;\r\n  font-weight: 500;\n}\n#header .logo strong {\r\n  margin-left: 6px;\r\n  text-transform: uppercase;\n}\n#header .logo strong span {\r\n  color: #00a6eb;\n}\n.icon {\r\n  width: 24px;\r\n  height: 24px;\n}\n#preview {\r\n  position: absolute;\r\n  left: 0;\r\n  right: 0;\r\n  top: 65px;\r\n  bottom: 0;\r\n  transition: all 0.2s;\n}\n#preview-frame {\r\n  border: 0;\r\n  position: absolute;\r\n  transition: 0.5s;\n}\n.preview-desktop {\r\n  left: 0;\r\n  width: 100%;\r\n  height: 100%;\n}\n.preview-tablet {\r\n  width: 768px;\r\n  height: 100%;\r\n  left: calc(50% - 384px);\n}\n.preview-mobile {\r\n  width: 380px;\r\n  height: 680px;\r\n  left: calc(50% - 190px);\r\n  top: 0;\r\n  margin-top: 20px;\n}\n.preview-devices ul {\r\n  margin: 0 0 0 20px;\r\n  padding: 0;\r\n  list-style: none;\r\n  list-style-type: none;\r\n  display: flex;\r\n  align-items: center;\n}\n.preview-devices a {\r\n  transition: 0.3s;\r\n  border-bottom: 2px solid #fff;\r\n  color: #1e5f8d;\r\n  display: inline-block;\r\n  padding: 5px 10px;\r\n  margin: 0 5px;\n}\n.preview-devices a:hover {\r\n  color: #00a6eb;\n}\n.preview-devices .active a {\r\n  border-bottom: 2px solid #00a6eb;\r\n  color: #00a6eb;\n}\n.navigate {\r\n  margin-left: auto;\n}\n.navigate .icon-chevron-left,\r\n.navigate .icon-chevron-right {\r\n  width: 36px;\r\n  height: 36px;\n}\n.navigate ul {\r\n  padding: 0;\r\n  margin: 0;\r\n  list-style: none;\r\n  display: flex;\r\n  align-items: center;\n}\n.navigate li {\r\n  margin: 0 0 0 5px;\n}\n.navigate li:last-child {\r\n  margin-right: 0;\n}\n.navigate a {\r\n  transition: 0.3s;\r\n  padding: 0 10px;\r\n  border: solid 1px #dfdfdf;\r\n  border-radius: 4px;\r\n  color: #828282;\r\n  display: flex;\r\n  align-items: center;\r\n  height: 40px;\r\n  justify-content: center;\r\n  text-decoration: none;\n}\n.navigate a:hover {\r\n  border: solid 1px #00a6eb;\r\n  color: #00a6eb;\n}\n.navigate a.download {\r\n  background: #00a6eb;\r\n  border: solid 1px #00a6eb;\r\n  color: #fff;\r\n  font-size: 15px;\r\n  padding: 0 16px;\n}\n.navigate a.download span {\r\n  padding-left: 4px;\n}\n.navigate a.download:hover {\r\n  background: #00b4ff;\r\n  border-color: #00b4ff;\n}\n.navigate a.first-latest {\r\n  background: #e6e6e6;\r\n  border: 1px solid #e6e6e6;\r\n  color: #fff;\n}\n@media (max-width: 1024px) {\n.preview-devices {\r\n    display: none;\n}\n}\n@media (max-width: 768px) {\n.logo {\r\n    display: none;\n}\n.navigate {\r\n    margin: 0;\r\n    width: calc(100% - 4px);\n}\n.navigate ul {\r\n    justify-content: center;\n}\n.navigate li {\r\n    flex-grow: 1;\r\n    flex-basis: 0;\n}\n.navigate a.download span {\r\n    display: none;\n}\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("header", { attrs: { id: "header" } }, [
        _c("div", { staticClass: "logo" }, [
          _c("a", { attrs: { href: "/plateform-xpanel", rel: "home" } }, [
            _c("strong", [_vm._v("Pro"), _c("span", [_vm._v("Atm")])])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "preview-devices" }, [
          _c("ul", [
            _c(
              "li",
              {
                staticClass: "preview-test active",
                attrs: { id: "preview-test-desktop" }
              },
              [
                _c("a", { attrs: { href: "#" } }, [
                  _c("i", {
                    staticClass: "mdi mdi-desktop-mac",
                    staticStyle: { "font-size": "25px" }
                  })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass: "preview-test",
                attrs: { id: "preview-test-tablet" }
              },
              [
                _c("a", { attrs: { href: "#" } }, [
                  _c("i", {
                    staticClass: "mdi mdi-tablet",
                    staticStyle: { "font-size": "25px" }
                  })
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass: "preview-test",
                attrs: { id: "preview-test-mobile" }
              },
              [
                _c("a", { attrs: { href: "#" } }, [
                  _c("i", {
                    staticClass: "mdi mdi-cellphone",
                    staticStyle: { "font-size": "25px" }
                  })
                ])
              ]
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "navigate" }, [
          _c("ul", [
            _c("li", [
              _c("a", { attrs: { href: "/plateform-xpanel", title: "" } }, [
                _c("i", {
                  staticClass: "mdi mdi-home",
                  staticStyle: { "font-size": "25px" }
                })
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { attrs: { id: "preview" } }, [
        _c("iframe", {
          staticClass: "preview-desktop",
          attrs: {
            id: "preview-frame",
            src: "http://localhost:8000/",
            frameborder: "0"
          }
        })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./public/img/preview-bg.jpg":
/*!***********************************!*\
  !*** ./public/img/preview-bg.jpg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/preview-bg.jpg?64885dc65a8d957887bc3dd726e7287d";

/***/ }),

/***/ "./resources/js/components/backend/preview.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/backend/preview.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preview.vue?vue&type=template&id=44c7ad5c& */ "./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&");
/* harmony import */ var _preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preview.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/preview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./preview.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/preview.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./preview.vue?vue&type=template&id=44c7ad5c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/preview.vue?vue&type=template&id=44c7ad5c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_preview_vue_vue_type_template_id_44c7ad5c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);