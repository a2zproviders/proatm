(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[131],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'HoempageSetting',
  data: function data() {
    return {
      formdata: {
        testimonial: '',
        name: '',
        company: '',
        website: '',
        logo: ''
      },
      token: localStorage.getItem('token')
    };
  },
  mounted: function mounted() {
    this.getInfo();
  },
  methods: {
    getInfo: function getInfo() {
      var _this = this;

      var id = this.$route.params.Tid; // console.log(id)

      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["GET"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_TESTIMONIAL"] + '/' + id, header).then(function (res) {
        _this.formdata = res.data;
        console.log(res.data);
      });
    },
    storePage: function storePage() {
      var _this2 = this;

      var id = this.$route.params.Pid;
      var header = {
        Authorization: "Bearer " + this.token
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["PUT"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_TESTIMONIAL"] + '/' + id, {
        record: this.formdata
      }, header).then(function (res) {
        _this2.$swal('Sucecess', 'Record updated successfully', 'Ok').then(function (result) {
          if (result.value) {
            _this2.$router.push({
              name: 'ViewPage'
            });
          }
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [_vm._v("General Settings ")]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "Settings" } } }, [
                _vm._v("Settings")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [_vm._v("General Settings")]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.storePage($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "testimonial" } }, [
                    _vm._v("Settings")
                  ]),
                  _vm._v(" "),
                  _c("textarea", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.formdata.testimonial,
                        expression: "formdata.testimonial"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      id: "testimonial",
                      rows: "4",
                      placeholder: "Enter testimonial"
                    },
                    domProps: { value: _vm.formdata.testimonial },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.formdata,
                          "testimonial",
                          $event.target.value
                        )
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "name" } }, [
                    _vm._v("Client Name")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.formdata.name,
                        expression: "formdata.name"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "name",
                      placeholder: "Client name"
                    },
                    domProps: { value: _vm.formdata.name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.formdata, "name", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "company" } }, [
                    _vm._v("Company name")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.formdata.company,
                        expression: "formdata.company"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "company",
                      placeholder: "Company name"
                    },
                    domProps: { value: _vm.formdata.company },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.formdata, "company", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "website" } }, [
                    _vm._v("Website url")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.formdata.website,
                        expression: "formdata.website"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "website",
                      placeholder: "Website url"
                    },
                    domProps: { value: _vm.formdata.website },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.formdata, "website", $event.target.value)
                      }
                    }
                  })
                ]),
                _vm._v(" "),
                _vm._m(0),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-gradient-primary mr-2",
                    attrs: { type: "submit" }
                  },
                  [_vm._v("Submit")]
                ),
                _vm._v(" "),
                _c("button", { staticClass: "btn btn-light" }, [
                  _vm._v("Cancel")
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", [_vm._v("Logo upload")]),
      _vm._v(" "),
      _c("input", {
        staticClass: "file-upload-default",
        attrs: { type: "file", name: "logo[]" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "input-group col-xs-12" }, [
        _c("input", {
          staticClass: "form-control file-upload-info",
          attrs: { type: "text", disabled: "", placeholder: "Upload Image" }
        }),
        _vm._v(" "),
        _c("span", { staticClass: "input-group-append" }, [
          _c(
            "button",
            {
              staticClass: "file-upload-browse btn btn-gradient-primary",
              attrs: { type: "button" }
            },
            [_vm._v("Upload")]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/setting/HomePageSetting.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/components/backend/setting/HomePageSetting.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomePageSetting.vue?vue&type=template&id=5d5f848d& */ "./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d&");
/* harmony import */ var _HomePageSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomePageSetting.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HomePageSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/setting/HomePageSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomePageSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d& ***!
  \****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomePageSetting.vue?vue&type=template&id=5d5f848d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/setting/HomePageSetting.vue?vue&type=template&id=5d5f848d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomePageSetting_vue_vue_type_template_id_5d5f848d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);