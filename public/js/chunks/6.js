(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_picture_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-picture-input */ "./node_modules/vue-picture-input/PictureInput.vue");
/* harmony import */ var _common_UploadImage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/UploadImage */ "./resources/js/components/common/UploadImage.vue");
/* harmony import */ var _services_api_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/api.js */ "./resources/js/services/api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PagesComponent",
  components: {
    PictureInput: vue_picture_input__WEBPACK_IMPORTED_MODULE_1__["default"],
    UploadImage: _common_UploadImage__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      formdata: {
        type: "custom",
        title: "",
        description: "",
        image: ""
      },
      service: {},
      page: {},
      services: {},
      // galleries: {},
      pages: {},
      setClass: ""
    };
  },
  validations: {
    formdata: {
      title: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      slug: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["required"]
      },
      description: {
        maxLength: Object(vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_0__["maxLength"])(300)
      }
    }
  },
  mounted: function mounted() {
    this.applymounted();
  },
  methods: {
    applymounted: function applymounted() {
      if (this.$route.params.id) {
        this.getInfo();
      }
    },
    getInfo: function getInfo() {
      var _this = this;

      Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["show_slider"])(this.$route.params.id).then(function (res) {
        _this.formdata = res.data;
      });
    },
    submitForm: function submitForm() {
      var _this2 = this;

      var params = {
        slider: this.formdata
      };
      var apiResponse;

      if (this.$route.params.id) {
        apiResponse = Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["update_slider"])(this.$route.params.id, params);
      } else {
        apiResponse = Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["add_slider"])(params);
      }

      apiResponse.then(function (res) {
        _this2.$swal("Done!", res.data.message, "success").then(function () {
          _this2.$router.push({
            name: "ViewSlides"
          });
        });
      });
    },
    onRemoved: function onRemoved() {
      this.formdata.image = "";
    }
  },
  watch: {
    "formdata.type": function formdataType(type) {
      var _this3 = this;

      this.formdata.service_id = "";
      this.formdata.page_id = "";
      this.services = this.pages = {};

      if (type === "service") {
        Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["view_services"])("type=all").then(function (res) {
          _this3.services = res.data;
        });
      }

      if (type === "page") {
        Object(_services_api_js__WEBPACK_IMPORTED_MODULE_3__["view_pages"])("type=all&response_type=array").then(function (res) {
          _this3.pages = res.data;
        });
      }
    },
    service: function service(_service) {
      this.formdata.service_id = _service.id;
      this.formdata.title = _service.title;
      this.formdata.description = _service.short_description;
      this.formdata.image = _service.image_url.full;
    },
    page: function page(_page) {
      this.formdata.page_id = _page.id;
      this.formdata.title = _page.title;
      this.formdata.description = _page.short_description;
      this.formdata.image = _page.image_url.full;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-dropzone */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "./node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _services_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/api */ "./resources/js/services/api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var token = localStorage.getItem("token");
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    image: String,
    label: {
      type: String,
      "default": "Upload Image"
    }
  },
  data: function data() {
    return {
      file: null,
      dropzoneOptions: {
        url: "".concat(_services_api__WEBPACK_IMPORTED_MODULE_2__["baseURL"], "media"),
        thumbnailWidth: 150,
        maxFilesize: 0.5,
        headers: {
          Authorization: "Bearer ".concat(token)
        }
      },
      images: {}
    };
  },
  components: {
    vueDropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  mounted: function mounted() {
    var _this = this;

    Object(_services_api__WEBPACK_IMPORTED_MODULE_2__["view_media_images"])().then(function (res) {
      _this.images = res;
    });
  },
  methods: {
    uploadDone: function uploadDone(file, response) {
      this.file = response.data.image_url;
      this.images.data.push(response.data);
    }
  },
  watch: {
    file: function file(image_url) {
      this.$emit("uploaded", image_url);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ninput[type=\"radio\"][data-v-11f96c21] {\r\n  display: none;\n}\n.media-image[data-v-11f96c21] {\r\n  width: 100px;\r\n  height: 100px;\r\n  -o-object-fit: cover;\r\n     object-fit: cover;\r\n  border: 2px solid;\r\n  cursor: pointer;\n}\ninput[type=\"radio\"]:checked + .media-image[data-v-11f96c21] {\r\n  border-color: var(--primary);\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.modal-content {\r\n  background-color: #fff !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=style&index=1&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "content-wrapper" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h3", { staticClass: "page-title" }, [
        _vm._v(_vm._s(_vm.$route.params.id ? "Edit" : "Add") + " Slide")
      ]),
      _vm._v(" "),
      _c("nav", { attrs: { "aria-label": "breadcrumb" } }, [
        _c("ol", { staticClass: "breadcrumb" }, [
          _c(
            "li",
            { staticClass: "breadcrumb-item" },
            [
              _c("router-link", { attrs: { to: { name: "ViewSlides" } } }, [
                _vm._v("Slide")
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              staticClass: "breadcrumb-item active",
              attrs: { "aria-current": "page" }
            },
            [
              _vm._v(
                "\n          " +
                  _vm._s(_vm.$route.params.id ? "Edit" : "Add") +
                  " Slide\n        "
              )
            ]
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 grid-margin stretch-card" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                staticClass: "forms-sample",
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.submitForm($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c(
                    "div",
                    { staticClass: "col-sm-9" },
                    [
                      _c(
                        "b-form-group",
                        { attrs: { label: "Source" } },
                        [
                          _c("b-form-radio-group", {
                            attrs: {
                              options: [
                                {
                                  text: "Service",
                                  value: "service"
                                },
                                /*
                      {
                        text: 'Gallery',
                        value: 'gallery',
                      },
                      */
                                {
                                  text: "Page",
                                  value: "page"
                                },
                                {
                                  text: "Custom Slides",
                                  value: "custom"
                                }
                              ]
                            },
                            model: {
                              value: _vm.formdata.type,
                              callback: function($$v) {
                                _vm.$set(_vm.formdata, "type", $$v)
                              },
                              expression: "formdata.type"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.formdata.type == "service"
                        ? _c("b-form-group", { attrs: { label: "Service" } }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.service,
                                    expression: "service"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.service = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("Select Service")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.services, function(s, i) {
                                  return _c(
                                    "option",
                                    { key: i, domProps: { value: s } },
                                    [
                                      _vm._v(
                                        "\n                      " +
                                          _vm._s(s.title) +
                                          "\n                    "
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.formdata.type == "page"
                        ? _c("b-form-group", { attrs: { label: "Page" } }, [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.page,
                                    expression: "page"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.page = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("Select Page")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.pages, function(p, i) {
                                  return _c(
                                    "option",
                                    { key: i, domProps: { value: p } },
                                    [
                                      _vm._v(
                                        "\n                      " +
                                          _vm._s(p.title) +
                                          "\n                    "
                                      )
                                    ]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "page_title" } }, [
                          _vm._v("Title")
                        ]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model.trim",
                              value: _vm.$v.formdata.title.$model,
                              expression: "$v.formdata.title.$model",
                              modifiers: { trim: true }
                            }
                          ],
                          staticClass: "form-control",
                          class: { "is-invalid": _vm.$v.formdata.title.$error },
                          attrs: {
                            type: "text",
                            id: "page_title",
                            placeholder: "Slide title"
                          },
                          domProps: { value: _vm.$v.formdata.title.$model },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.$v.formdata.title,
                                "$model",
                                $event.target.value.trim()
                              )
                            },
                            blur: function($event) {
                              return _vm.$forceUpdate()
                            }
                          }
                        }),
                        _vm._v(" "),
                        !_vm.$v.formdata.title.required
                          ? _c("div", { staticClass: "invalid-feedback" }, [
                              _vm._v(
                                "\n                    Field is required\n                  "
                              )
                            ])
                          : _vm._e()
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "form-group" }, [
                        _c("label", { attrs: { for: "description" } }, [
                          _vm._v("Description")
                        ]),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.$v.formdata.description.$model,
                              expression: "$v.formdata.description.$model"
                            }
                          ],
                          staticClass: "form-control",
                          class: {
                            "is-invalid": _vm.$v.formdata.description.$error
                          },
                          attrs: {
                            id: "description",
                            rows: "15",
                            placeholder: "Description"
                          },
                          domProps: {
                            value: _vm.$v.formdata.description.$model
                          },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                _vm.$v.formdata.description,
                                "$model",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        !_vm.$v.formdata.description.maxLength
                          ? _c("div", { staticClass: "invalid-feedback" }, [
                              _vm._v(
                                "\n                    Short description must not have more than\n                    " +
                                  _vm._s(
                                    _vm.$v.formdata.description.$params
                                      .maxLength.max
                                  ) +
                                  "\n                    letters.\n                  "
                              )
                            ])
                          : _vm._e()
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-sm-3" },
                    [
                      _c(
                        "button",
                        {
                          staticClass:
                            "btn btn-block btn-gradient-primary mb-3",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("\n                  Save\n                ")]
                      ),
                      _vm._v(" "),
                      _c("upload-image", {
                        attrs: {
                          image: _vm.formdata.image,
                          label: "Upload Image"
                        },
                        on: {
                          uploaded: function(img) {
                            return (_vm.formdata.image = img)
                          }
                        }
                      })
                    ],
                    1
                  )
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.image
        ? _c("div", { staticClass: "mb-3" }, [
            _c("img", {
              staticClass: "w-100",
              attrs: { src: _vm.image, alt: "Upload Image" }
            })
          ])
        : _vm._e(),
      _vm._v(" "),
      _c(
        "b-button",
        {
          directives: [
            {
              name: "b-modal",
              rawName: "v-b-modal.upload-image",
              modifiers: { "upload-image": true }
            }
          ],
          attrs: { block: "" }
        },
        [_vm._v(_vm._s(_vm.label))]
      ),
      _vm._v(" "),
      _c(
        "b-modal",
        {
          attrs: {
            id: "upload-image",
            size: "xl",
            "hide-header": "",
            "ok-only": "",
            centered: ""
          }
        },
        [
          _c(
            "b-tabs",
            { attrs: { "content-class": "mt-3" } },
            [
              _c("b-tab", { attrs: { title: "Choose Image", active: "" } }, [
                _vm.images.data
                  ? _c(
                      "div",
                      _vm._l(_vm.images.data, function(img, i) {
                        return _c("label", { key: i }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.file,
                                expression: "file"
                              }
                            ],
                            attrs: { type: "radio" },
                            domProps: {
                              value: img.image_url,
                              checked: _vm._q(_vm.file, img.image_url)
                            },
                            on: {
                              change: function($event) {
                                _vm.file = img.image_url
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("img", {
                            staticClass: "media-image",
                            attrs: { src: img.image_url }
                          })
                        ])
                      }),
                      0
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c(
                "b-tab",
                { attrs: { title: "Upload Image" } },
                [
                  _c(
                    "vue-dropzone",
                    {
                      ref: "myVueDropzone",
                      attrs: {
                        id: "dropzone",
                        options: _vm.dropzoneOptions,
                        useCustomSlot: true
                      },
                      on: { "vdropzone-success": _vm.uploadDone }
                    },
                    [
                      _c("div", { staticClass: "dropzone-custom-content" }, [
                        _c("h3", { staticClass: "dropzone-custom-title" }, [
                          _vm._v(
                            "\n              Drag and drop to upload content!\n            "
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "subtitle" }, [
                          _vm._v(
                            "\n              ...or click to select a file from your computer\n            "
                          )
                        ])
                      ])
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/slider/Add.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/backend/slider/Add.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Add.vue?vue&type=template&id=013d5b39& */ "./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39&");
/* harmony import */ var _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Add.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/slider/Add.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/Add.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Add.vue?vue&type=template&id=013d5b39& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/slider/Add.vue?vue&type=template&id=013d5b39&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Add_vue_vue_type_template_id_013d5b39___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/common/UploadImage.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/common/UploadImage.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UploadImage.vue?vue&type=template&id=11f96c21&scoped=true& */ "./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true&");
/* harmony import */ var _UploadImage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UploadImage.vue?vue&type=script&lang=js& */ "./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& */ "./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&");
/* harmony import */ var _UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./UploadImage.vue?vue&type=style&index=1&lang=css& */ "./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");







/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_4__["default"])(
  _UploadImage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "11f96c21",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/common/UploadImage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=0&id=11f96c21&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_0_id_11f96c21_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=style&index=1&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=style&index=1&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_style_index_1_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UploadImage.vue?vue&type=template&id=11f96c21&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/common/UploadImage.vue?vue&type=template&id=11f96c21&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UploadImage_vue_vue_type_template_id_11f96c21_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/services/api.js":
/*!**************************************!*\
  !*** ./resources/js/services/api.js ***!
  \**************************************/
/*! exports provided: update_meta, baseURL, admin_login, get_busi_categories, view_store, add_store, edit_store, delete_store, show_store, add_menu, view_menu, update_menu_order, update_menu_parent, delete_menu_order, view_all_pages, show_template_info, view_services, view_pages, view_media_images, view_slider, add_slider, show_slider, update_slider, delete_slider, bulk_action_slider, update_profile, update_store_profile, view_posts, post_sidebar, send_enquiry */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_meta", function() { return update_meta; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseURL", function() { return baseURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "admin_login", function() { return admin_login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get_busi_categories", function() { return get_busi_categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_store", function() { return view_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_store", function() { return add_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "edit_store", function() { return edit_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_store", function() { return delete_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_store", function() { return show_store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_menu", function() { return add_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_menu", function() { return view_menu; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_order", function() { return update_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_menu_parent", function() { return update_menu_parent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_menu_order", function() { return delete_menu_order; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_all_pages", function() { return view_all_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_template_info", function() { return show_template_info; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_services", function() { return view_services; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_pages", function() { return view_pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_media_images", function() { return view_media_images; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_slider", function() { return view_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "add_slider", function() { return add_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "show_slider", function() { return show_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_slider", function() { return update_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "delete_slider", function() { return delete_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bulk_action_slider", function() { return bulk_action_slider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_profile", function() { return update_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "update_store_profile", function() { return update_store_profile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "view_posts", function() { return view_posts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "post_sidebar", function() { return post_sidebar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "send_enquiry", function() { return send_enquiry; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var update_meta = function update_meta(metaJSON) {
  if (metaJSON.title) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('title').text(metaJSON.title);
  }

  if (metaJSON.keywords) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=keywords]').text(metaJSON.keywords);
  }

  if (metaJSON.description) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('meta[name=description]').text(metaJSON.description);
  }
};
var domain = window.location.hostname;
var baseURL = "/api/".concat(domain, "/store/");
var instance = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  baseURL: baseURL,
  // prod
  json: true
});

var execute = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    var params,
        headers,
        data,
        method,
        token,
        _args = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context.abrupt("return", instance({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function execute() {
    return _ref.apply(this, arguments);
  };
}();

var instance2 = axios__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  // baseURL: `/proAtm/api/${domain}/store/`, //dev
  baseURL: "/api/admin/",
  //prod
  json: true
});

var admin_execute = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var params,
        headers,
        data,
        method,
        token,
        _args2 = arguments;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
            headers = {
              'Accept': 'application/json'
            };
            data = null;
            method = params.method ? params.method : 'GET';

            if (params.data) {
              data = params.data;
            }

            if (!params.no_auth) {
              token = localStorage.getItem('admin_token');
              headers.Authorization = 'Bearer ' + token;
            }

            if (params.files) {
              headers['Content-Type'] = 'multipart/form-data';
            }

            return _context2.abrupt("return", instance2({
              method: method,
              url: params.url,
              data: data,
              headers: headers
            }));

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function admin_execute() {
    return _ref2.apply(this, arguments);
  };
}();

var admin_login = function admin_login(data) {
  var params = {
    method: 'POST',
    url: 'login',
    data: data,
    no_auth: true
  };
  return admin_execute(params);
};
var get_busi_categories = function get_busi_categories() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "business-category/?".concat(query)
  };
  return admin_execute(params);
};
var view_store = function view_store() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var params = {
    url: "store/?".concat(query)
  };
  return admin_execute(params);
};
var add_store = function add_store(data) {
  var params = {
    url: 'store',
    method: 'POST',
    data: data
  };
  return admin_execute(params);
};
var edit_store = function edit_store(id, data) {
  var params = {
    url: "store/".concat(id),
    method: 'PUT',
    data: data
  };
  return admin_execute(params);
};
var delete_store = function delete_store(id) {
  var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var params = {
    url: "store/".concat(id, "/?").concat(query),
    method: 'DELETE'
  };
  return admin_execute(params);
};
var show_store = function show_store(id) {
  var params = {
    url: "store/".concat(id)
  };
  return admin_execute(params);
};
var add_menu = function add_menu(data) {
  var params = {
    method: 'POST',
    url: 'menu-item',
    data: data
  };
  return execute(params);
};
var view_menu = function view_menu() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "menu-location?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/menu-location?".concat(query);
  }

  return execute(params);
};
var update_menu_order = function update_menu_order(data) {
  var params = {
    method: 'POST',
    url: "menu-location-order",
    data: data
  };
  return execute(params);
};
var update_menu_parent = function update_menu_parent(data) {
  var params = {
    method: 'POST',
    url: "menu-parent",
    data: data
  };
  return execute(params);
};
var delete_menu_order = function delete_menu_order(id) {
  var params = {
    method: 'DELETE',
    url: "menu-item/".concat(id)
  };
  return execute(params);
};
var view_all_pages = function view_all_pages() {
  var params = {
    url: 'page/?type=all'
  };
  return execute(params);
};
var show_template_info = function show_template_info(component) {
  var params = {
    url: "web/template-info/?component=".concat(component),
    no_auth: true
  };
  return execute(params);
};
/**
 * Store Admin Panel
 */

var view_services = function view_services(query) {
  var params = {
    url: "services/?".concat(query)
  };
  return execute(params);
};
var view_pages = function view_pages(query) {
  var params = {
    url: "page/?".concat(query)
  };
  return execute(params);
};
var view_media_images = function view_media_images() {
  var params = {
    url: "media"
  };
  return execute(params);
};
var view_slider = function view_slider() {
  var query = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var auth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  var params = {
    url: "slider/?".concat(query)
  };

  if (!auth) {
    params.no_auth = true;
    params.url = "web/slider?".concat(query);
  }

  return execute(params);
};
var add_slider = function add_slider(data) {
  var params = {
    url: 'slider',
    method: 'POST',
    data: data
  };
  return execute(params);
};
var show_slider = function show_slider(id) {
  var params = {
    url: "slider/".concat(id)
  };
  return execute(params);
};
var update_slider = function update_slider(id, data) {
  var params = {
    url: "slider/".concat(id),
    method: 'PUT',
    data: data
  };
  return execute(params);
};
var delete_slider = function delete_slider(id) {
  var params = {
    url: "slider/".concat(id),
    method: 'DELETE'
  };
  return execute(params);
};
var bulk_action_slider = function bulk_action_slider(data) {
  var params = {
    url: "slider/bulk-action",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_profile = function update_profile(data) {
  var params = {
    url: "edit-profile",
    method: 'POST',
    data: data
  };
  return execute(params);
};
var update_store_profile = function update_store_profile(data) {
  var params = {
    url: "edit-store-profile",
    method: 'POST',
    data: data,
    files: true
  };
  return execute(params);
};
/**
 * Blogs
 */

var view_posts = function view_posts(query) {
  var params = {
    url: "web/post/?".concat(query),
    no_auth: true
  };
  return execute(params);
};
var post_sidebar = function post_sidebar() {
  var params = {
    url: "web/post-sidebar",
    no_auth: true
  };
  return execute(params);
};
/**
 * Send Contact Enquiry
 */

var send_enquiry = function send_enquiry(data) {
  var params = {
    url: 'web/send-inquiry',
    method: 'POST',
    data: data,
    no_auth: true
  };
  return execute(params);
};

/***/ })

}]);