(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[89],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../services/ApiEndPoints.js */ "./resources/js/services/ApiEndPoints.js");
/* harmony import */ var _services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../services/ApiService.js */ "./resources/js/services/ApiService.js");
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-slick */ "./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slick-carousel/slick/slick.css */ "./node_modules/slick-carousel/slick/slick.css");
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    parameters: {
      type: Object
    }
  },
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      slickOptions: {
        slidesToShow: 3
      },
      randomPost: {},
      limit: this.parameters.limit,
      exclude: "",
      type: this.parameters.type
    };
  },
  mounted: function mounted() {
    this.getRandomPost();
  },
  methods: {
    getRandomPost: function getRandomPost() {
      var _this = this;

      this.$emit("loader_token", true);
      var header = {};
      var data = {
        limit: this.limit,
        type: this.type
      };
      Object(_services_ApiService_js__WEBPACK_IMPORTED_MODULE_1__["POST"])(_services_ApiEndPoints_js__WEBPACK_IMPORTED_MODULE_0__["STORE_FRONT_BLOG_LIST"], data, header).then(function (res) {
        _this.randomPost = res.data;

        _this.$emit("loader_token", false);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "suncity_services home-section" }, [
    _c("div", { staticClass: "container py-5" }, [
      _c("h3", [_vm._v(_vm._s(_vm.parameters.heading))]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _vm.parameters.subheading
        ? _c("p", { staticClass: "py-3 w-75 mx-auto" }, [
            _vm._v("\n      " + _vm._s(_vm.parameters.subheading) + "\n    ")
          ])
        : _vm._e(),
      _vm._v(" "),
      Object.entries(_vm.randomPost).length > 0
        ? _c("section", [
            _vm.parameters.type == "grid"
              ? _c(
                  "div",
                  { staticClass: "d-flex flex-wrap" },
                  _vm._l(_vm.randomPost, function(rblog, key) {
                    return _c(
                      "div",
                      {
                        key: key,
                        staticStyle: {
                          "margin-right": "1px",
                          border: "1px solid #ccc",
                          width: "32%",
                          "margin-bottom": "2px"
                        }
                      },
                      [
                        _c("img", { attrs: { src: rblog.image_url.thumb } }),
                        _vm._v(" "),
                        _c(
                          "h5",
                          { staticClass: "card-title pt-3 text-center" },
                          [
                            _c(
                              "router-link",
                              {
                                attrs: {
                                  to: {
                                    name: "BlogDetails",
                                    params: { slug: rblog.slug }
                                  }
                                }
                              },
                              [_vm._v(_vm._s(rblog.name))]
                            )
                          ],
                          1
                        )
                      ]
                    )
                  }),
                  0
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.parameters.type == "slider"
              ? _c(
                  "div",
                  [
                    _c(
                      "slick",
                      { ref: "slick", attrs: { options: _vm.slickOptions } },
                      _vm._l(_vm.randomPost, function(rblog, key) {
                        return _c("div", { key: key }, [
                          _c("img", {
                            staticStyle: {
                              "max-height": "300px",
                              padding: "20px"
                            },
                            attrs: { src: rblog.image_url.thumb }
                          })
                        ])
                      }),
                      0
                    )
                  ],
                  1
                )
              : _vm._e()
          ])
        : _c("section", [
            _c("div", { staticStyle: { "text-align": "center" } }, [
              _vm._v("No Records Found.")
            ])
          ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "circle_dot" }, [
      _c("span"),
      _vm._v(" "),
      _c("span"),
      _vm._v(" "),
      _c("span")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeBlog.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeBlog.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HomeBlog.vue?vue&type=template&id=559ff1c3& */ "./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3&");
/* harmony import */ var _HomeBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HomeBlog.vue?vue&type=script&lang=js& */ "./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HomeBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/front/homeComponent/HomeBlog.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeBlog.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeBlog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HomeBlog.vue?vue&type=template&id=559ff1c3& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/front/homeComponent/HomeBlog.vue?vue&type=template&id=559ff1c3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeBlog_vue_vue_type_template_id_559ff1c3___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);