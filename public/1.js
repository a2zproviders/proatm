(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "suncity_services",
        staticStyle: { background: "#fff !important" }
      },
      [
        _c("div", { staticClass: "container py-5" }, [
          _c("h1", [_vm._v("Our Pricing")]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("p", { staticClass: "py-3 w-75 mx-auto" }, [
            _vm._v(
              "\n             Donec rutrum congue leo eget malesuada. Sed porttitor lectus nibh. Curabitur aliquet quam id dui posuere blandit. Curabitur aliquet quam id dui posuere blandit. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec sollicitudin molestie malesuada. Sed porttitor lectus nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n         "
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(3),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(4),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4 mb-4" }, [
              _c("div", { staticClass: "card" }, [
                _c(
                  "div",
                  { staticClass: "card-body" },
                  [
                    _c("img", { attrs: { src: "img/cS-1.jpg" } }),
                    _vm._v(" "),
                    _c("h5", { staticClass: "card-title pt-3" }, [
                      _vm._v("Gold Package - 2000/-")
                    ]),
                    _vm._v(" "),
                    _vm._m(5),
                    _vm._v(" "),
                    _c(
                      "router-link",
                      {
                        staticClass: "btn btn-primary mt-2",
                        attrs: { to: "#" }
                      },
                      [_vm._v("Check Now")]
                    )
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container mt-3" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("img", {
            attrs: {
              src: "img/cS-1.jpg",
              alt: "",
              width: "100%",
              height: "400px"
            }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container mt-4" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-3" }, [
          _c("div", { staticClass: " feature-box" }, [
            _c("div", { staticClass: "feature-title" }, [
              _c("i", { staticClass: "fa fa-briefcase circle-icon" }),
              _vm._v(" "),
              _c("h4", [_vm._v("Support")])
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(" We provide quality "),
              _c("strong", [_vm._v("support")]),
              _vm._v(
                ", evolve quality outsourcing and professional that strategic theme areas. We synergistically strategize worldwide with functionalities. "
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("div", { staticClass: " feature-box" }, [
            _c("div", { staticClass: "feature-title" }, [
              _c("i", { staticClass: "fa fa-star circle-icon" }),
              _vm._v(" "),
              _c("h4", [_vm._v("Product Reviews")])
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(" A lot of good "),
              _c("strong", [_vm._v("product reviews")]),
              _vm._v(
                ",  evolve quality outsourcing and professional that strategic theme areas. We synergistically strategize worldwide with functionalities. "
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("div", { staticClass: " feature-box" }, [
            _c("div", { staticClass: "feature-title" }, [
              _c("i", { staticClass: "fa fa-user circle-icon" }),
              _vm._v(" "),
              _c("h4", [_vm._v("Guest Checkout")])
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                " You can checkout without,  evolve quality outsourcing and professional that strategic theme areas. We synergistically strategize worldwide with functionalities. "
              )
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("div", { staticClass: " feature-box" }, [
            _c("div", { staticClass: "feature-title" }, [
              _c("i", { staticClass: "fa fa-rocket circle-icon" }),
              _vm._v(" "),
              _c("h4", [_vm._v("Fast Delivery")])
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(" We deliver very fast,  evolve quality "),
              _c("em", [_vm._v("outsourcing and professional")]),
              _vm._v(
                " that strategic theme areas. We synergistically strategize worldwide with functionalities. "
              )
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "circle_dot" }, [
      _c("span"),
      _vm._v(" "),
      _c("span"),
      _vm._v(" "),
      _c("span")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Cras justo odio\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Dapibus ac facilisis in\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                              Morbi leo risus\n                              "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Cras justo odio\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Dapibus ac facilisis in\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Morbi leo risus\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", { staticClass: "list-group" }, [
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Cras justo odio\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("14")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Dapibus ac facilisis in\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("2")
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "li",
        {
          staticClass:
            "list-group-item d-flex justify-content-between align-items-center"
        },
        [
          _vm._v(
            "\n                            Morbi leo risus\n                            "
          ),
          _c("span", { staticClass: "badge badge-primary badge-pill" }, [
            _vm._v("1")
          ])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/backend/Dashboard.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/backend/Dashboard.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=1673257e& */ "./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/backend/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=1673257e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/backend/Dashboard.vue?vue&type=template&id=1673257e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_1673257e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);