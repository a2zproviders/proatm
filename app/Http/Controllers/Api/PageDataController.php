<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\PageData;
use App\Model\Store;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\File;

class PageDataController extends Controller
{
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("page-point/original/{$image}");
                $file_thumb = public_path("page-point/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/page-point/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/page-point/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName."?v=".uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pid = request('pid');
        $query = PageData::query();
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('title', 'LIKE', '%'.$s.'%')
          ->orWhere('description', 'LIKE', '%'.$s.'%');
        }
        $service = $query->where('page_id', $pid)->latest()->paginate(10);
        foreach ($service as $index => $t) {
            $service[$index]->logo_thumb = $t->image ? url('page-point/thumbnail/' . $t->image) : '';
            $service[$index]->logo = $t->image ? url('page-point/original/' . $t->image) : '';
        }

        return response()->json($service->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $service = PageData::create($arr);
        $this->image_upload($request, $service);
        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PageData  $pageData
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, $pageData)
    {
        $pageData  = PageData::findOrFail($pageData);
        $pageData->image_url = !empty($pageData->image) ? url('page-point/thumbnail/'.$pageData->image) : "";
        if (!empty($pageData->image_url)) {
            $binary  = file_get_contents($pageData->image_url);
            $pageData->image_url = 'data:image/jpg;base64,' .base64_encode($binary);
        }

        return response()->json($pageData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PageData  $pageData
     * @return \Illuminate\Http\Response
     */
    public function edit(PageData $pageData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PageData  $pageData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, $pageData)
    {
        $arr = $request->record;
        $pageData = PageData::findOrFail($pageData);
        $pageData->fill($arr)->save();
        $this->image_upload($request, $pageData);
        return response()->json($pageData, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PageData  $pageData
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageData $pageData)
    {
        //
    }
}
