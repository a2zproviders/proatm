<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\ServicePageData;
use App\Model\Store;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\File;

class ServicePageDataController extends Controller
{
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("service-point/original/{$image}");
                $file_thumb = public_path("service-point/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/service-point/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/service-point/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName."?v=".uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $s_id = request('sid');
        $query = ServicePageData::query();
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('title', 'LIKE', '%'.$s.'%')
            ->orWhere('description', 'LIKE', '%'.$s.'%');
        }
        $service = $query->where('s_id', $s_id)->latest()->paginate(10);
        foreach ($service as $index => $t) {
            $service[$index]->logo_thumb = $t->image ? url('service-point/thumbnail/' . $t->image) : '';
            $service[$index]->logo = $t->image ? url('service-point/original/' . $t->image) : '';
        }

        return response()->json($service->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $service = ServicePageData::create($arr);
        $this->image_upload($request, $service);
        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ServicePageData  $servicePageData
     * @return \Illuminate\Http\Response
     */
    public function show($store, $servicePageData)
    {
        $servicePageData = ServicePageData::findOrFail($servicePageData);
        $servicePageData->image_url = !empty($servicePageData->image) ? url('service-point/thumbnail/'.$servicePageData->image) : '';
        if (!empty($servicePageData->image_url)) {
            // $imageArr = explode("?", $servicePageData->image_url);
            // $servicePageData->image_url = $imageArr[0];
            # RFC 2397 conform
            $binary = file_get_contents($servicePageData->image_url);

            # with two slashes
            $servicePageData->image_url = 'data:image/jpg;base64,' . base64_encode($binary);
            // $binary = file_get_contents($uriPhp);
        }
        return response()->json($servicePageData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ServicePageData  $servicePageData
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicePageData $servicePageData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ServicePageData  $servicePageData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, $servicePageData)
    {
        $arr = $request->record;
        $servicePageData = ServicePageData::findOrFail($servicePageData);
        $service = $servicePageData->fill($arr)->save();
        $this->image_upload($request, $servicePageData);
        return response()->json('success', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ServicePageData  $servicePageData
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicePageData $servicePageData)
    {
        //
    }
}
