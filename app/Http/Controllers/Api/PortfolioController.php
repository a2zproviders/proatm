<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Portfolio;
use App\Model\Category;
use App\Model\Store;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class PortfolioController extends Controller
{
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("img/portfolio/original/{$image}");
                $file_thumb = public_path("img/portfolio/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/portfolio/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/img/portfolio/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName."?v=".uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $query = Portfolio::with('category');
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (request('drafted') == 'true') {
            $query->where('is_visible', 0);
        } else {
            $query->where('is_visible', 1);
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('name', 'LIKE', '%'.$s.'%')
          ->orWhere('slug', 'LIKE', '%'.$s.'%');
        }
        $portfolio = $query->where('store_id', $store->uid)
      ->latest()
      ->paginate(10);
        $re = $portfolio;
        $p_r = Portfolio::where('store_id', $store->uid)->where('is_visible', 1)->count();
        $d_r = Portfolio::where('store_id', $store->uid)->where('is_visible', 0)->count();
        $t_r = Portfolio::where('store_id', $store->uid)->onlyTrashed()->count();
        if (!empty($request->type) && $request->type == 'portfolio-list') {
            $counts = ['draft'=>$d_r, 'published'=>$p_r,'trashed'=>$t_r];
            $re = compact('portfolio', 'counts');
        }
        return response()->json($re);
    }
    public function portfolioList(Store $store)
    {
        $query = Portfolio::where('store_id', $store->uid)
         ->select('image', 'slug', 'name')
         ->where('is_visible', 1)
        ->latest()
        ->paginate(30);
        return response()->json($query);
    }
    public function singleShow(Store $store, $portfolio)
    {
        $portfolios = Portfolio::
       with(
           [
           'category' => function ($q) {
               $q->select('categories.id', 'categories.name', 'categories.slug');
           }]
       )
       ->where('store_id', $store->uid)->where('slug', $portfolio)->firstOrFail();
        $portfolios->image = $portfolios->image ? url('img/portfolio/original/' . $portfolios->image) : '';
        // $page->image_url = !empty($image) ? url('img/product/original/' . $image) : '';


        return response()->json($portfolios);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $exist = Portfolio::where('slug', '=', $arr['slug'])->first();

        if ($exist===null) {
            $arr['slug'] = Str::slug($arr['slug']);
            $arr['store_id'] = auth()->user()->store_id;
            $portfolio  = Portfolio::create($arr);
            $this->image_upload($request, $portfolio);
            $category = Category::find(explode(',', $request->categories));
            $portfolio->category()->attach($category);
            $msg = "Portfolio created successfully";
            $msg2 = "Great!!!";
            $status = 1;
        } else {
            $msg2 = "Record not created";
            $msg = "Slug already exist. Change slug and try again.";
            $status = 2;
        }
        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show($store, $portfolio)
    {
        $portfolio = Portfolio::
      with(
          [
          'category' => function ($q) {
              $q->select('categories.id');
          }]
      )
      ->where('store_id', auth()->user()->store_id)->findOrFail($portfolio);
        $cat = [];
        if (!empty($portfolio->category)) {
            foreach ($portfolio->category as $key=>$value) {
                $cat[$key] = $value->id;
            }
        }

        $portfolio->checked_category = $cat; //$cat;

        unset($portfolio->category);

        $portfolio->image = $portfolio->image ? url('img/portfolio/original/' . $portfolio->image) : '';
        // $page->image_url = !empty($image) ? url('img/product/original/' . $image) : '';

        if (!empty($portfolio->image)) {
            $binary = file_get_contents($portfolio->image);
            $portfolio->image_url_prefill = 'data:image/jpg;base64,'. base64_encode($binary);
        }
        return response()->json($portfolio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, Portfolio $portfolio)
    {
        $data = $request->record;
        $portfolio->fill($data);
        $portfolio->save();
        $category = Category::find(explode(',', $request->categories));
        $portfolio->category()->sync($category);
        $this->image_upload($request, $portfolio);
        $msg2 = "Great !!!";
        $msg = "Record updated successfully.";
        $status = 1;
        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Portfolio::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Portfolio::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Restore') {
            $status = Portfolio::withTrashed()->whereIn('id', explode(",", $request->ids))->restore();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Portfolio::withTrashed()->whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Portfolio::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $store, $id)
    {
        $page = Portfolio::withTrashed()->findOrFail($id);
        // dd($page);
        if (request('flag') == 'Trash') {
            if ($page->delete()) {
                return response()->json(['status'=>1, 'message'=>'deleted'], 200);
            }
        } elseif (request('flag') == 'Restore') {
            if (Portfolio::withTrashed()->where('id', $page->id)->restore()) {
                return response()->json(['status'=>1, 'message'=>'Record Re-store'], 200);
            }
        } elseif (request('flag') == 'Delete') {
            if (Portfolio::withTrashed()->where('id', $page->id)->forceDelete()) {
                return response()->json(['status'=>1, 'message'=>'Record deleted'], 200);
            }
        }
    }
    public function updateStatus(Request $request, $store, $id)
    {
        $post  = Portfolio::withTrashed()->findOrFail($id);
        if ($post) {
            $post->update(['is_visible'=>!$post->is_visible]);
            $msg = "Updated successfully";
        } else {
            $msg = "Page not found";
        }
        return response()->json(['status'=> 1, 'message'=>$msg], 200);
    }
}
