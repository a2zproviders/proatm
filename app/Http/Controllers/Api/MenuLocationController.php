<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\MenuLocation;
use App\Model\MenuItem;
use App\Model\Store;
use Illuminate\Http\Request;

class MenuLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $lists = MenuLocation::with([
             'menuitems' => function ($q) use ($store) {
                 $q->where('menu_items.store_id', $store->uid)
                 ->has('page')
                 ->orWhere('type', 'external')
                 ->orderBy('sort_order', 'ASC');
             },
             'menuitems.page'
             ])
             ->get();

        if (empty($request->type) || $request->type != 'web') {
            return response()->json($lists);
        } else {
            $listArr = [];
            if (!$lists->isEmpty()) {
                foreach ($lists as $key => $list) {
                    $listArr[$list->location] = $list->menuitems;
                }
            }

            return response()->json($listArr);
        }
    }

    public function all()
    {
        $list =  MenuLocation::pluck('location', 'id');

        return response()->json($list);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function list_item()
    {
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function show(MenuLocation $menuLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuLocation $menuLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MenuLocation  $menuLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuLocation $menuLocation)
    {
        //
    }
}
