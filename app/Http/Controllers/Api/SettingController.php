<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        // dd($request->all());
        $user_id  = auth()->user()->store_id;
        $validator = Validator::make($request->all(), [
          'password'         => 'required|string|min:8|max:16',
          'new_password'     => 'required|string|min:8|max:16|same:confirm_password',
          'confirm_password' => 'required|string|min:8|max:16|same:new_password'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 0, 'message' => 'Error occures!'], 401);
        } else {
            $findUser  = new User();
            if (Hash::check($request->password, auth()->user()->password)) {
                $n_p = Hash::make($request->new_password);
                $findUser->where("store_id", $user_id)->update(['password'=> $n_p]);
                auth()->user()->token()->delete();
                return response()->json(['status' => 1, 'message' => 'Successfully updated!'], 200);
            } else {
                return response()->json(['status' => 2, 'message' => 'Current password not match'], 200);
            }
        }
    }
}
