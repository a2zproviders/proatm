<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Model\Pages;
use App\Model\MenuItem;
use App\Model\Store;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class PageController extends Controller
{
    protected function image_upload($request, $page)
    {
        if ($request->hasFile('image')) {
            if (!empty($page->image)) {
                $nameArr  = explode("?v=", $page->image);
                $image     = $nameArr[0];
                $file_main  = public_path("img/product/original/{$image}");
                $file_thumb = public_path("img/product/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $page->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/product/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/img/product/original');
            $image->move($destinationPath, $imageName);

            $page->image = $imageName."?v=".uniqid();
            $page->save();
            return true;
        } else {
            return false;
        }
    }
    public function index(Request $request, Store $store)
    {
        $user  = auth()->user();
        if (empty($request->type) || $request->type != 'all') {
            $query = Pages::withCount('page_points');
            if (request('trash') == 'true') {
                $query->onlyTrashed();
            }
            if (!empty(request('s'))) {
                $s = request('s');
                $s = trim($s);
                $query->where(function ($q) use ($s) {
                    $q->where('title', 'LIKE', '%'.$s.'%');
                });
            }
            $page = $query->where('store_id', $store->uid)->latest()->paginate(10);
            foreach ($page as $index => $t) {
                $page[$index]->logo_thumb = $t->image ? url('img/product/thumbnail/' . $t->image) : '';
                $page[$index]->logo = $t->image ? url('img/product/original/' . $t->image) : '';
            }
        } elseif($request->type == 'all' && $request->response_type == 'array') {
            $page = Pages::where('store_id', $store->uid)->orderBy('title')->get();
        } else {
            $page = Pages::where('store_id', $store->uid)->pluck('title', 'id');
        }
        return response()->json($page, 200);
    }
    /**
     * Update function
     * @param Int $page
     * @method PUT
     */
    public function update(Request $request, $store, Pages $page)
    {
        $data = $request->record;
        $data = array_filter($data);
        $page->fill($data);
        $this->image_upload($request, $page);
        $page->menu_location = is_array($data['menu_location']) ? implode(',', $data['menu_location']):$data['menu_location'];
        if ($page->save()) {
            $menuLocations = !empty($data['menu_location']) ? explode(",", $data['menu_location']) : [];
            if (!empty($menuLocations) && is_array($menuLocations)) {
                MenuItem::whereNotIn('mp_id', $menuLocations)->where('page_id', $page->id)->where('store_id', auth()->user()->store_id)->delete();
                foreach ($menuLocations as $m_id) {
                    $m_item = [
                      'mp_id' => $m_id,
                      'page_id' => $page->id,
                      'store_id' => auth()->user()->store_id
                    ];
                    MenuItem::updateOrCreate($m_item, $m_item);
                }
            } else {
                MenuItem::where('page_id', $page->id)->where('store_id', auth()->user()->store_id)->delete();
            }
            $msg = 'Record updated successfully';
        } else {
            $msg = 'Some server erros occurs, please try again.';
        }


        return response()->json($msg, 200);
    }
    public function show(Request $request, Store $store, Pages $page)
    {
        $page->menu_location = !empty($page->menu_location) ? explode(',', $page->menu_location) : [];
        $page->template_name = $page->template_info->component;
        $image = $page->image;
        $page->image = $page->image ? url('img/product/original/' . $page->image) : '';

        $page = $page->toArray();
        $page = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $page);

        return response()->json($page);
    }
    public function WebShow(Store $store, $id)
    {
        $page = Pages::where('template', $id)->where('store_id', $store->uid)->firstOrFail();
        $page->image = $page->image ? url('img/product/original/' . $page->image) : '';

        // $page = (array) $page;
        $page = $page->toArray();
        $page = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $page);

        return response()->json($page);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $menuLocations = $arr['menu_location'];
        $arr['menu_location'] = is_array($arr['menu_location']) ? implode(',', $arr['menu_location']) : $arr['menu_location'];
        $arr['store_id'] = auth()->user()->store_id;
        $arr['slug'] = Str::slug($arr['slug']);
        $pages = Pages::create($arr);
        $this->image_upload($request, $pages);
        if (!empty($arr['menu_location'])) {
            $menuLocations = !empty($arr['menu_location']) ? explode(",", $arr['menu_location']) : [];
            foreach ($menuLocations as $m_id) {
                $m_item = [
              'mp_id'     => $m_id,
              'page_id'   => $pages->id,
              'store_id'  => auth()->user()->store_id
            ];
                MenuItem::updateOrCreate($m_item, $m_item);
            }
        }
        // $this->image_upload($request, $pages);

        return response()->json('success', 200);
    }
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Pages::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Pages::whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Pages::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Pages::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
    public function destroy(Request $request, $store, $id)
    {
        $page = Pages::withTrashed()->findOrFail($id);
        // dd($page);
        if (request('flag') == 'Trash') {
            if ($page->delete()) {
                return response()->json(['status'=>1, 'message'=>'deleted'], 200);
            }
        } elseif (request('flag') == 'Restore') {
            if (Pages::withTrashed()->where('id', $page->id)->restore()) {
                return response()->json(['status'=>1, 'message'=>'Record Re-store'], 200);
            }
        } elseif (request('flag') == 'Delete') {
            if (Pages::withTrashed()->where('id', $page->id)->forceDelete()) {
                return response()->json(['status'=>1, 'message'=>'Record deleted permanently!!!'], 200);
            }
        }
    }
    public function updateStatus(Request $request, $store, $id)
    {
        $page  = Pages::withTrashed()->findOrFail($id);
        if ($page) {
            $page->update(['is_visible'=>!$page->is_visible]);
            $msg = "Updated successfully";
        } else {
            $msg = "Page not found";
        }
        return response()->json(['status'=> 1, 'message'=>$msg], 200);
    }
}