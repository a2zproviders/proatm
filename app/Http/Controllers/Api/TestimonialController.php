<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Testimonial;
use App\Model\Store;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\File;

class TestimonialController extends Controller
{
    protected function image_upload($request, $testimonial)
    {
        if ($request->hasFile('logo')) {
            if (!empty($testimonial->logo)) {
                $nameArr  = explode("?v=", $testimonial->logo);
                $logo     = $nameArr[0];
                $file_main  = public_path("img/testimonial/original/{$logo}");
                $file_thumb = public_path("img/testimonial/thumbnail/{$logo}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('logo');
            $imageName = $testimonial->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/testimonial/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/img/testimonial/original');
            $image->move($destinationPath, $imageName);

            $testimonial->logo = $imageName."?v=".uniqid();
            $testimonial->save();
            return true;
        } else {
            return false;
        }
    }
    public function store(Request $request)
    {
        $arr = $request->record;
        $arr['store_id'] = auth()->user()->store_id;
        $testimonial = Testimonial::create($arr);

        $this->image_upload($request, $testimonial);

        return response()->json('success', 200);
    }
    public function index()
    {
        $user = auth()->user();
        $query = Testimonial::query();
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('testimonial', 'LIKE', '%'.$s.'%')
                ->orWhere('name', 'LIKE', '%'.$s.'%')
                ->orWhere('website', 'LIKE', '%'.$s.'%')
                ->orWhere('company', 'LIKE', '%'.$s.'%');
        }
        $testimonial = $query->where('store_id', $user->store_id)->latest()->paginate(10);
        foreach ($testimonial as $index => $t) {
            $testimonial[$index]->logo_thumb = $t->logo ? url('img/testimonial/thumbnail/' . $t->logo) : '';
            $testimonial[$index]->logo = $t->logo ? url('img/testimonial/original/' . $t->logo) : '';
        }
        return response()->json($testimonial->toArray(), 200);
    }
    public function TestimonialList(Store $store)
    {
        $query = Testimonial::query();
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('testimonial', 'LIKE', '%'.$s.'%')
              ->orWhere('name', 'LIKE', '%'.$s.'%')
              ->orWhere('website', 'LIKE', '%'.$s.'%')
              ->orWhere('company', 'LIKE', '%'.$s.'%');
        }
        $testimonial = $query->where('store_id', $store->uid)->where('is_visible', 1)->latest()->paginate(10);
        foreach ($testimonial as $index => $t) {
            $testimonial[$index]->logo_thumb = $t->logo ? url('img/testimonial/thumbnail/' . $t->logo) : '';
            $testimonial[$index]->logo = $t->logo ? url('img/testimonial/original/' . $t->logo) : '';
        }
        return response()->json($testimonial, 200);
    }
    public function update(Request $request, $store, Testimonial $testimonial)
    {
        $testimonial->fill($request->record);

        if ($testimonial->save()) {
            $this->image_upload($request, $testimonial);
            $msg = 'Record updated successfully';
        } else {
            $msg = 'Some error occurs. try again !!!';
        }
        return response()->json($msg, 200);
    }
    public function show($store, Testimonial $testimonial)
    {
        $testimonial->logo = $testimonial->logo ? url('img/testimonial/original/' . $testimonial->logo) : '';

        if (!empty($testimonial->logo)) {
            $binary = file_get_contents($testimonial->logo);
            $testimonial->image_url_prefill = 'data:image/jpg;base64,'. base64_encode($binary);
        }
        return response()->json($testimonial, 200);
    }
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Testimonial::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Testimonial::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Testimonial::whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Testimonial::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
    public function destroy(Request $request, $store, $id)
    {
        $testimonial = Testimonial::withTrashed()->findOrFail($id);
        if (request('flag') == 'Trash') {
            if ($testimonial->delete()) {
                return response()->json(['status'=>1, 'message'=>'deleted'], 200);
            }
        } elseif (request('flag') == 'Restore') {
            if (Testimonial::withTrashed()->where('id', $testimonial->id)->restore()) {
                return response()->json(['status'=>1, 'message'=>'Record Re-store'], 200);
            }
        } elseif (request('flag') == 'Delete') {
            if (Testimonial::withTrashed()->where('id', $testimonial->id)->forceDelete()) {
                return response()->json(['status'=>1, 'message'=>'Record Re-store'], 200);
            }
        }
    }
}
