<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Post;
use App\Model\Category;
use App\Model\Inquiry;
use App\Model\Tag;
use App\Model\Store;
use Illuminate\Http\Request;
use Image;
use Mail;
use Illuminate\Support\Facades\auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ContactInqueryController extends Controller
{
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("img/post/original/{$image}");
                $file_thumb = public_path("img/post/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id . '.' . $image->getClientOriginalExtension();

            $destinationPath = public_path('/img/post/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);
            //save original image
            $destinationPath = public_path('/img/post/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName . "?v=" . uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }

    public function sidebar(Store $store)
    {
        $data = [];
        $data['archive'] = Post::where('store_id', $store->uid)->groupBy('date')->latest()->selectRaw('DATE_FORMAT(created_at, "%Y-%m") AS date')->pluck('date');

        $data['categories'] = Category::where('store_id', $store->uid)
            ->whereHas('category_type', function ($q) {
                $q->where('name', 'Post');
            })
            ->orderBy('name')
            ->pluck('name', 'slug');

        $data['tags'] = Tag::where('store_id', $store->uid)->pluck('name', 'slug');

        return response()->json($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function randomPost(Store $store)
    {
        $postCount = Post::where('store_id', $store->uid)->get()->count();
        // dd($postCount);
        if ($postCount > 9) {
            $randomPost = Post::select('id', 'name', 'slug', 'excerpt', 'image')->where('is_visible', 1)->where('store_id', $store->uid)->get()->random(9);
        } else {
            $randomPost = Post::select('id', 'name', 'slug', 'excerpt', 'image')->where('is_visible', 1)->where('store_id', $store->uid)->get();
        }
        return response()->json($randomPost);
    }
    public function randomPostHome(Request $request, Store $store)
    {
        $postCount = Post::where('store_id', $store->uid)->get()->count();
        // dd($request->limit);
        if ($postCount > $request->limit) {
            $randomPost = Post::select('id', 'name', 'slug', 'excerpt', 'image')->where('is_visible', 1)->where('store_id', $store->uid)->get()->random($request->limit);
        } else {
            $randomPost = Post::select('id', 'name', 'slug', 'excerpt', 'image')->where('is_visible', 1)->where('store_id', $store->uid)->get();
        }
        return response()->json($randomPost);
    }
    
    public function index(Request $request, Store $store)
    {
        $query = Inquiry::query();
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('name', 'LIKE', '%' . $s . '%')
                ->orWhere('email', 'LIKE', '%' . $s . '%')
                ->orWhere('mobile', 'LIKE', '%' . $s . '%');
        }
        if (!empty(request('status'))) {
                $query->where('status', request('status'));
        }

        $post = $query->where('user_id', $store->uid)
            ->latest()
            ->paginate(10);

        $re = compact('post');

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $exist = Post::where('slug', '=', $arr['slug'])->first();

        if ($exist === null) {
            $arr['slug'] = Str::slug($arr['slug']);
            $arr['store_id'] = auth()->user()->store_id;
            $post  = Post::create($arr);
            $this->image_upload($request, $post);
            $category = Category::find(explode(',', $request->categories));
            $post->category()->attach($category);
            $tag = Tag::find(explode(',', $request->tag));
            $post->tag()->attach($tag);
            $msg = "Post created successfully";
            $msg2 = "Great!!!";
            $status = 1;
        } else {
            $msg2 = "Record not created";
            $msg = "Slug already exist. Change slug and try again.";
            $status = 2;
        }
        return response()->json(['status' => $status, 'message' => $msg, 'message1' => $msg2]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function singleShow(Store $store, $post)
    {
        $post = Post::with(
            [
                'tag' => function ($q) {
                    $q->select('tags.id', 'tags.name', 'tags.slug');
                },
                'category' => function ($q) {
                    $q->select('categories.id', 'categories.name', 'categories.slug');
                }
            ]
        )
            ->where('store_id', $store->uid)->where('slug', $post)->firstOrFail();
        $post->image = $post->image ? url('img/post/original/' . $post->image) : '';
        // $page->image_url = !empty($image) ? url('img/product/original/' . $image) : '';

        $post = $post->toArray();
        $post = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $post);

        return response()->json($post);
    }
    public function show(Store $store, $post)
    {
        $posts = Post::with(
            [
                'tag' => function ($q) {
                    $q->select('tags.id');
                },
                'category' => function ($q) {
                    $q->select('categories.id');
                }
            ]
        )
            ->where('store_id', $store->uid)->findOrFail($post);
        $cat = [];
        $tag = [];
        if (!empty($posts->category)) {
            foreach ($posts->category as $key => $value) {
                $cat[$key] = $value->id;
            }
        }
        if (!empty($posts->tag)) {
            foreach ($posts->tag as $key => $value) {
                $tag[$key] = $value->id;
            }
        }
        $posts->checked_category = $cat; //$cat;
        $posts->checked_tag = $tag; //$cat;
        unset($posts->category);
        unset($posts->tag);

        if (!empty($posts->image)) {
            $binary = file_get_contents(env('APP_URL') . 'img/post/original/' . $posts->image);
            $posts->image_url_prefill = 'data:image/jpg;base64,' . base64_encode($binary);
        }
        $posts->image = $posts->image ? url('img/post/original/' . $posts->image) : '';

        $posts = $posts->toArray();
        $posts = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $posts);

        return response()->json($posts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, Post $post)
    {
        $data = $request->record;
        $data = array_filter($data);
        $post->fill($data);
        $post->save();
        $category = Category::find(explode(',', $request->categories));
        $post->category()->sync($category);
        $tag = Tag::find(explode(',', $request->tag));
        $post->tag()->sync($tag);
        $this->image_upload($request, $post);

        $msg2 = "Great !!!";
        $msg = "Record updated successfully.";
        $status = 1;

        return response()->json(['status' => $status, 'message' => $msg, 'message1' => $msg2]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Post::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Post::whereIn('id', explode(",", $request->ids))->update(array('is_visible' => 1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Restore') {
            $status = Post::withTrashed()->whereIn('id', explode(",", $request->ids))->restore();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Post::withTrashed()->whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Post::whereIn('id', explode(",", $request->ids))->update(array('is_visible' => 0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status' => $status, 'message' => $msg], 200);
    }

    public function destroy(Request $request, $store, $id)
    {
        $page = Inquiry::findOrFail($id);
        // dd($page);
        if ($page->delete()) {
            return response()->json(['status' => 1, 'message' => 'deleted'], 200);
        }
    }

    public function updateStatus(Request $request, Store $store, $id)
    {
        $inquiry  = Inquiry::findOrFail($id);
        if ($inquiry) {
            // dd($request->record['status']);
            $inquiry->update(['status' => $request->record['status'], 'reply' => $request->record['reply']]);
            $msg = "Updated successfully";

            // Sending Mail
            $data = [
                'to'                => $store->user->email,
                'receiver'          => $store->user->name,
                'sender'            => $inquiry->name,
                'sender_email'      => $inquiry->email,
                'subject'           => 'Contact Enquiry Status has been changed via ' . $store->store_name,
                'subject_user'      => 'Your Enquiry Status has been changed to ' . $store->store_name,
                'store'             => $store,
                'inquiry'           => $inquiry
            ];
            Mail::send('emails.enquiry-user', $data, function ($message) use ($data) {
                extract($data);

                $message->to($sender_email, $sender)
                    ->subject($subject_user)
                    ->replyTo($to, $receiver)
                    ->from($to, $receiver);
            });
        } else {
            $msg = "Page not found";
        }
        return response()->json(['status' => 1, 'message' => $msg], 200);
    }
}
