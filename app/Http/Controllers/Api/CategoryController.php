<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\CategoryType;
use App\Model\Store;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("img/category/original/{$image}");
                $file_thumb = public_path("img/category/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/category/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            //save original image
            $destinationPath = public_path('/img/category/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName."?v=".uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }
    public function all(Store $store, $categoryType)
    {
        $categoryType = CategoryType::where('name', $categoryType)->first();
        $category =  Category::where('store_id', $store->uid)->where('category_type_id', $categoryType->id)->pluck('name', 'id');

        return response()->json($category);
    }
    public function allCategory(Store $store, $categoryType)
    {
        $categoryType = CategoryType::where('name', $categoryType)->first();
        $category =  Category::select('name', 'slug', 'category_type_id', 'id')
            ->with(['service'=>function ($q) {
                return $q->select('title', 'slug', 'id', 'category_id');
            }])
            ->where('store_id', $store->uid)
            ->where('category_type_id', $categoryType->id)
            ->latest()
            ->get();

        return response()->json($category);
    }
    public function blogCategory(Request $request, Store $store, $categoryType = 'Post')
    {
        $categoryType = CategoryType::where('name', $categoryType)->first();
        $query = Category::query();
        $select = ['id', 'name', 'image', 'slug'];
        if (!empty($request->exclude)) {
            foreach (explode(',', $request->exclude) as $key => $field) {
                $key = array_search($field, $select);
                unset($select[$key]);
            }
        }

        if (!empty($request->limit)) {
            $query->skip(0)->take($request->limit);
        }
        $category =  $query->select($select)->where('store_id', $store->uid)->where('category_type_id', $categoryType->id)->get();
        return response()->json($category->toArray());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $limit = $request->limit ?: 10;
        $category = Category::with('category_type');
        if($request->trashed) {
            $category->onlyTrashed();
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $category->where('name', 'LIKE', '%'.$s.'%');
        }
        if (!empty(request('type'))) {
            $s = request('type');
            $s = ucwords(trim($s));
            $category->whereHas('category_type', function ($q) use ($s) {
                $q->where('name', $s);
            });
        }
        $cat = $category->where('store_id', auth()->user()->store_id)->latest()->paginate($limit);
        $count = [];

        $query = Category::where('store_id', auth()->user()->store_id);
        if (!empty(request('type'))) {
            $s = request('type');
            $s = ucwords(trim($s));
            $query->whereHas('category_type', function ($q) use ($s) {
                $q->where('name', $s);
            });
        }
        $count['all']   = $query->count();
        $count['trash'] = $query->onlyTrashed()->count();
        return response()->json([
            'count'      => $count, 
            'categories' => $cat
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function singleShow(Store $store, $category)
    {
        $categories = Category::
         with(
             ['posts' => function ($q) {
                 $q->select('posts.id', 'posts.name', 'posts.slug', 'posts.image');
             },
           ]
         )
         ->where('store_id', $store->uid)->where('slug', $category)->firstOrFail();
        $posts = $categories->posts;
        unset($categories->posts);
        return response()->json(['category'=>$categories, 'posts'=>$posts]);
    }
    public function store(Request $request)
    {
        // dd($request->record);
        $arr = $request->record;
        $arr['slug'] = Str::slug($arr['slug']);
        $arr['store_id'] = auth()->user()->store_id;
        $category = Category::create($arr);
        $category_id = $category->id;
        $categories =  Category::where('store_id', auth()->user()->store_id)->where('category_type_id', $arr['category_type_id'])->pluck('name', 'id');
        return response()->json(compact('category_id', 'categories'));
    }
    public function addCategory(Request $request)
    {
        // dd($request->record);
        $arr = $request->record;
        $categoryType = CategoryType::where('name', $arr['category_type_id'])->first();
        $arr['slug'] = Str::slug($arr['slug']);
        $arr['category_type_id'] = $categoryType->id;
        $exist = Category::where('slug', '=', $arr['slug'])->where('store_id', auth()->user()->store_id)->where('category_type_id', $categoryType->id)->count();
        if (!$exist) {
            $arr['store_id'] = auth()->user()->store_id;
            $category = Category::create($arr);
            $this->image_upload($request, $category);
            $msg = "Category created successfully";
            $msg2 = "Great!!!";
            $status = 1;
        } else {
            $msg2 = "Record not created";
            $msg = "Slug already exist. Change slug and try again.";
            $status = 2;
        }
        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Category $category)
    {
        $category->image = $category->image ? url('img/category/original/' . $category->image) : '';
        // $page->image_url = !empty($image) ? url('img/product/original/' . $image) : '';

        if (!empty($category->image)) {
            $binary = file_get_contents($category->image);
            $category->image_url_prefill = 'data:image/jpg;base64,'. base64_encode($binary);
        }
        return response()->json($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, Category $category)
    {
        $data = $request->record;
        // dd($category);
        $data['slug'] = Str::slug($data['slug']);
        // $exist = Category::where('slug', '=', $data['slug'])->first();
        // if ($exist === null) {
        $category->fill($data);
        $category->save();
        $this->image_upload($request, $category);
        $msg = "Category updated successfully";
        $msg2 = "Great!!!";
        $status = 1;
        // } else {
        //     $msg2 = "Record not created";
        //     $msg = "Slug already exist. Change slug and try again.";
        //     $status = 2;
        // }
        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $store, $id)
    {
        $category = Category::withTrashed()->findOrFail($id);

        switch ($request->action) {
            case 'delete-permanent':
                $category->forceDelete();
                return response()->json(['message'=>'deleted permanently.']);
                break;

            case 'restore':
                $category->restore();
                return response()->json(['message'=>'restored.']);
                break;
            
            default:
                $category->delete();
                return response()->json(['message'=>'deleted']);
                break;
        }
    }
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Category::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Category::withTrashed()->whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Restore') {
            $status = Category::withTrashed()->whereIn('id', explode(",", $request->ids))->restore();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
}
