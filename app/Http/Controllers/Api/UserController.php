<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use   App\Model\User;
use   App\Model\Store;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $successStatus = 202;
    protected $errorStatus = 401;

    /**
      *login API
      *
      */
      public function admin_login(Request $request)
      {
          if (Auth::attempt([
              'login'       => $request->login, 
              'password'    => $request->password, 
              'role'        => 1
            ])) {
              $user = auth()->user();
              $user = User::with(['role_info'])->findOrFail(auth()->user()->id);
              $success['token'] = $user->createToken('myApp')->accessToken;
              $success['user']  = $user;
              return response()->json(['success' => $success, $this->successStatus]);
          } else {
              return response()->json(['error'=>'Unauthorised', 'request' => $request->all()], 401);
          }
      }

    /**
      *login API
      *
      */
    public function login(Store $store)
    {
        if (Auth::attempt([
                'login'=>request('login'), 
                'password'=>request('password'), 
                'role' => 2,
                'store_id'  => $store->uid
            ])) {
            $user = auth()->user();
            // dd($user);
            $user = User::with(['role_info'])->findOrFail(auth()->user()->id);
            $success['token'] = $user->createToken('myApp')->accessToken;
            $success['user'] = $user;
            return response()->json(['success'=>$success, $this->successStatus]);
        } else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
        *register api
        *
        * @return \Illuminate\Http\Response
        */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'user'  => 'required|array',
          'user.first_name'=>'required|regex:/^[a-zA-Z ]+/',
          'user.last_name'=>'regex:/^[a-zA-Z ]*/',
          'user.email' => 'required|email',
          'user.mobile' => 'required|numeric|regex:/\d{10}/',
          'user.password' =>'required|string|min:8|max:16|same:user.password',
          'user.c_password' =>'required|string|min:8|max:16|same:user.password',
          'store'           => 'required|array',
          'store.store_name'  => 'required|regex:/^[a-zA-Z ]+/',
          'store.category_id'  => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>'Validation error!!!'], $this->errorStatus);
        }
        $input = $request->get('user');
        unset($input['c_password']);
        $input['password'] = Hash::make($input['password']);
        $input['role'] = 2;
        $emailArr = explode("@", $input['email']);
        $input['login'] = $emailArr[0];

        $store = $request->get('store');
        $user = User::create($input);
        $user->login .= $user->id; // Append ID
        $user->store_id = $user->id;
        $user->save();
        $user->store()->create($store);

        $success['token'] = $user->createToken('proAtm')->accessToken;
        $success['name']  =$user->name;
        return response()->json(['success'=>$success], 201);
    }
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->delete();

        return response()->json(['success' => 'logout success']);
    }
    public function userDetails($store)
    {
        $store = Store::with('user')->where('store_url', $store)->firstOrFail();
        if($store->logo) {
            $store->logo = url("img/stores/{$store->logo}");
        }

        if($store->favicon) {
            $store->favicon = url("img/stores/{$store->favicon}");
        }
        return response()->json($store, 200);
    }
    public function editProfile(Request $request, $store)
    {
        $request->validate([
            'user'          => 'required|array',
            'user.title'    => 'required|string',
            'user.first_name'    => 'required|string'
        ]);

        $user = auth()->user();
        $user->fill($request->user);
        $user->save();

        return response()->json([
            'message'   => 'Profile updated successfully done.',
            'data'      => $user
        ]);
    }
}
