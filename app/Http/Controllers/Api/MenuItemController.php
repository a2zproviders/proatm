<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\MenuItem;
use App\Model\MenuLocation;
use App\Model\Store;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        $request->validate([
            'form'          => 'required|array',
            'form.label'    => 'required|string',
            'form.type'     => 'required',
            'menu_locations' => 'required|array'
        ]);

        foreach ($request->menu_locations as $mp_id) {
            $menuItem = new MenuItem($request->form);
            $menuItem->mp_id = $mp_id;
            $menuItem->store_id = $store->uid;
            $menuItem->save();
        }


        return response()->json(['message' => 'Data had been added.', 'data' => $menuItem]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function show(MenuItem $menuItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach ($request->menu_indexs as $id => $sort_order) {
            $menuItem = MenuItem::find($id);
            $menuItem->sort_order = $sort_order;
            $menuItem->save();
        }

        return response()->json(['status' => 1, 'message' => 'success']);
    }

    public function parentupdate(Request $request)
    {
        $menuItem = MenuItem::find($request->id);
        if ($request->parent && $request->parent == null) {
            $menuItem->parent = null;
        }else{
            $menuItem->parent = $request->parent;
        }
        $menuItem->save();

        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, MenuItem $menuItem)
    {
        $menu_id = MenuItem::findOrFail($menuItem->id);
        // dd($menu_id);
        if ($menu_id) {
            if ($menuItem->delete()) {
                return response()->json(['status' => 1, 'message' => 'Deleted successfully!'], 200);
            }
        } else {
            return response()->json(['status' => 2, 'message' => 'Menu item not found!'], 200);
        }
    }
}
