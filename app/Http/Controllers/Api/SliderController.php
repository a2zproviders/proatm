<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\Slider;
use App\Model\Store;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $query = Slider::where('store_id', $store->uid);
        if(isset($request->is_visible))
        {
            $query->where('is_visible', $request->is_visible);
        }
        $slider = $query->get();
        return response()->json($slider);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        $request->validate([
            'slider'        => 'required|array',
            'slider.title'  => 'required|string',
            'slider.image'  => 'required|string',
            'slider.type'   => 'required|string'
        ]);

        $slider = new Slider($request->slider);
        $slider->store_id = $store->uid;
        $slider->save();

        return response()->json([
            'message'   => 'New record has been added.',
            'data'      => $slider
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Slider $slider)
    {
        return response()->json($slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store, $slider_id)
    {
        $request->validate([
            'slider'  => 'required|array'
        ]);
            
        $slider = Slider::findOrFail($slider_id);
        $slider->fill($request->slider);
        $slider->store_id = $store->uid;
        $slider->save();

        return response()->json([
            'message'   => 'A record has been updated.',
            'data'      => $slider
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Slider $slider)
    {
        $slider->delete();
        return response()->json([
            'message'   => 'Selected record has been deleted.',
            'data'      => $slider
        ]);
    }

    public function bulk_action(Request $request, Store $store)
    {
        $request->validate([
            'action' => 'required|string',
            'ids'   => 'required|array'
        ]);
        $slider = Slider::whereIn('id', $request->ids);
        switch ($request->action) {
            case 'Delete':
                $slider->delete();
                break;
            case 'Enable':
                $slider->update(['is_visible' => '1']);
                break;
            case 'Disable':
                $slider->update(['is_visible' => '0']);
                break;
        }
        return response()->json([
            'message'   => 'Action performed on selected record(s).'
        ]);
    }
}
