<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\SalientFeature;
use Illuminate\Http\Request;

class SalientFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\SalientFeature  $salientFeature
     * @return \Illuminate\Http\Response
     */
    public function show(SalientFeature $salientFeature)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\SalientFeature  $salientFeature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalientFeature $salientFeature)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\SalientFeature  $salientFeature
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalientFeature $salientFeature)
    {
        //
    }
}
