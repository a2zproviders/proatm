<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Model\Service;
use App\Model\Store;
use App\Model\ServicePageData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    public function all(Store $store)
    {
        $list =  Service::where('store_id', $store->uid)->pluck('title', 'id');

        return response()->json($list);
    }
    protected function image_upload($request, $service)
    {
        if ($request->hasFile('image')) {
            if (!empty($service->image)) {
                $nameArr  = explode("?v=", $service->image);
                $image     = $nameArr[0];
                $file_main  = public_path("img/service/original/{$image}");
                $file_thumb = public_path("img/service/thumbnail/{$image}");
                File::delete($file_main, $file_thumb);
            }

            $image = $request->file('image');
            $imageName = $service->id.'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/img/service/thumbnail');
            $img = Image::make($image->getRealPath());
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$imageName);
            // save original image
            $destinationPath = public_path('/img/service/original');
            $image->move($destinationPath, $imageName);

            $service->image = $imageName."?v=".uniqid();
            $service->save();
            return true;
        } else {
            return false;
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $user = auth()->user();
        $query = Service::withCount('service_points');
        if (request('trash') == 'true') {
            $query->onlyTrashed();
        }
        if (!empty(request('visibility'))) {
            $query->where('is_visible', request('visibility'));
        }
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where(function ($q) use ($s) {
                $q->where('title', 'LIKE', '%'.$s.'%')
                  ->orWhere('slug', 'LIKE', '%'.$s.'%')
                  ->orWhere('short_description', 'LIKE', '%'.$s.'%');
            });
        }
        if(empty($request->type) || $request->type != 'all') {
            $service = $query->where('store_id', $store->uid)->latest()->paginate(12);
            foreach ($service as $index => $t) {
                $service[$index]->logo_thumb = $t->image ? url('img/service/thumbnail/' . $t->image) : '';
                $service[$index]->logo = $t->image ? url('img/service/original/' . $t->image) : '';
            }
            return response()->json(["service"=>$service, 'service_image'=>$service->append('image_url')], 200);
        } else {
            $service = $query->where('store_id', $store->uid)->orderBy('title')->get();
            return response()->json($service);
        }
    }
    public function ServiceWithCat(Store $store)
    {
        $user = auth()->user();
        $query = Service::withCount('category');

        if (!empty(request('visibility'))) {
            $query->where('is_visible', request('visibility'));
        }
        $service = $query->where('store_id', $store->uid)->latest();
        foreach ($service as $index => $t) {
            $service[$index]->logo_thumb = $t->image ? url('img/service/thumbnail/' . $t->image) : '';
            $service[$index]->logo = $t->image ? url('img/service/original/' . $t->image) : '';
        }
        return response()->json(["service"=>$service, 'service_image'=>$service->append('image_url')], 200);
    }
    public function serviceList(Request $request, Store $store)
    {
        // dd($request->limit);
        $query = Service::where('store_id', $store->uid)->skip(0)->take($request->limit)->get();
        return response()->json(["service"=>$query, 'service_image'=>$query->append('image_url')], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->record;
        $arr['store_id'] = auth()->user()->store_id;
        $service = Service::create($arr);

        $this->image_upload($request, $service);

        return response()->json('success', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store, Service $service)
    {
        if (!empty($service->image)) {
            // $image = $service->image;
            $service->image = url('img/service/original/'.$service->image);
            $binary =file_get_contents($service->image);
            $service->image_prefill_url = 'data:image/jpg;base64,'. base64_encode($binary);
        }

        $service = $service->toArray();
        $service = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $service);

        return response()->json($service);
    }
    public function showService(Store $store, Service $service)
    {
        $points = ServicePageData::where('s_id', $service->id)->get();
        foreach ($points as $k => $v) {
            $points[$k]->thumb = $v->image ? url('img/service-point/thumbnail/'.$v->image) : '';
        }
        if (!empty($service->image)) {
            $service->image = url('img/service/original/'.$service->image);
        }

        $service = $service->toArray();
        $service = array_map(function ($v) {
            return empty($v) || $v == "null" ? "" : $v;
        }, $service);

        return response()->json(['data' => $service, 'points' => $points]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Service  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, Service $service) {
        $data = $request->record;
        $service->fill($data);
        $this->image_upload($request, $service);
        $service->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Service::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Service::whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Service::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>1));
            if ($status) {
                $msg = 'Enabled successfully';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Service::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>0));
            if ($status) {
                $msg = 'Disabled successfully';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
    public function destroy(Request $request, $store, $id)
    {
        $service = Service::withTrashed()->findOrFail($id);
        
        if (request('flag') == 'Trash') {
            if ($service->delete()) {
                return response()->json(['status'=>1, 'message'=>'deleted'], 200);
            }
        } elseif (request('flag') == 'Restore') {
            if (Service::withTrashed()->where('id', $service->id)->restore()) {
                return response()->json(['status'=>1, 'message'=>'Record Re-store'], 200);
            }
        } elseif (request('flag') == 'Delete') {
            if (Service::withTrashed()->where('id', $service->id)->forceDelete()) {
                return response()->json(['status'=>1, 'message'=>'Record deleted permanently!!!'], 200);
            }
        }
    }
    public function updateStatus(Request $request, $store, $id)
    {
        $service  = Service::withTrashed()->findOrFail($id);
        if ($service) {
            $service->update(['is_visible'=>!$service->is_visible]);
            $msg = "Updated successfully";
        } else {
            $msg = "Page not found";
        }
        return response()->json(['status'=> 1, 'message'=>$msg], 200);
    }
}