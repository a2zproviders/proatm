<?php

namespace App\Http\Controllers\Api;

use App\Model\Inquiry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Post;
use App\Model\Store;
use Mail;

class InquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $query = Inquiry::query();
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('name', 'LIKE', '%' . $s . '%')
                ->orWhere('email', 'LIKE', '%' . $s . '%')
                ->orWhere('mobile', 'LIKE', '%' . $s . '%');
        }
        if (!empty(request('status'))) {
            $query->where('status', request('status'));
        }

        $post = $query->where('user_id', $store->uid)
            ->latest()
            ->paginate(10);

        $re = compact('post');

        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Store $store)
    {
        $request->validate([
            'record'          => 'required|array',
            'record.name'     => 'required|string',
            'record.email'    => 'required|email',
            // 'record.phone'=>'required|string',
            //   'record.subject'=>'required|string',
            'record.message'  => 'required|string',
            'token'           => 'required'
        ]);

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => '6LdWQWcaAAAAAG0neO1D9BbYGyCwKSP5nfU5hHC1',
            'response' => $request->get('token'),
            'remoteip' => $remoteip
        ];
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        // dd($resultJson);
        if (!$resultJson->success) {
            return response()->json([
                'message'   => 'Captcha code is not matched.'
            ], 403);
        }

        $inquiry = new Inquiry($request->record);
        $inquiry->user_id = $store->uid;
        $inquiry->save();

        // Sending Mail
        $data = [
            'to'                => $store->user->email,
            'receiver'          => $store->user->name,
            'sender'            => $inquiry->name,
            'sender_email'      => $inquiry->email,
            'subject'           => 'Contact Enquiry Sent via ' . $store->store_name,
            'subject_user'      => 'Your Enquiry Sent successfully to ' . $store->store_name,
            'store'             => $store,
            'inquiry'           => $inquiry
        ];
        // dd('dfgj', $data);
        Mail::send('emails.enquiry', $data, function ($message) use ($data) {
            extract($data);

            $message->to($to, $receiver)
                ->subject($subject)
                ->replyTo($sender_email, $sender)
                ->from($sender_email, $sender);
        });

        Mail::send('emails.enquiry-user', $data, function ($message) use ($data) {
            extract($data);

            $message->to($sender_email, $sender)
                ->subject($subject_user)
                ->replyTo($to, $receiver)
                ->from($to, $receiver);
        });

        return response()->json([
            'message' => 'Your message has been submitted successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inquiry $inquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Inquiry  $inquiry
     * @return \Illuminate\Http\Response
     */

    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Inquiry::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Inquiry::whereIn('id', explode(",", $request->ids))->update(array('is_visible' => 1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Restore') {
            $status = Inquiry::withTrashed()->whereIn('id', explode(",", $request->ids))->restore();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'PDelete') {
            $status = Inquiry::withTrashed()->whereIn('id', explode(",", $request->ids))->forceDelete();
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Inquiry::whereIn('id', explode(",", $request->ids))->update(array('is_visible' => 0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status' => $status, 'message' => $msg], 200);
    }

    public function destroy(Request $request, $store, $id)
    {
        $page = Inquiry::findOrFail($id);
        // dd($page);
        if ($page->delete()) {
            return response()->json(['status' => 1, 'message' => 'deleted'], 200);
        }
    }

    public function updateStatus(Request $request, Store $store, $id)
    {
        $inquiry  = Inquiry::findOrFail($id);
        if ($inquiry) {
            // dd($request->record['status']);
            $inquiry->update(['status' => $request->record['status'], 'reply' => $request->record['reply']]);
            $msg = "Updated successfully";

            // Sending Mail
            $data = [
                'to'                => $store->user->email,
                'receiver'          => $store->user->name,
                'sender'            => $inquiry->name,
                'sender_email'      => $inquiry->email,
                'subject'           => 'Contact Enquiry Status has been changed via ' . $store->store_name,
                'subject_user'      => 'Your Enquiry Status has been changed to ' . $store->store_name,
                'store'             => $store,
                'inquiry'           => $inquiry
            ];
            Mail::send('emails.enquiry-user', $data, function ($message) use ($data) {
                extract($data);

                $message->to($sender_email, $sender)
                    ->subject($subject_user)
                    ->replyTo($to, $receiver)
                    ->from($to, $receiver);
            });
        } else {
            $msg = "Page not found";
        }
        return response()->json(['status' => 1, 'message' => $msg], 200);
    }
}
