<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Store;
use Illuminate\Http\Request;

use Image;

class StoreController extends Controller
{
    public function editProfile(Request $request, Store $store) {
        $request->validate([
            'store'             => 'required|array',
            'store.store_name'  => 'required|string'
        ]);

        $request->store = array_map(function ($v) {
            return $v == "null" ? null : $v;
        }, $request->store);
        $store->fill($request->store);
        $store->show_title = !$request->store['show_title'] || $request->store['show_title'] === 'false' ? 0 : 1;
        $store->save();

        $save_path = public_path("img/stores/");
        if (!file_exists($save_path)) {
            mkdir($save_path, 755, true);
        }
        if($request->hasFile('logo')) {
            $image = $request->file('logo');
            $file_name = "logo_{$store->id}.jpg";
            // Image::make($image->getRealPath())
            // ->resize(250, 50, function($constraint) {
            //     $constraint->aspectRatio();
            // })
            // ->save($save_path.$file_name);

            $image->move($save_path, $file_name);
            

            $store->logo = "{$file_name}?v=".time();
        }

        if($request->hasFile('favicon')) {
            $image = $request->file('favicon');
            $file_name = "favicon_{$store->id}.jpg";
            Image::make($image->getRealPath())->resize(128, 128, function($constraint) {
                $constraint->aspectRatio();
            })
            ->resizeCanvas(128, 128)
            ->save($save_path.$file_name);

            $store->favicon = "{$file_name}?v=".time();
        }

        if($request->hasFile('logo') || $request->hasFile('favicon')) {
            $store->save();
        }

        return response()->json([
            'message'   => 'Data has been updated.',
            'data'      => $store
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ?: 10;
        $query = Store::with(['user']);

        if(!empty($request->show) && $request->show == 'all')
        {
            $stores = $query->orderBy('store_name')->pluck('store_name', 'uid');
        }
        else
        {
            $stores = $query->latest()->paginate($limit);
        }

        return response()->json($stores);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'store'             => 'required|array',
            'store.store_name'  => 'required|string',
            'store.store_url'   => 'required|string|unique:stores,store_url',
            'user'              => 'required|array',
            'user.name'         => 'required|string',
            'user.login'        => 'required|string|unique:users,login',
            'user.password'     => 'required|string'
        ]);
        $userArr = $request->user;
        unset($userArr['confirm_password']);
        $user = new User($userArr);
        $user->first_name   = $request->user['name'];
        $user->password     = \Hash::make($request->user['password']);
        $user->role         = 2;
        $user->save();
        
        $user->store_id = $user->id;
        $user->save();

        $user->store()->create($request->store);

        return response()->json([
            'message'   => 'Record has been saved.',
            'data'      => $user
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($store_id)
    {
        $store = Store::with('user')->findOrFail($store_id);

        if($store->logo) {
            $store->logo = url("img/stores/{$store->logo}");
        }

        if($store->favicon) {
            $store->favicon = url("img/stores/{$store->favicon}");
        }

        return response()->json($store);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $store = Store::with(['user', '']);
        $request->validate([
            'store'             => 'required|array',
            'store.store_name'  => 'required|string',
            'store.store_url'   => 'required|string|unique:stores,store_url,'.$user->store->id.',id',
            'user'              => 'required|array',
            'user.name'         => 'required|string',
            'user.login'        => 'required|string|unique:users,login,'.$user->id.',id',
            'user.password'     => 'required|string'
        ]);

        $user = new User($request->user);
        $user->first_name   = $request->user['name'];
        $user->password     = \Hash::make($request->user['password']);
        $user->role         = 2;
        $user->save();
        
        $user->store_id = $user->id;
        $user->save();

        $user->store()->create($request->store);

        return response()->json([
            'message'   => 'Record has been saved.',
            'data'      => $user
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
