<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\HomeSetting;
use Illuminate\Http\Request;
use App\Model\Store;
use Illuminate\Support\Facades\auth;

class HomeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store_id = auth()->user()->store_id;
        $list =  HomeSetting::with('templates')
        ->where('store_id', $store_id)->get();
        return response()->json($list);
    }
    public function homeTemplate(Store $store)
    {
        $list =  HomeSetting::with('templates')
        ->where('status', 'yes')
        ->where('store_id', $store->uid)
        ->orderBy('order', 'ASC')
        ->get();
        return response()->json($list);
    }
    public function templateInfo(Request $request, Store $store)
    {
        $list =  HomeSetting::with('templates')
        ->where('status', 'yes')
        ->where('store_id', $store->uid)
        ->whereHas('templates', function ($q) use ($request) {
            $q->where('component', $request->component);
        })
        ->orderBy('order', 'ASC')
        ->firstOrFail();
        return response()->json($list);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\HomeSetting  $homeSetting
     * @return \Illuminate\Http\Response
     */
    public function show(HomeSetting $homeSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\HomeSetting  $homeSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, $homeSetting)
    {
        $homeSetting = HomeSetting::findOrFail($homeSetting);
        $data = $request->record;
        $homeSetting->fill($data);
        $homeSetting->save();
        return response()->json($homeSetting);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\HomeSetting  $homeSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeSetting $homeSetting)
    {
        //
    }
}
