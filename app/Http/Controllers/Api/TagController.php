<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Tag;
use App\Model\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\auth;
use Illuminate\Support\Str;

class TagController extends Controller
{
    public function all(Store $store)
    {
        $tag =  Tag::where('store_id', $store->uid)->pluck('name', 'id');
        return response()->json($tag);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Store $store)
    {
        $tag = Tag::with('posts');
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $tag->where('name', 'LIKE', '%'.$s.'%');
        }
        $t = $tag->where('store_id', auth()->user()->store_id)->latest()->paginate(10);
        return response()->json($t);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record  = $request->record;
        $record['store_id'] = auth()->user()->store_id;
        $tag = Tag::create($record);
        $tag_id = $tag->id;
        $tags = Tag::where('store_id', auth()->user()->store_id)->pluck('name', 'id');
        return response()->json(compact('tag_id', 'tags'));
    }
    public function addTag(Request $request)
    {
        // dd($request->record);
        $arr = $request->record;
        $arr['slug'] = Str::slug($arr['slug']);
        $exist = Tag::where('slug', '=', $arr['slug'])->first();
        if ($exist===null) {
            $arr['store_id'] = auth()->user()->store_id;
            $tag = Tag::create($arr);
            $msg = "Tag created successfully";
            $msg2 = "Great!!!";
            $status = 1;
        } else {
            $msg2 = "Record not created";
            $msg = "Slug already exist. Change slug and try again.";
            $status = 2;
        }
        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function singleShow(Store $store, $tag)
    {
        $tag = Tag::
        with(
            ['posts' => function ($q) {
                $q->select('posts.id', 'posts.name', 'posts.slug', 'posts.image');
            },
          ]
        )
        ->where('store_id', $store->uid)->where('slug', $tag)->firstOrFail();
        $posts = $tag->posts;
        unset($tag->posts);
        return response()->json(['tag'=>$tag, 'posts'=>$posts]);
    }
    public function show(Store $store, Tag $tag)
    {
        return response()->json($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $store, Tag $tag)
    {
        $data = $request->record;
        $data['slug'] = Str::slug($data['slug']);
        $tag->fill($data);
        $tag->save();
        $msg = "Tag updated successfully";
        $msg2 = "Great!!!";
        $status = 1;

        return response()->json(['status'=>$status, 'message'=>$msg, 'message1'=>$msg2]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($store, $tag)
    {
        $tag = Tag::withTrashed()->findOrFail($id);

        if ($tag->delete()) {
            return response()->json(['status'=>1, 'message'=>'deleted'], 200);
        }
    }
}
