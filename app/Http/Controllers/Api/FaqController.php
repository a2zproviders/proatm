<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Faq;
use App\Model\Store;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\File;

class FaqController extends Controller
{
    public function store(Request $request)
    {
        $arr = $request->record;
        $arr['store_id'] = auth()->user()->store_id;
        $faq = Faq::create($arr);
        return response()->json('success', 200);
    }
    public function index(Store $store)
    {
        $user = auth()->user();
        $query = Faq::where('store_id', $user->store_id);
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('question', 'LIKE', '%'.$s.'%')
                ->orWhere('answer', 'LIKE', '%'.$s.'%');
        }
        $faq = $query->latest()->paginate(10);
        return response()->json($faq->toArray(), 200);
    }
    public function faqList(Store $store)
    {
        $query = Faq::where('store_id', $store->uid);
        if (!empty(request('s'))) {
            $s = request('s');
            $s = trim($s);
            $query->where('question', 'LIKE', '%'.$s.'%')
                ->orWhere('answer', 'LIKE', '%'.$s.'%');
        }
        $faq = $query->where('is_visible', 1)->latest()->paginate(20);
        return response()->json($faq->toArray(), 200);
    }
    public function update(Request $request, $store, Faq $faq)
    {
        $faq->fill($request->record);

        if ($faq->save()) {
            $msg = 'Record updated successfully';
        } else {
            $msg = 'Some error occurs. try again !!!';
        }
        return response()->json($msg, 200);
    }
    public function show($store, Faq $faq)
    {
        return response()->json($faq, 200);
    }
    public function multipalDelete(Request $request)
    {
        if ($request->action == 'Delete') {
            $status = Faq::whereIn('id', explode(",", $request->ids))->delete();
            if ($status) {
                $msg = 'Successfully deleted';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } elseif ($request->action == 'Enable') {
            $status = Faq::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>1));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        } else {
            $status =  Faq::whereIn('id', explode(",", $request->ids))->update(array('is_visible'=>0));
            if ($status) {
                $msg = 'Successfully updated';
                $status = 1;
            } else {
                $msg = 'Error occurs !';
                $status = 0;
            }
        }
        return response()->json(['status'=>$status, 'message'=>$msg], 200);
    }
    public function destroy($store, Faq $faq)
    {
        if ($faq->delete()) {
            return response()->json(['status'=>1, 'message'=>'deleted'], 200);
        }
    }
}
