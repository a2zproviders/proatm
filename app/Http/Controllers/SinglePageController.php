<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Store;
use App\Model\Pages;

class SinglePageController extends Controller
{
    public function index($page = null)
    {
        $domain = request()->getHost();
        
        $data = [];
        $data['store'] = Store::where('store_url', $domain)->firstOrFail();
        $page = Pages::where('slug', $page)->where('store_id', $data['store']->uid)->first();
        $data['meta'] = [
            'title' => $data['store']->store_name
        ];
        if(!empty($data['store']->site_tagline)) {
            $data['meta']['title'] .= ' | ' . $data['store']->site_tagline;
        }
        if(!empty($page))
        {
            $data['meta']['title']          = $page->seo_title ?: $page->title.' | '.$data['store']->store_name;
            $data['meta']['keywords']       = $page->seo_keywords ?: "";
            $data['meta']['description']    = $page->seo_description ?: $page->short_description;
        }
        return view('app', $data); 
    }
}
