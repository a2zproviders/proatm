<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuLocation extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function menuitems()
    {
        return $this->hasMany('\App\Model\MenuItem', 'mp_id', 'id');
    }
}
