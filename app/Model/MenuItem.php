<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $guarded = [];
    public function page()
    {
        return $this->belongsTo(Pages::class);
    }
}
