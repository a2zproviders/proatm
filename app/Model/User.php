<?php

namespace App\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guarded = [];
    protected $hidden = [
      'password', 'remember_token', 'login', 'deleted_at'
    ];
    public function store()
    {
        return $this->hasOne(Store::class, 'uid', 'store_id');
    }
    public function role_info()
    {
        return $this->hasOne(Role::class, 'id', 'role');
    }
}
