<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePageData extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function service()
    {
        return $this->hasOne('\App\Model\Service', 's_id', 'id');
    }
}
