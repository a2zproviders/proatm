<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HomeSetting extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function templates()
    {
        return $this->hasOne(Template::class, 'id', 'template_id');
    }
}
