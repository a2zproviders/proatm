<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table    = "media";
    protected $guarded  = ['created_at', 'updated_at'];
    protected $appends  = ['image_url'];

    public function getImageUrlAttribute()
    {
        $string = $this->image;
        return "data:image/png;base64,{$string}";
    }
}
