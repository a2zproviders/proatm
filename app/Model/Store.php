<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = [];
    protected $with    = ['user'];
    protected $appends = ['category_name'];
    protected $casts   = [
        'social_links'  => 'array'
    ];

    public function getCategoryNameAttribute()
    {
        return $this->category->name;
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'store_id', 'uid');
    }
    public function pages()
    {
        return $this->hasMany(Pages::class, 'store_id', 'uid');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'uid');
    }

    public function category()
    {
        return $this->belongsTo(BusinessCategory::class);
    }
}
