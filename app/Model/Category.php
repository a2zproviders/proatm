<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $appends = ['image_url'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_categories', 'category_id', 'post_id');
    }
    public function service()
    {
        return $this->hasMany(Service::class, 'category_id', 'id');
    }
    public function category_type()
    {
        return $this->belongsTo(CategoryType::class, 'category_type_id', 'id');
    }
    public function getImageUrlAttribute()
    {
        $thumb = !empty($this->image) ? url('img/category/thumbnail/'. $this->image): url('img/dummy.jpg');
        $fullimage = !empty($this->image) ? url('img/category/original/'. $this->image):url('img/dummy.jpg');
        return compact('thumb', 'fullimage');
    }
}
