<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    //

    public function service_points()
    {
        return $this->hasMany(ServicePageData::class, 's_id', 'id');
    }
    
    public function getImageUrlAttribute()
    {
        $thumb = !empty($this->image) ? url('img/service/thumbnail/'. $this->image): url('img/dummy.jpg');
        $full = !empty($this->image) ? url('img/service/original/'. $this->image):url('img/dummy.jpg');
        return compact('thumb', 'full');
    }
}
