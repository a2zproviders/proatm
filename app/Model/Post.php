<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $appends = ['image_url'];
    protected $hidden = ['deleted_at'];
    // protected $hidden = array('categories', 'tag');
    public function category()
    {
        return $this->belongsToMany(Category::class, 'post_categories', 'post_id', 'category_id');
    }
    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }
    public function getImageUrlAttribute()
    {
        $thumb = !empty($this->image) ? url('img/post/thumbnail/'. $this->image): url('img/dummy.jpg');
        $fullimage = !empty($this->image) ? url('img/post/original/'. $this->image):url('img/dummy.jpg');
        return compact('thumb', 'fullimage');
    }
}
