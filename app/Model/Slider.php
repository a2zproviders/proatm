<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    // use SoftDeletes;
    protected $guarded = ['created_at', 'updated_at'];
    protected $appends = ['redirect_url'];
    protected $casts = ['is_visible' => 'boolean'];

    public function getRedirectUrlAttribute()
    {
        $url = null;
        if($this->page) {
            $url = '/'.$this->page->slug;
        }
        elseif($this->service) {
            $url = '/service/'.$this->service->slug;
        }

        return $url;
    }

    public function page() {
        return $this->belongsTo(Pages::class);
    }
    public function service() {
        return $this->belongsTo(Service::class);
    }
}
