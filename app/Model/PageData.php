<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageData extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function page()
    {
        return $this->hasOne('\App\Model\Pages', 'page_id', 'id');
    }
}
