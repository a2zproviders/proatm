<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
    //
    use SoftDeletes;
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];
    protected $with = ['template_info', 'page_points'];
    protected $appends = ['image_url'];
    
    public function template_info()
    {
        return $this->hasOne(Template::class, 'id', 'template');
    }
    public function page_points()
    {
        return $this->hasMany(PageData::class, 'page_id', 'id');
    }
    public function getImageUrlAttribute()
    {
        $thumb = !empty($this->image) ?  url('img/product/thumbnail/'.$this->image):"";
        $full  =  !empty($this->image) ?  url('img/product/original/'.$this->image):"";
        return compact('thumb', 'full');
    }
}