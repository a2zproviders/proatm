<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Portfolio extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $appends = ['image_url'];
    public function category()
    {
        return $this->belongsToMany(Category::class, 'portfolio_categories', 'portfolio_id', 'category_id');
    }
    public function getImageUrlAttribute()
    {
        $thumb = !empty($this->image) ? url('img/portfolio/thumbnail/'. $this->image): url('img/dummy.jpg');
        $fullimage = !empty($this->image) ? url('img/portfolio/original/'. $this->image):url('img/dummy.jpg');
        return compact('thumb', 'fullimage');
    }
}
